<?php
require_once("AbsConection.php");

class Usuario extends AbsConection
{
	public $nombre;
	public $email;
	private $clave;
	private $id;

	function __construct()
	{
		$this->db_name = 'dbsimple';
	}

	public function get($user_id = '')
	{
		if ($user_id != '') {
			$this->query = "SELECT * FROM usuario WHERE idusuario= '$user_id'";
			$this->consige_resultado_sentecia();
		}
		if (count($this->rows) == 1) {
			foreach ($this->rows[0] as $propiedad => $valor) {
				$this->$propiedad = $valor;
			}
		}
	}

	public function set($user_datos = array())
	{
		if (array_key_exists('idusuario', $user_datos)) {
			$this->get($user_datos['idusuario']);

			if ($user_datos['idusuario'] != $this->idusuario) {
				foreach ($user_datos as $campo => $valor) {
					$$campo = $valor;
				}
				$this->query = "INSERT INTO usuario(id, nombre, email, clave)VALUES({$id}, '{$this->nombre}', '{$this->email}', '{$this->clave}')";
				$this->ejecute_simple_sentencia();
			}
		}
	}

	public function update($user_datos = array())
	{
		foreach ($user_datos as $campo => $valor) {
			$this->$campo = $valor;
		}
		$this->query = "UPDATE usuario SET nombre = '{$this->nombre}', email= '{$this->email}', clave = '{$this->clave}' WHERE idusuario = '{$this->id}'";
		$this->ejecute_simple_sentencia();
	}

	public function delete($id = '')
	{
		$this->query = "DELETE FROM usuario WHERE id= '{$id}'";
		$this->ejecute_simple_sentencia();
	}

	function __destruct()
	{
		unset($this);
	}
}
