<?php
abstract class AbsConection
{
	static $db_host = "localhost";
	static $db_user = "root";
	static $db_pass = "12345";
	static $db_name = "dbsimple";
	protected $query;
	protected $rows = array();
	private $db;

	//metodos abstractos para la clase que los herede
	abstract protected function get();
	abstract protected function set();
	abstract protected function update();
	abstract protected function delete();

	public function abrir_coneccion()
	{
		$this->db = new mysqli(self::$db_host, self::$db_user, self::$db_pass, $this->db_name);
		if ($this->db->connect_errno) {
			echo "Fallo al conectar a MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error;
		}
	}

	public function cerrar_coneccion()
	{
		$this->db->close();
	}

	//ejecuta sentencias de insert, delete, update
	protected function ejecute_simple_sentencia()
	{
		$this->abrir_coneccion();
		$this->db->query($this->query);
		$this->cerrar_coneccion();
	}

	//ejecuta sentencias de consulta
	protected function consige_resultado_sentecia()
	{
		$this->abrir_coneccion();
		$resultado = $this->db->query($this->query);

		while ($this->rows[] = $resultado->fetch_assoc());
		$resultado->close();
		$this->cerrar_coneccion();
		array_pop($this->rows);
	}

	public static function set_db_host($db_host){
		self::$db_host = $db_host;
	}
	public static function set_db_user($db_user){
		self::$db_user = $db_user;
	}
	public static function set_db_pass($db_pass){
		self::$db_pass = $db_pass;
	}
	public static function set_db_name($db_name){
		self::$db_name = $db_name;
	}
}
