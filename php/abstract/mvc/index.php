<?php
require_once("config.php");

$full_url = (isset($_GET['url'])) ?	$_GET['url'] : "Index/index";
$url = explode("/", $full_url);
$controllers = '';
$size_url = count($url);
$params = array();

try {
	$meta = substr(strrchr($full_url, ".php?"), 1);
	if ($meta) {
		$ext_meta  = explode("/", $meta);
		for ($j = 0; $j < count($ext_meta); $j++) {
			if ($j == 0) {
				$method = $ext_meta[0];
			} else {
				$params[] = $ext_meta[$j];
			}
		}
		if ($method === "") {
			$method = "index";
		}
	} else {
		throw new Exception("El controlador no es correcto.", 1);
		exit;
	}

	$path = trim(URL . "app/Controllers/ ");
	$controllersPash = trim(BASEPATH . "Controllers\ ");
	$meta = substr($full_url, stripos(".php", $full_url));
	$url = explode("/", $meta);
	$size_url = count($url);

	for ($i = 0; $i < $size_url; $i++) {
		if (!strrpos($url[$i], ".php", -4)) {
			$controllersPash .= trim($url[$i] . '\ ');
		} else {
			$controllersPash .= $url[$i];
			$controllers = $path . $url[$i];
		}
	}
} catch (Exception $e) {
	print_r("!Error. " . $e);
}

/*
evaluar si tenemo una clase cargada
*/
spl_autoload_register(function ($class) {
	//evaluamos si existe la clase el la libreria
	if (file_exists(LIBRARY . $class . ".php")) :
		require(LIBRARY . $class . ".php");
	endif;
});

/*
accedemos al controlador con la clase que se intancia
*/
try {
	if (file_exists($controllersPash)) {
		require($controllersPash);
		$controllers = new $controllers();
		if (isset($method)) {
			//verificamos si existe el metodo en la clase
			if (method_exists($controllers, $method)) {
				if (isset($params)) {
					//el metodo recive un parametro
					$controllers->{$method}($params);
				} else {
					$controllers->{$method}();
				}
			} else {
				echo "No existe metodo";
			}
		}
	} else {
		throw new Exception("!Error. El controlador no existe.", 1);
	}
} catch (Exception $e) {
	echo "!Error. " . "<br/>" . $controllersPash . "<br/>" . $e;
}
