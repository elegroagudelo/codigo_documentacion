<?php
class Amigos
{
	public $nombre;
	public $apellidos;
	private $edad;

	public function __construct($nom, $ape, $eda)
	{
		$this->nombre = $nom;
		$this->apellidos = $ape;
		$this->edad = $eda;
	}

	public function verinformacion()
	{
		echo "Nombre: " . $this->nombre . "<br>";
		echo "Apellidos: " . $this->apellidos . "<br>";
		echo "Edad: " . $this->edad . "<br>";
	}
}

$amigos = new Amigos("Edwin Andres", "Legro Agudelo", 26);
$amigos->verinformacion();
