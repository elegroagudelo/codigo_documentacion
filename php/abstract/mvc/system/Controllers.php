<?php
class Controllers
{

	public function __construct()
	{
		$this->loadClassmodels();
		$this->view = new views();
	}

	public function loadClassmodels()
	{
		//hacemos la referncia con la clase index para asignar el modelo
		$model = get_class($this) . "_model";
		$path = 'Models/' . $model . ".php";
		if (file_exists($path)) {
			require $path;
			//intanciamos la clase model
			$this->model = new $model();
		}
	}
}
