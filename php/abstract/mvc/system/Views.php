<?php
class Views
{

	public function __construct()
	{
	}

	public function render($controller, $view)
	{
		$controllers = get_class($controller);
		require views . DTF . "head.php";
		require views . $controllers . "/" . $view . ".php";
	}
}
