<!DOCTYPE html>
<html lang="es">
<head>
	<title>Sistema MVC</title>
	<link rel="stylesheet" type="text/css" href="<?=URL.VIEWS.DTF;?>Css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?=URL.VIEWS.DTF;?>Css/fontawesome.css">
	<link rel="stylesheet" type="text/css" href="<?=URL.VIEWS.DTF;?>Css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?=URL.VIEWS.DTF;?>Css/style.css">
	<script type="text/javascript" src="<?=URL.VIEWS.DTF;?>Js/jquery.js"></script>
	<script type="text/javascript" src="<?=URL.VIEWS.DTF;?>Js/bootstrap.js"></script>
</head>
<body>