<?php

class ProcesadorFabrica implements ProcesadorFabricaInterfaz
{

	protected array $factories = [
		'text/xml' => XmlProcesador::class,
		'text/json' => JsonProcesador::class
	];

	protected string $default = JsonProcesador::class;

	public function make(string  $mediaType): ProcesadorIterfaz
	{
		$procesador = $this->factories[$mediaType] ?? $this->default;
		if ($procesador) {
			return new $procesador;
		}
		throw new InvalidArgumentException(sprintf("No factory for %s"), $mediaType);
	}
}
