<?php 
interface ProcesadorIterfaz
{
    public function read(string $content): array;

    public function write(array $data): string;

}