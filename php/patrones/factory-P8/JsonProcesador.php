<?php
require_once  'ProcesadorIterfaz.php';

class JsonProcesador implements ProcesadorIterfaz
{

    public function read(string $content): array {
        return json_decode($content, true);
    }

    public function write(array $data): string {
        return json_encode($data);
    }
}
