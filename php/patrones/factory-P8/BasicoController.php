<?php 
require_once  'XmlProcesador.php';
require_once  'JsonProcesador.php';
require_once  'DataServicios.php';
require_once  'Response.php';

class BasicoController {

    protected DataServicios $dataServicios;

    public function __construct(DataServicios $dataServicios)
    {
        $this->dataServicios = $dataServicios;
    }

    public function get(Request $request, int $datasetid): Response
    {
        $data = $this->dataServicios->get($datasetid);
        $format = $request->header('Accept');
        
        if($format === 'text/xml'){
            $output = (new XmlProcesador)->write($data);
        }else{
            $format = $request->header('application/json');
            $output = (new JsonProcesador)->write($data);
        }
        return Response::make(
            $output,
            200,
            ['Content-Type'=> 'application/json']
        );
    }

    public function set(Request $request): Response 
    {
        return Response::make(
            json_encode($request),
            200,
            ['Content-Type'=> 'application/json']
        );
    }    

}