<?php
require_once  'ProcesadorFabricaInterfaz.php';
require_once  'XmlProcesador.php';
require_once  'JsonProcesador.php';
require_once  'DataServicios.php';
require_once  'Response.php';

class FactoryController
{

	protected DataServicios $dataServicios;
	private ProcesadorFabricaInterfaz $factory;

	public function __construct(DataServicios $dataServicios, ProcesadorFabricaInterfaz $factor)
	{
		$this->dataServicios = $dataServicios;
		$this->factory = $factor;
	}

	public function get(Request $request, int $datasetid): Response
	{
		$data = $this->dataServicios->get($datasetid);
		$format = $request->header('Accept');
		$factory = $this->factory->make($format);

		return Response::make(
			$factory->write($data),
			200,
			['Content-Type' => $format]
		);
	}

	public function set(Request $request): Response
	{
		return Response::make(
			json_encode($request),
			200,
			['Content-Type' => 'application/json']
		);
	}
}
