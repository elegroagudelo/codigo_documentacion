<?php 
class Response {

    private function __construct(string $data, array $type)
    {
    }

    private function __clone()
    {
    }

    public static function make(
        string $body, 
        int $statusCode, 
        array $headers
    ): Response
    {
        return new Response($body, $headers);
    }

}