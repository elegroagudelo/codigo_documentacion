<?php 
require_once 'BaseComponent.php';

class Component2 extends BaseComponent
{
    public function doC(): void
    {
        echo "002 Componente 2C\n";
        $this->mediator->notify($this, "C");
    }

    public function doD(): void
    {
        echo "002 Componente 2D\n";
        $this->mediator->notify($this, "D");
    }
}