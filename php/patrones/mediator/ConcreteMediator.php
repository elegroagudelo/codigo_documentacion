<?php
require_once 'IMediator.php';
use php\mediator\IMediator;

class ConcreteMediator implements IMediator {

    private static $component1;
    private static $component2;

    public static function init(Component1 $c1, Component2 $c2){
        $mediator = new ConcreteMediator;
        self::$component1 = $c1;
        self::$component1->setMediator($mediator);
        self::$component2 = $c2;
        self::$component2->setMediator($mediator);
    } 
    
    public function notify(object $sender, string $event) : void 
    {
        if ($event == "A") {
            echo "\nMediador reacciona a A and triggers following operations:\n";
            self::$component2->doC();
        }

        if ($event == "D") {
            echo "\nMediador reacciona a D and triggers following operations:\n";
            self::$component1->doB();
            self::$component2->doC();
        }
    }
}