<?php
require_once 'BaseComponent.php';

class Component1 extends BaseComponent
{
    public function doA(): void
    {
        echo "001 Componente 1A\n";
        $this->mediator->notify($this, "A");
    }

    public function doB(): void
    {
        echo "001 Componente 1B.\n";
        $this->mediator->notify($this, "B");
    }
}