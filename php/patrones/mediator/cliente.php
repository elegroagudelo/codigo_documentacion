<?php 
/*Mediator es un patrón de diseño de comportamiento que reduce el 
acoplamiento entre los componentes de un programa haciendo que se 
comuniquen indirectamente a través de un objeto mediador especial.
*/
require_once 'Component1.php';
require_once 'Component2.php';
require_once 'ConcreteMediator.php';

$c1 = new Component1();
$c2 = new Component2();
ConcreteMediator::init($c1, $c2);

$c1->doA();
$c2->doD();
