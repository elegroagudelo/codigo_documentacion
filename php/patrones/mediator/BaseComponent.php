<?php 
require_once 'IMediator.php';
use php\mediator\IMediator;

class BaseComponent
{
    protected $mediator;

    public function __construct(IMediator $mediator = null)
    {
        $this->mediator = $mediator;
    }

    public function setMediator(IMediator $mediator): void
    {
        $this->mediator = $mediator;
    }
}
