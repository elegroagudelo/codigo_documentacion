<?php 
namespace php\mediator;

interface IMediator
{
    public function notify(object $sender, string $event): void;
}