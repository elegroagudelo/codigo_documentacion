<?php 
class Connection
{

    protected static $connection; 

    private function __construct()
    {
        echo "Connection creada sin acceder directamemte al constructor\n";
    }

    public static function Connect()
    {
        if(empty(self::$connection)){
            self::$connection = new Connection;
        }
        return self::$connection;
    }

    public function Query($sql)
    {
        if(empty(self::$connection)){
            echo "Error, no se ha creado la coneccion de forma correcta";
        }
        return "Se ejecuta el query sin problema\n";
    }
}

$db = Connection::Connect();
$db = Connection::Connect()->Query("Code query aquí");
print_r($db);