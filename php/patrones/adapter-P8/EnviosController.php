<?php
/**
 * Adapter es un patrón de diseño estructural que 
 * permite la colaboración entre objetos con interfaces incompatibles
 */
require_once "Emailer.php";

class EnviosController implements Emailer{

    public function send():void {
        print_r("Aqui se procesa el envio inicial\n");
    }
}

function sendMails(Emailer $emailer): void {
    $emailer->send();
}

/**
 * buscamos adaptar el proveedor Envios2Controller 
 * para la funcionalidad de la clase principal que hace la implementación
 */
class Envios2Controller
{
    public function sendMails(): void {
        print_r("Aqui se metodo 2 por adapter ok\n");
    }
}

class EmailsProviderAdapter implements Emailer {

    private Envios2Controller $emailProvider;

    public function __construct(Envios2Controller $emailProvider)
    {
        $this->emailProvider = $emailProvider;
    }

    public function send():void {
        $this->emailProvider->sendMails();
    }
}

EnviosController : $controller = new EnviosController();
sendMails($controller);

//no se puede usar directamente el proveedor 1
//para tal fin se debe usar un adaptador
Envios2Controller : $controller2 = new Envios2Controller();
#sendMails($controller2); //aqui se genera un error por adaptación


EmailsProviderAdapter : $controller3 = new EmailsProviderAdapter(new Envios2Controller());
sendMails($controller3); //aqui se resuelve el problema por medio de un adapter

