<?php

/**
 * Singleton Patron PHP
 * Clase static Alertas,
 * La clase alertas es la clase responsable mantener una colección de alertas
 * generadas durante una solicitud. buscando que la informaacion alamcenada persista 
 * dentro de la aplicación y se pueda acceder desde otros componentes del mismo. como algun middleware o service   
 * */
final class Alertas
{

	private static array $alertas = [];
	private static Alertas $instancea;


	private function __construct()
	{
	}

	private function __clone()
	{
	}

	public static function setInstance(Alertas $instancea): Alertas
	{
		self::$instancea = $instancea;
		return self::getInstance();
	}

	public static function getInstance(): self
	{
		if (!isset(self::$instancea)) {
			self::$instancea = new self;
			print_r("Ok se crea una unica instancia de la clase Alerta\n");
		}
		return self::$instancea;
	}

	public function add(string $type, string $message): self
	{
		if(empty(self::$alertas[$type]))
		{
			self::$alertas[$type] = [];
		}
		self::$alertas[$type][] = $message;
		return $this;
	}

	public function get(string $type): array
	{
		return self::$alertas[$type] ?? [];
	}

	public function all(): array
	{
		return self::$alertas;
	}
}

$aler = Alertas::getInstance()->add("success", "Ok caso de exito");
$aler = Alertas::getInstance()->add("error", "Ok se ha generado un error en el sistema");
$aler = Alertas::getInstance()->get("success");
$aler = Alertas::getInstance()->all();
$success = Alertas::getInstance()->get("success");
var_dump($aler);
var_dump($success);