<?php

/**
 * 1. Definir una familia de algoritmos
 * 2. Ponerlos en clases separadas
 * 3. Hacer los objetos intercambiables (implementar una abstracion) 
*/
require_once 'InterfaceOperaciones.php';

class  SumaStrategy implements InterfaceOperaciones {
    public function doOperation($a , $b): int{
        return $a + $b;    
    }
}

class RestaStrategy implements InterfaceOperaciones {
    public function doOperation($a , $b): int{
        return $a - $b;
    }
}

class Calculadora {

    private InterfaceOperaciones $operation;

    public function __construct(InterfaceOperaciones $operation)
    {
        $this->operation = $operation;
    }

    public function execute(int $a, int $b): int
    {
       return  $this->operation->doOperation($a, $b);
    }

    /**
     * executeTradicional function
     * Metodo en desuso para implementar una abstraccion
     * @param [type] $a
     * @param [type] $b
     * @param [type] $operation
     * @return void
     */
    public function executeTradicional($a, $b, $operation)
    {
        if($operation == "addition"){
            return (new SumaStrategy())->doOperation($a, $b);
        }elseif($operation == "subtraction"){
            return (new RestaStrategy())->doOperation($a, $b);
        }else{
            return false;
        }
    }


    public function setOperation(InterfaceOperaciones $operation)
    {
        $this->operation = $operation;
    }

    //usando metodos magicos
    public function __call($method, $arguments)
    {
        //metodo suma = SumaStrategy
        //metodo resta = RestaStrategy
        //argumentos = [4, 5]
        $classname  = ucfirst($method). 'Strategy';
        list($a, $b) = $arguments;
        $this->setOperation(new $classname());
        return $this->execute($a, $b);
    }
}

//$calculator = new Calculadora();
//$result = $calculator->executeTradicional(5, 7, "addition");
//var_dump($result);
#----------------------------------------

$calculator = new Calculadora(new SumaStrategy());
$result = $calculator->execute(5, 7);
var_dump($result);
//Esto genera un problema ya que es necesario crear nuevas instacnias de le clas epor cada operacion que s erequiera.

$calculator->setOperation(new RestaStrategy());
$result = $calculator->execute(10, 4);
var_dump($result);

//usnaod los metodos magicos de php
$result = $calculator->suma(10, 4);
var_dump($result);
$result = $calculator->resta(10, 4);
var_dump($result);