<?php 
require_once 'ISalonChat.php';
require_once 'UsuarioChat.php';

class SalonDeChat implements ISalonChat{
    
    private $participantes = array();
    
    public function envia(String $de, String $a, String $msj)
    {
        if(isset($this->participantes[$de]) && isset($this->participantes[$a])) {
            $user = $this->participantes[$a];
            $user->recibe($de, $msj);
        }
    }

    public function registra(UsuarioChat $user){
        $this->participantes[$user->getNombre()] = $user;
    }

}