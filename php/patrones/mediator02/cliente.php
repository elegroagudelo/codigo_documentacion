<?php 
/*
Sabemos que el patrón Mediator introduce un objeto para mediar la comunicación entre "colegas". 
Algunas veces el objeto Mediador implementa operaciones simplemente para enviarlas o otros objetos; 
otras veces pasa una referencia a él mismo y por consiguiente utiliza la verdadera delegación.
Entre los colegas puede existir dos tipos de dependencias:
Un tipo de dependencia requiere un objeto para conseguir la aprobación de otros objetos antes de hacer tipos específicos de cambios de estado.
El otro tipo de dependencia requiere un objeto para notificar a otros objetos después de que este ha hecho un tipo específico de cambios de estado.
Ambos tipos de dependencias son manejadas de un modo similar. Las instancias de Colega1, Colega2, .... están asociadas con un objeto mediator. 
Cuando ellos quieren conseguir la aprobación anterior para un cambio de estado, llaman a un método del objeto Mediator. 
El método del objeto Mediator realiza cuidadoso el resto.
Pero hay que tener en cuenta lo siguiente con respecto al mediador: Poner toda la dependencia de la lógica para un conjunto de objetos 
relacionados en un lugar puede hacer incomprensible la dependencia lógica fácilmente. 
Si la clase Mediator llega a ser demasiado grande, entonces dividirlo en piezas más pequeñas puede hacerlo más comprensible.
*/
require_once 'ISalonChat.php';
require_once 'IUsuarioChat.php';
require_once 'SalonDeChat.php';
require_once 'UsuarioChat.php';

$salon = new SalonDeChat();

$user01 = new UsuarioChat($salon);
$user01->setNombre("Pepe");
$salon->registra($user01);

$user02 = new UsuarioChat($salon);
$user02->setNombre("Alan");
$salon->registra($user02);

$user03 = new UsuarioChat($salon);
$user03->setNombre("Felipe");
$salon->registra($user03);

$user03->envia("Alan", "Hola saludos ");
$user01->envia("Felipe", "Hola saludos ");
$user02->envia("Pepe", "Hola saludos ");