<?php 
require_once 'IUsuarioChat.php';
require_once 'SalonDeChat.php';

class UsuarioChat implements IUsuarioChat {
    
    private String $nombre;
    private SalonDeChat $salon;

    public function __construct(SalonDeChat $salon){
        $this->salon = $salon;
    }

    public function recibe(String $de, String $msj){
        $str = "El usuario ". $de ." te dice ".$msj."\n";
        echo $str;
    }

    public function envia(String $a, String $msj){
       $this->salon->envia($this->getNombre(), $a, $msj);
    }

    public function getNombre(){
        return $this->nombre;
    } 

    public function setNombre(String $nombre){
        $this->nombre = $nombre;
    } 

} 