<?php 

interface IUsuarioChat {
    
    public function recibe(String $de, String $msj);
    public function envia(String $a, String $msj);
} 