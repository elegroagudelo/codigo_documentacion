<?php 
require_once 'UsuarioChat.php';

interface ISalonChat {

    public function registra(UsuarioChat $participante);
    public function envia(String $de, String $a, String $msg);

}