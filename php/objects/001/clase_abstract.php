<?php
/* son utilizadas para usarlas como clases padres
*/
abstract class clase_astrapcta
{

	function __construct()
	{
	}

	abstract protected function valor();
	abstract protected function getValor($datos);

	public function metodoComun()
	{
		$this->valor();
	}
}

class miclase extends clase_astrapcta
{

	function __construct()
	{
	}

	public function valor()
	{
		echo "el metodo de la clase abstract";
	}

	public function getValor($datos)
	{
		echo "retorna el get valor " . $datos;
	}
}

$object = new 	miclase();
$object->valor();
echo "<br/><br/>";
$object->metodoComun() . "<br/>";
