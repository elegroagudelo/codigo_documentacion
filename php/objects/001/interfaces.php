<?php

interface interfaces 
{
	public function variables($nombre, $var );
}

interface inter_hija extends interfaces
{
	public function metodo();
}


class mismetodo implements inter_hija
{
		private $vars= array();

	function __construct()
	{
		
	}
	public function metodo()
	{
		echo "metodo inter_hija";
	}

	public function variables($nombre, $var)
	{
		print_r($this->vars[$nombre]= $var);
	}

}
	$object= new mismetodo();
	$object->variables("a", 5);
	echo "<br>";
	$object->variables("b", 4);
	echo "<br>";
	$object->variables("c", 2);
	echo "<br>";
	$object->variables("d", 7);
