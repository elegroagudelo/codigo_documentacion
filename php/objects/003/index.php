<?php
require_once("MD_usuarios.php");

if ($_POST) {
	$datos_insert = array(
		'idusuario'	=> $_POST['idusuario'],
		'nombres'	=> $_POST['nombres'],
		'apellidos'	=> $_POST['apellidos'],
		'email'		=> $_POST['email'],
		'ciudad'	=> $_POST['ciudad']
	);

	$db = new MD_usuarios();
	$db->set($datos_insert);
}

?>
<!DOCTYPE html>
<html>

<head>
	<title>Objetos</title>
	<style>
		body {
			background: #ededed;
		}

		.container {
			width: 70%;
			margin: auto;
			margin-top: 40px;
			min-height: 300px;
			background: #fff;
			border: 1px solid #fff;
		}

		.cuerpo {
			padding: 20px;
		}

		.panel {
			padding: 15px;
			border: 1px solid #ddd;
			width: 40%;
		}

		input {
			width: 100%;
			display: inline-block;
		}

		.group {
			padding: 8px;
		}
	</style>
</head>

<body>
	<div class="container">
		<div class="cuerpo">
			<h4>Formulario de registro</h4>
			<div class="panel">
				<form action="" method="post">
					<div class="group">
						<label>ID</label>
						<input type="text" name="idusuario">
					</div>
					<div class="group">
						<label>Apellidos</label>
						<input type="text" name="apellidos">
					</div>
					<div class="group">
						<label>Nombres</label>
						<input type="text" name="nombres">
					</div>
					<div class="group">
						<label>Email</label>
						<input type="text" name="email">
					</div>
					<div class="group">
						<label>Ciudad</label>
						<input type="text" name="ciudad">
					</div>
					<div class="group">
						<input type="submit" value="Registrar">
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

</html>