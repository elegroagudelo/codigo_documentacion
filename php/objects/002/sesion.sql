Create database sesion;
use sesion;

create table estado(
codigo int(11)NOT NULL Primary Key,
estadopedido char(45)NOT NULL
)Engine=InnoDB; 

INSERT INTO estado (codigo, estadopedido) VALUES ('1', 'Activo');
INSERT INTO estado (codigo, estadopedido) VALUES ('2', 'Cancelado');


create table usuarios(
id int(11)NOT NULL Primary Key,
rol char(20) NOT NULL, 
usuario char(100)NOT NULL,
pass char(20)NOT NULL,
imagen char(30),
tel char(20)NOT NULL,
email char(100) NOT NULL
)Engine=InnoDB;


create table productos(
codigo int(11)NOT NULL auto_increment Primary Key,
nombre char(70)NOT NULL,
valorunidad int(15)NOT NULL
)Engine=InnoDB;

create table pedidos(
codigo int(20)NOT NULL auto_increment Primary Key,
cantidad int(11)NOT NULL,
productos_codigo int(11)NOT NULL,
usuarios_id int(11)NOT NULL,
estado_codigo int(11),
INDEX(productos_codigo),
Foreign key(productos_codigo)
References productos(codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION,

INDEX(usuarios_id),
Foreign key(usuarios_id)
References usuarios(id)
ON DELETE NO ACTION
ON UPDATE NO ACTION,

INDEX(estado_codigo),
Foreign key(estado_codigo)
References estado(codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)Engine=InnoDB;


INSERT INTO productos (codigo, nombre, valorunidad) VALUES ('100', 'Celeron Dual Core E3400.jpg', 800000);
INSERT INTO productos (codigo, nombre, valorunidad) VALUES ('200', 'Core i3 2100 3.01Ghz.jpg', 800000);
INSERT INTO productos (codigo, nombre, valorunidad) VALUES ('300', 'AMD Phenom II x4 850.jpg', 967000);
INSERT INTO productos (codigo, nombre, valorunidad) VALUES ('400', 'Asus M4N68T-M V2.jpg', 960700);
INSERT INTO productos (codigo, nombre, valorunidad) VALUES ('500', 'Athlon II X2 250.jpg', 950800);
INSERT INTO productos (codigo, nombre, valorunidad) VALUES ('600', 'Athlon II X4 640.jpg', 967000);

INSERT INTO usuarios (id, rol, usuario, pass, tel, email) VALUES ('1000', 'cliente', 'Manuel Celemin', '12345', '123467', 'manuel.celemin@gmail.com');


Create USER 'Myuser'@'localhost' IDENTIFIED BY '12345';
Grant  ALL ON sesion.* To "Myuser"@"localhost"; 