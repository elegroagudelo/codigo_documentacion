<?php 
class Listas extends CI_Model{


    public function __construct(){
        parent::__construct();
        $this->load->database();

    }

    function getClientes_id($id){ //optener un cliente
    $query = $this->db->Where('id',$id);  //consulta selectiva
    $query = $this->db->get("usuarios");
    return $query->result();
    }


    public function get_productos(){
    return  $this->db->get("productos")->result();
    }

    public function post_productos(){
    $query= $this->db->query("SELECT * FROM productos WHERE 1");
    return  $query->result();
    }

    public function getproductos_id($codigo){ //optener un cliente
    $query = $this->db->Where('codigo',$codigo);  //consulta selectiva
    return $query = $this->db->get("productos");
    }

   function addpedidos($datos){
    $this->db->insert('pedidos',$datos);
    return $this->db->insert_id();
    }

    function usuario($id, $pass){
    $query= $this->db->query("SELECT id, rol, usuario, pass, email, imagen FROM usuarios WHERE id ='$id' and pass ='$pass'");
    return $query; 
    }

    public function getpedidos_id($codigo){ //optener un cliente
    $query= $this->db->query("SELECT pedidos.codigo as codigo, cantidad, productos.nombre as producto,
    usuarios.id, usuarios.usuario as user,  estado.estadopedido as estado  FROM pedidos 
    inner join usuarios on pedidos.usuarios_id = usuarios.id  
    inner join productos on pedidos.productos_codigo = productos.codigo  
    inner join estado on  pedidos.estado_codigo = estado.codigo 
    WHERE usuarios_id='$codigo'");
    return  $query->result();
    }


}