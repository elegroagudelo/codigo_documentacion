<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Control extends CI_Controller {

	    public function __construct()
    {
        parent::__construct();
        $this->load->library('parser');
        $this->load->model('listas');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('table');
        $this->load->library('form_validation');
        $this->load->helper(array('download', 'file', 'url', 'html', 'form'));
    }
   
   public function login(){
    $msg= "El usuario no es correcto";
   	if ($_POST) {
   		$id= $_POST['id'];
   		$pass= $_POST['pass'];
   		$query= $this->listas->usuario($id, $pass);
   		if ($query->num_rows()> 0) {
   			foreach ($query ->result()  as $row) {
   				$nom= $row->usuario;
   				$rol= $row->rol;
   			}
   		
      $this->sessiones($id, $pass, $nom, $rol);
   		if ($this->session->userdata('rol')=="cliente") {
   			redirect('control/productos');
   		}else{ 
        $con['msg']=$msg;
        $carga['contenido']= $this->load->view('login',$con, TRUE);
        $this->load->view('index', $carga);}

   		}else{ 
        $con['msg']=$msg;
        $carga['contenido']= $this->load->view('login',$con, TRUE);
        $this->load->view('index', $carga);}
   	}else{ 
      $carga['contenido']= $this->load->view('login','', TRUE);
      $this->load->view('index', $carga);}
   }

   	public function index(){
   		redirect("control/pedidos_clientes");
	}

 	public function	pedidos_clientes(){
  if ($this->session->userdata('rol')=="cliente") {
  $usuario= $this->session->userdata('identidad');
 	if ($this->uri->segment(3)) {
 	$codigo=$this->uri->segment(3);
 	$query= $this->listas->getproductos_id($codigo);
  
  if ($query->num_rows()> 0) {
        foreach ($query->result() as $row) {
          $nom= $row->nombre;
          $valor= $row->valorunidad;
        }
  $pedido = array(
    'cantidad' => 1,
    'productos_codigo' => $codigo,
    'usuarios_id' => $usuario,
    'estado_codigo'=> 1
  );
  $this->listas->addpedidos($pedido);
	}
 	  }

  $con['query']= $this->listas->getpedidos_id($usuario);
  $contenido= $this->load->view('admin/pedidos_clientes',$con, TRUE);
	$carga['contenido']= $contenido;
	$this->load->view('index', $carga);
 	}else{ redirect("control/login");}
 }
 
 	public function	productos(){
    if ($this->session->userdata('rol')=="cliente") {
 	$data['query']= $this->listas->post_productos();
	$contenido= $this->load->view('admin/productos',$data, TRUE);
	$carga['contenido']= $contenido;
	$this->load->view('index', $carga);
 	}else{ redirect("control/login");}
 }


//SESSION
	function sessiones($id, $pass, $nom, $rol){ //funcion para crear session
	$work = array(
		'identidad' => "$id",
		'usuario' => "$nom",
		'pass' => "$pass",
		'rol'=> "$rol",
		'logeado' => TRUE );
	return  $this->session->set_userdata($work);
	}

	function cerrarsession(){
	$this->session->unset_userdata('carrito');
	$this->session->sess_destroy();
	redirect("control/login");
	}

}
