<?php

//Construccion de array
$personas =array ('tom','kate','ivan');
$personas[0]= 'tom';
$personas[1]= 'kate';
$personas[2]= 'ivan';  	

echo " Con mi array ", $personas[1],' ', $personas[0],' ', $personas[2];

//Array asociativos 
echo "<br/>";

$actions =array('fingers'=>'hand', 'toes'=>'foot', 'teeth'=>'mouth');

echo $actions['fingers'];
echo "<br/>";
echo $actions['toes'];
echo "<br/>";
echo $actions['teeth'];

//adicionar elementos al array o modificarlos

$name[0]= 'edwin';
$name[1]= 'andres';
$name[2]= 'carlos';

echo "<br/>", $name[0];

$name[0]= 'ivan'; //permite modificar y cambiar el valor de [0].

echo "<br/>", $name[0];
	

//array con loops 

$grupo =array('tom', 'ivan', 'andres', 'carlos');

for ($i=0; $i < sizeof($grupo); $i++) { /* señalamos con sizeof la cantidad de elementos que hacen parte del array */
	 echo  "<br/>",$grupo[$i]; /*de este modo poedo mostrar todos los valores de mi array */
}

//Uso de foreach
echo "<br/>";
echo "<br/>";
echo "<br/>";


// foreach = Para cada uno
foreach($grupo as $x){
echo '<br/>',$x;

	} 

echo "<br/>";
echo "<br/>";


$array = array(1, 2, 3, 4);

foreach ($array as &$valor) {
    $valor = $valor * 2;
	echo $valor;
}


echo "<br/>";
echo "<br/>";

foreach ($grupo as $clave => $valor) {
    echo "Clave: $clave; Valor: $valor<br />\n";
}
/*
Clave: 0; Valor: tom
Clave: 1; Valor: ivan
Clave: 2; Valor: andres
Clave: 3; Valor: carlos
*/
?>
