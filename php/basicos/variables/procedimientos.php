<?php
	
	/*Funcion: 
	* Es la manera en que un programa grande, se puede fragmentar en mas pequeñas partes, para que este sea mas legible, 
	* y en consecuencia mas facil de mantener.
	* Son fragmentos de programa, encerrados entre llaves, que realizan una operacion  o varias. si retorna  un valor
	* se le define como Funcion  sino es un procedimineto.  
	*/

	/*Primero un procedimiento*/
	$n1 = 12;
	$n2 = 10;
	suma($n1, $n2);

	function suma($num1, $num2) /*la funcion resive dos parametros para realizar las operaciones pertinentes*/
	{
			$result = $num1+$num2;
	echo 'El resultado del procemiento es: ',$result,'</br>';
	}/*en este caso no devuelve nada */


	/* Segundo uno que si devuelve  seria: */
	function resta($num1, $num2)
	{
			$result = $num1-$num2;
			return $result; /*en este caso se hace uso de la instruccion return */
		}
		
		$resultado = resta($n1, $n2);
		echo 'El resultado de la resta  con funcion es: ',$resultado; 
		/* en este caso en vez de mostrar un mensaje por  pantalla, lo que hace es retornar el resultado y para resivirlo se requiere 
		* de una variable que lo contenga*/

		/*si a un procedimiento o funcion no se le tuviese que enviar parametros, simplemente se colocan parentesis
		*
		*/

		/*el siguiente es un procedimiento  sin parametros*/
		saludo();
		function saludo()
		{
			echo '</br> Hola mundo ';
		}

		/*Este es una funcion sin parametros*/

		function calcular()
		{
			$num1= 10;
			$num2= 2;
			$multi= $num1*$num2;
			return $multi; /*se retorna el valor de la operacion a la funcion*/
		}
		$R = calcular(); /*la variable $R resive el resultado de la operacion*/
		echo '</br>El resultado es: ',$R;

		/*ahora seguimos con  con una forma de enviar parametros por referencia, pudiendose asi modificar su valor*/

		function SumaUno(&$uno, &$dos)
		{
			$uno++; /*suma 1*/
			$dos= $dos+1; /*suma 1*/
		}

		$a=1;
		$b=1;
		echo '</br>',"la variable vale: ", $a, ' la b vale ', $b ,'</br>';
		SumaUno($a, $b);
		echo "la variable vale: ", $a, ' la b vale ', $b ,'</br>';
		/* Se utiliza   uno y dos como referencia a las variables que se les envia  y 
		* devuelven las modificaciones sufridas*/


		/*Ambito local de una variable  :
		* siendo aquellas que se definen dentro de la funcion  y tienen valor solo dentro, por lo cual se les denomina
		* de ambito local.   
		*/


		

		/*$sQuery ="INSERT INTO tb_usuario (id, Usuario, Contraseña, Nombres, Apellidos, Rol)VALUES('".$id."', '".$user."', '".$pass."', '".$nom."', '".$ape."', '".$rol."'"; 
 		*$result = mysql_query($sQuery,$conne);
 		*--------------------------------------------------------------------------------*/
 		/* EJERCICIOS*/

 		$misuma = sumas(); 
 		function sumas()
 		{
 			$num1 = 12;
 			$num2 = 20;
 			$result = $num1 + $num2;

 			return $result;
 		}
 			echo "<br/>La suma es: ", $misuma;


 			$miresta = restas();
 			function restas()
 			{
 				$num1 = 22;
 				$num2 = 13;
 				$result = $num1 - $num2;
 			return $result;
 			}
 				echo "<br/>la resta es: ", $miresta;


 				$midivicion = divide($misuma, $miresta);
 				function divide(&$s, &$r)
 				{
 					$result = $s / $r;
 					return $result;
 				}
 				echo "<br/>la divicion es: ", $midivicion ;

 /*-----------------------------------------------------------------------------------------*/
 				/*INCLUSION DE ARCHIVOS DE FORMA CONDICIONAL 
 				*$cond = 5;
	 			*	if $cond == 5 
 				* {
 				*	include('Conexion.php'); /*el Include: permite que en tiempo de ejecucion se incluya un archivo
 				} */
 /*-----------------------------------------------------------------------------------------*/

 				


?>