<!DOCTYPE html>
<html>
<head>
	<title>Formatode de fecha</title>

</head>
<body>
<?php
// Se asume que hoy es March 10th, 2001, 5:16:18 pm, y que estamos en la
// zona horaria Mountain Standard Time (MST)


$hoy = date("F j, Y, g:i a");                 // March 10, 2001, 5:16 pm
echo "<br/>",$hoy;
$hoy = date("m.d.y");                         // 03.10.01
echo "<br/>",$hoy;
$hoy = date("j, n, Y");                       // 10, 3, 2001
echo "<br/>",$hoy;
$hoy = date("Ymd");                           // 20010310
echo "<br/>",$hoy;
$hoy = date('h-i-s, j-m-y, it is w Day');     // 05-16-18, 10-03-01, 1631 1618 6 Satpm01
echo "<br/>",$hoy;
$hoy = date('\i\t \i\s \t\h\e jS \d\a\y.');   // it is the 10th day.
echo "<br/>",$hoy;
$hoy = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001
echo "<br/>",$hoy;
$hoy = date('H:m:s \m \i\s\ \m\o\n\t\h');     // 17:03:18 m is month
echo "<br/>",$hoy;
$hoy = date("H:i:s");                         // 17:16:18
echo "<br/>",$hoy;
$hoy = date("Y-m-d H:i:s");                   // 2001-03-10 17:16:18 (el formato DATETIME de MySQL)

echo $hoy;

?>

</body>
</html>

<?php
	/*Como puedo generar FECHAS EN ESPAÑOL*/
setlocale(LC_ALL,"es_ES");
echo date('l, d M Y');/*Para ello usamos la funcion setlocale la cual establece la informacion de la configuracion regional, nos ofrese traducciones de cosas basicas como la fecha*/
echo "<br/>";
/*si usamos SETLOCALE tambien es necesario usar la funcion STRFTIME la cual formatea una fecha/hora local   */

setlocale( LC_TIME, 'spanish' );
echo "<br/><br/>";
echo strftime("%A %d de %B del %Y");


$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

echo "<br/>";
echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

echo "<br/>";
/*utilidad de la funcion STRTOTIME  de una fecha en ingles la pasa a formtado Unix*/
echo strtotime("now"), "\n";
echo "<br/>";
echo strtotime("10 September 2000"), "\n";
echo "<br/>";
echo strtotime("+1 day"), "\n";
echo "<br/>";
echo strtotime("+1 week"), "\n";
echo "<br/>";
echo strtotime("+1 week 2 days 4 hours 2 seconds"), "\n";
echo "<br/>";
echo strtotime("next Thursday"), "\n";
echo "<br/>";
echo strtotime("last Monday"), "\n";

echo "<br/>";

/*Constructor foreach SOLO aplica para los array con el manupulamos los atributos que posea*/
$array = array(1, 2, 3, 4);

foreach ($array as &$valor) {
    $valor = $valor * 2;
}
// $array ahora es array(2, 4, 6, 8)
unset($valor); // rompe la referencia con el último elemento

/*Las opciones de fecha o mez las podemos consultar con un input de este tipo  MONTH*/
echo "<input type='month' name='month'>";

/*El USO DE COLSPAN:
* permite combinar celdas  para insertar textos dentro de una tabla
*/

echo "<table style='border:1px solid black;'><tr> <th>Month</th><th>Savings</th></tr> <tr><td>January</td><td>$100</td></tr><tr><td>February</td>
    <td>$100</td> </tr><tr><td colspan='2'>Sum: $180</td></tr></table>"; /* lo aplicamos a la ultima fila*/


$meses = array ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
echo "Fecha actual: " . date("d-m-Y") . "<br>";
echo "Dia del a&ntilde;o: " . date("z") . "<br>";
echo "mes ". date("n"). "<br>";
echo "Estamos en el mes: " . $meses[date("n") ] . "<br>";


$colores = array("fuertes","suaves");
$colores["fuertes"] = array("rojo" => "FF0000", "verde" => "00FF00", "azul" => "0000FF");
$colores["suaves"] = array("rosa" => "FE9ABC"," amarillo" => "FDF189" , "malva" => "9A2F68");

echo $colores["fuertes"] ["rojo"];
print_r($colores). "</br>";


$color = array( "fuertes" => array( "rojo" => "FF0000","verde" => "00FF00","azul" => "0000FF"),
				"suaves" => array( "rosa" => "FE9ABC","amarillo" => "FDF189","malva" => "9A2F68"));
echo $color["fuertes"] ["rojo"];
?>