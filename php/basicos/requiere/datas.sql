
CREATE DATABASE planilla;
USE planilla;


CREATE TABLE empleado (
  id_empleado int(11) NOT NULL auto_increment PRIMARY KEY,
  nombre varchar(150) NOT NULL,
  apellido varchar(150) NOT NULL,
  telefono varchar(20) NOT NULL,
  cargo varchar(100) NOT NULL,
  sueldo varchar(30) NOT NULL
) ENGINE=InnoDB;

