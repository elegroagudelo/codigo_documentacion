<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Guardar Formulario</title>
<style type="text/css">
#header {
	background-color: #1698d5;
	height: 150px;
	position: absolute;
	left: 0px;
	top: 0px;
	right: 0px;
}
#formulario {
	width: 600px;
	position: absolute;
	top: 151px;
	left: 50%;
	margin-left: -300px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #333;
}
#header #titulo {
	color: #FFF;
	text-align: center;
	top: 30px;
	position: relative;
}
</style>
</head>

<body>
<div id="header">
  <h1 id="titulo">Mostrar registros en MYSQL con PHP</h1></div>

<div id="formulario">
 <?
 
//Invocamos el archivo de conexión

require ("conexion.php");

//Estblecemos el query o consulta mysql y lo almacenamos en una variable denominada $result, hay que resaltar que utilizamos mysqli_query y como parámetro colocamos la cadena de conexion que obtenemos del archivo conexion.php
$result = mysqli_query($enlace, "SELECT * FROM empleado");
 
//ahora vamos construyendo la tabla, la primera fila siendo mas preciso esta es posible construirla toda en pocas lineas, lo coloco así para una mejor comprensión
echo "<table border='1' align='center'>";
echo "<tr bgcolor='#CCCCCC'>";
echo "<td><b>Nombres</b></td>";
echo "<td><b>Apellidos</b></td>";
echo "<td><b>Telefono</b></td>";
echo "<td><b>Cargo</b></td>";
echo "<td><b>Sueldo</b></td>";
echo "</tr>";
 
//Con la condicional while obtenemos los datos, mysqli_fetch_array() nos permite hacer esto, solo colocamos la variable que contiene esta con el nombre de la columna y nos arroja el dato, y como while es una condicional repetitiva y a la vez estamos construyendo la tabla, nos resultara una tabla con los datos ordenados según lo consultemos.
while ($row = mysqli_fetch_array($result)){
	echo "<tr>";
	echo "<td>".$row[nombre]."</td>";
	echo "<td>".$row[apellido]."</td>";
	echo "<td>".$row[telefono]."</td>";
	echo "<td>".$row[cargo]."</td>";
	echo "<td>".$row[sueldo]."</td>";
	echo "</tr>";
// Cerramos el While	
}
 
//Notemos que colocamos ". y ." para concatenar las variables php y HTML
//Cerramos la tabla, es necesario hacerlo fuera del while, de los contrario solo mostraría la segunda fila dentro de la tabla el resto estaría desordenado.
echo "</table>";
// mysqli_close() es el evivalente a mysql_close() sirve para finalizar la conexión.
mysqli_close($enlace);
?>
</div>

</body>
</html>
