<?php
		
		echo '<html><head><title>Operaciones </title></head><body>';
		
		echo '<form method="get" action ="index.php">';
	
		/*---- Usar variables del sistema */
		echo $_SERVER['HTTP_USER_AGENT']; /* Esta variable permite optener informacion del navegador, sistema operativo
		* al optener esta informacion podemos redireccionar la pagina teniendo encuenta el sistema operativo, su version y 
		* su navegador
		*/

		echo "<br/><br/>",$_SERVER['HTTP_ACCEPT_LANGUAGE'];/* Nos permite devolver informacion del lenguaje que el
		* navegador considera principal
		* con ella podemos redireccionar la pagina a otra con la lengua del cliente.si exixte la pagina.
		*/

		/*echo "<br/><br/>", $HTTP_REFERER; // Nos indica desde que pagina viene nuestro visitante, muy util
		* para crear botones de tipo atras.
		*/
		echo "<br/><br/>",$_SERVER['PHP_SELF']; /* nos devuelve una cadena de caracteres con la informacion 
		* sobre el script que se esta utilizando en este momento. usado para crear botones de recarga.
		*/

		/*echo "<br/><br/>",$_SERVER['HTTP_GET_VARS']; // nos devuelve informacion referente a las variables pasadas por 
		*el metodo get.
		*
		* echo "<br/><br/>",$_SERVER['HTTP_POST_VARS']; //lo mismo con lo anterior
		* echo "<br/><br/>",$_SERVER['HTTP_COOKIES_VARS']; 
		* echo "<br/><br/>",$_SERVER['PHP_AUTH_USER']; // nos permite almacenar informacion referente a la variable
		* usuario cuando entramos  a un sitio web restringido. permite controlar accesos. 
		* echo "<br/><br/>",$_SERVER['PHP_AUTH_USER']; // almacena la informacion del password.*/
		/*
		*/ 
		echo "<br/><br/>",$_SERVER['REMOTE_ADDR']; /* devuleve la direccion ip del cliente.
		* 
		*/
		echo "<br/><br/>",$_SERVER['DOCUMENT_ROOT'];/* devuleve la direccion fisica de la pagina 
		*/ 
		/* echo "<br/><br/>",$_SERVER['PHPSESSID'];  conserva la identificacion de sesion del usuario
		*/

		/*-----------------------------------------------------------------------------------------------*/ 
		
		/*VARIABLES GLOBALES
		*aquellas que facilitan informacion del sistema estan definidas en los siguientes array:
		$_SERVER
		$_ENV
		$_POST
		$_GET
		$_SESSION
		$_COOKIES

		VARIABLES CONSTANTES

		usamos DEFINE como: 
		*/
		define("EDITORIAL", "Edwin constante");
		echo  "<br/><br/>",EDITORIAL;
		/* 
		* 
		DEFINIR VARIABLES:
		INTEGER
		FLOATING POINT NUMBER
		ARRAY
		OBJECT
		*/

		/*NUMEROS ENTEROS:
		*Son los numeros negativos o positivos incluyendo el 0 
		*/
		$decimal= 9;
		echo "<br/>",$decimal;

		/*un Octal  precedidos siempre de un 0*/
		$Octal=031;
		echo "<br/> Octal ",$Octal;

		/*Hexadecimal precedido de 0X*/

		$Hexadecimal= 0X3f;
		echo "<br/> Hexadecimal ",$Hexadecimal;

		/*para cambiar de base decimal a base octal*/
		$num = 1020;
		echo "<br/> cambiar de decimal a octal ",decoct($num);


		/*para cambiar de base decimal a base Hexadecimal*/
 		echo "<br/> cambiar de decimal a Hexadecimal ", dechex($num);

		/*Cambiar de decimal a binario */
		echo "<br/> de decimal a binario ", decbin($num); 		

		/*Cambiar de octal a decimal */
		echo "<br/>  cambiar de octal a decimal ", octdec(01774);

		/*Cambiar de Hexadecimal a decimal*/

		echo "<br/> cambiar de hexa a decimal ", hexdec(0X3f);


		/* De binario a decimal*/
		echo "<br/>cambiar de binario a decimal ", bindec(1111110101);


				/*FLOATING POINT NUMBER:
				* Numeros reales, muy necesario en caso de que los decimal es sena infinitos  tras una divicion. 1/9 = 0,111111... 
				*/
				$a = 5.123;
				echo "<br/>",$a;
				$b = 5.4E2;
				echo "<br/>",$b;
				$c = 2E10; /*20000000000 con 10 ceros*/
				echo "<br/>", $c;


				/*STRING  O CADENA DE CARACTERES*
				* Con Comillas dobles: 
				  Me permite introducir variables dentro de estas...
				*/
				  echo "<br/>Hola este es un ejemplo de comillas dobles $a, $b, $c ";

				 /*las comillas simples no nos permite espandir  e introducir variables dentro pero si etiquetas Html*/
				 echo  '<br/>Este es un ejemplo de comillas simples';


				 /*MULTILINEA CON HEREDOC
				 * similar a las comillas dobles pero permitiendo  varias lineas,  
				 * posee un identificador de tres palabras precedido de (<<<) ; 
				 */

$pi = "3.1416";
$mit=<<<END
<p> El numero pi tiene  valor  se debe colocar la siguiente variable al inicio del documento y luego (;) <p>. $pi
END;
echo $mit;
/*El NOWDOC  finciona  lo mismo que el HEREDOC  solo que toca  colocar comillas simples al <<<'END'*/

				/*CONCATENAR  SE HACE CON  UN PUNTO (.)*/
				$cadena1 = 'Hola';
				$cadena2 = ' Mundo';
				echo "<br/>", $cadena1.$cadena2;

				/*ARRAY*/
				$familia = array('Padre' => "Angel", 'Madre' => "Agela", "hermana"=>"sonia" );
				print_r($familia);


				/*Tambien puedo agregar mas datos al array*/
				$familia['esposa']= "Maria";
				echo("<br/>");
				print_r($familia);


				/*array escalar  //genera un numero */
				$alumnos= array ("Pepe","Juan","Anselmo");
				print_r($alumnos);
				$alumnos[]="Jose";
				print_r($alumnos);
				
				/*Opciones con array
				* Elimnar un campo del array
				*/

				$alumnos = array_slice($alumnos, 0,4);
				echo "<br/>";
				print_r($alumnos);


				/*array_shift: elimina el primer elemnto de la tabla */

				$alumnos = array_shift($alumnos);
				echo "<br/>";
				print_r($alumnos);


				/*Cambio FORZADO*/

				$string = "45";
				$numero = (Int)$string;
				echo "<br/>";
				print_r($numero);
				echo "<br/>";
				echo Gettype($numero); /* con Gettype puedo reconocer una variable si esta lo es*/
				echo "<br/>";
				echo "<br/>";
				
				/*Operaciones de asignacion*/
				$a= 10;
				$a+= 20;
				echo "el valor es ",$a ;




		echo '</form></body></html>';
		
		
	



?>