
Novedades de PHP8
======

Menos lineas
----
Manejo de las property.

```
    class Usuario {

        public function construct(
            public string $nombres,
            public string $apellidos,
            public int $documento,
            public string $username,
            public bool $estado
        ){
            #....
        }
    }
```

Con ello se logra una sintaxis más elegante, conciso y eficiente del código, para definir las property y el ambiento de aplicación si son: (privadas, public, static, protected).

El proposito de PHP8 es:

* Evitar estructuras anidadas o código estructurado dentro de la programación orientada a objetos. Puntualmente podemos escribir en una unica linea lo que antes requeria un bloque anidado de condiciones.

```
    <?php

    ----
    if ($usuario->perfil) {
        echo $usuario->perfil->email;
    }

    Misma sintaxis
    ---- 
    echo $usuario?->perfil?->email;
```

ClOSURES
===

Funciones anonimas que se asignan a una determinada varible.    

```
    # fn(parametros) => expression

    $data = function ($num) {
        return $num;
    }

    $data = fn($num) => $num;

```

ARRAY
===

Los arrays son más simples en su sintaxis.  

```
    <?php 

    $data = [1, 23];

    function example($num1, $num2){
        var_dump($num1, $num2);
    }

    example(...$data);
```

Con el fin de remplazar el uso de func_get_args(), usamos el operador ...$argumentos
```
    <?php 

    function example(...$argumentos){
        var_dump($argumentos);
    }

    example(1, 23, 25);
```

Forzar los tipos de datos
------
Usando código directo y simple, esta sintaxis hace que todo sea más compacto y limpio.       

```
    <?php 

    #forzado de los argumentos al tipo entero si o si
    function example(int ...$argumentos){
        var_dump($argumentos);
    }

    example(1, 23, 25);
```

Otro ejemplo de aplicación
```
    <?php 

    $numeros = [1, 23];

    function example(int $num1, int $num2){
        var_dump($num1, $num2);
    }

    example(...$numeros);
```

Tambien
```
    <?php

    function example(int ...$argumentos){
        var_dump($argumentos);
    }

    example(1, 23, 45, 34);
```


Remplazar el uso de list()
-----

```
    <?php

    $data = [ 2, 34, 56, 34];

    [$num1, $num2, $num3, $num4] = $data;
    
    var_dump($num1, $num2, $num3, $num4);
```

Desestructurar Los Array
----
```
    <?php

    $data = ["nombre"=>"Edwin", "apellido"=>"Legro", "email"=> "edwin@gmail.com"];

    ['nombre' => $nombre_user, 'apellido'=> $apellido_user, 'email'=> $email_user] = $data;

    echo $nombre_user;
    echo $apellido_user;
    echo $email_user;

    [
        'HTTP_HOST' => $host,
        'SERVER_PORT'=> $port
    ] = $_SERVER;

    var_dump($host, $port);
```

Otros casos impotantes de aplicación.
```
    <?php

    $data = [
        ["nombre"=>"Edwin"],
        ["nombre"=>"Andres"],
        ["nombre"=>"Felipe"],
        ["nombre"=>"Alan"]
    ];

    #usando array
    foreach ($users as ['nombre' => $name]) {
        echo $name;
    }

    #usando list
    foreach ($users as list('nombre' => $name)) {
        echo $name;
    }
```

Expresiones MATCH
----
