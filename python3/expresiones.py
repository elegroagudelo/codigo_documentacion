#uso de expresiones regulares
#regex
#patrones para componer una expresion
# .  es un caracter excepto nueva linea
# \n nueva linea
# \r  
# \w cualquier caracter numero o alafa
# \W mayus cualquier caracter numero o alafa
# \S cualquier que noes espacio en blanco
# \D cualquier que no es numero
# ^ inicio de cadena
# \ escape de caracteres especiales
# [] cualquier que se encuentre
# \b separacion entre numero y letras

import re 

busqueda = re.search(r"\d\d\d", "Kilometro 998")
busqueda = re.search(r"^[K]", "Kilometro")
print(busqueda)

#crear un patron
patron = re.compile("\d\d\d")
print(patron.search("Kilometro 998").group())

if(re.search(r"^[K]", "Kilometro")):
    print('OK')


#sustitucion

salida = re.sub(r"\d", "_", "el nombre del archivo 122324 es valido")
salida = re.sub(r"\d", "_", "el nombre del archivo 122324 es valido", 2) #los primero 2 caracteres 
print(salida)
#permite replazar los numero por _

#Modificadores
regex = re.compile(r"ab", re.IGNORECASE)
regex.search("el nombre es Alan Babel")

