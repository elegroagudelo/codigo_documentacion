""" nested functions >> son las funciones anidadas """
#crear cierres o closures

def metodoA(x):
    def metodoB(y):
        return  x * y
    return metodoB()


fun = metodoA(5) #pasa al metodo inicial
print(fun(3)) #luego accede al metodo B

#Decoradores
#cambia la implementacion del metodo
#se ejecuta lo que haga el decorador
def modecorador(funcion):
    def funcionDecorador(*args, **kkwars):
        print("Primer  decorador")
    return funcionDecorador



@modecorador
def funcion():
    print("Mi primer decorador")

funcion()



#Programacion funcional (paradigma) (declarativo)
def lower(elementos): return elementos.lower()

lista= ["Edwin", "AndRES", "ALAN"]
print(list(map(lower, lista)))
#pasa todos los elementos a minusculas

#mismo resultado
print( [cad.lower() for cad in lista])


#funcion de orden superior
def saludo(idioma):
    def saludo_es():
        print("Hola")
    def saludo_en():
        print("Hi")
    
    idioma_fun = {
        "es": saludo_es,
        "en": saludo_en 
    }
    return idioma_fun[idioma]

saludar = saludo("es")
saludar()



#programacion funcional
#modulo functools clase reduce
from functools import reduce 

numeros = (1,2,3,4,5,6)
def suma(x, y):
    return x+y

#permite sumar de forma recursiva cada elemento de la tupla
sumar = reduce(suma, numeros)
print(sumar)



""" Closure, tipo de caso especial """
""" Cuando se tienen una clase que solo posee un metodo, o cunaod se usan decoradores """
""" Donde la varible x pertence a orden superior del scope y es recordada simpre que se llame"""
def make_multiplayer(x):
    def multiplayer(n):
        return x * n 
    return multiplayer


times10 = make_multiplayer(10)
times4 = make_multiplayer(4)

print(times10(5))
print(times4(6))
print(times4(times10(2)))