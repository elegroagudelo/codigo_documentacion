# unidad de procesamiento más pequeña que se puede ejecutar
# de forma aislada, como tareas
# el espacio de memoria es compartido
# siempre debe haber un hilo principal y otros subhilos o tareas
#permite lanzar diferentes tareas al mismo tiempo
import threading
import time

class MiHilo(threading.Thread):
    
    def run(self):
        print("{} arranca el hilo".format(self.getName()))
        time.sleep(2)
        print("{} terminado".format(self.getName()))
    

if __name__ == "__main__"
    for x in range(4):
        hilo = MiHilo(name="Thread-{}".format(x+1))
        hilo.start()
        time.sleep(.5)



#Procesos: todos disponen de espacio de memoria