""" Lambda, funciones anonimas, posee una sola linea de codigo"""
""" Usa la variable que aloja una funcion lambda"""
def run2():
    palindrome = lambda string : string == string[::-1]
    print(palindrome("ana"))


""" Una funcion de orden superior, son funciones que resiben como parametro otra funcion """
""" son: filter, map, reduce """
""" me permite filtrar datos, donde los parametros de lambda son obtenidos de la lista """
def run():
    mylis = [11,9,10, 12, 23,5,67,7]
    odd = list(filter(lambda x: x % 2 != 0, mylis))
    print(odd)


if __name__ == '__main__':
    run()
