""" Iterables, estructuras de datos, adicionales de python """
""" Los iterables son todos los objetos que se pueden recorrer en un ciclo, listas, strings, diccionarios """


if __name__ == "__main__":
    milista = [11,2,4,54,67,2]
    """ Permite recorrer una lista usando el iterador """
    milista = iter(milista)
    print(next(milista))

    while True:
        try:
            elemento = next(milista)
            print(elemento)
        except StopIteration:
            break
    
    for _i in milista:
        print(_i)