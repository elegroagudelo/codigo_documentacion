import json

with open("archivo.json") as file:
    data = json.load(file)

print(data)
print(data['clientes'])


#comprimir o descomprimir archivos
import zipfile
from zipfile import ZipFile

with zipfile.ZipFile("archivo_crear.zip", 'w') as fzip:
    fzip.write("archivo1.json")
    fzip.write("archivo2.txt")
    fzip.printdir()


#descomprimir
import zipfile
from zipfile import ZipFile

with zipfile.ZipFile("archivo_crear.zip", 'r') as fzip:
    fzip.extractall()


#Formato gzip
#primero lo abrimos y luego escribimos
import gzip

with open("archivo1.json", 'rb') as original:
    with gzip.open("archivo.gz", "wb") as archivo1:
        archivo1.writelines();

#Formato bz2
import bz2

cadena =b"cadena aqui presente para comprimir";
cadena_comp = bz2.compress(cadena)
print(cadena_comp)

print(bz2.decompress(cadena_comp))


#Comprimir en tar
import tarfile

archivo_tar = tarfile.open("archivo_comp.tar.gz", "w:gz")
archivo_tar.add("Documento.docx")
archivo_tar.add("datos.csv")
archivo_tar.close()
