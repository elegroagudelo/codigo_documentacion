""" set, es una conjunto de elementos unicos e inmutables y desordenados """


def setts():
    """ similar a los diccionarios pero sin claves, solo valores """
    myset = { 1, "Hola", 4.5, "Saludos" }
    """ Los repetidos son eliminados """

    myset = { 4.5, "Hola", 4.5, "Saludos" }

    myset = {} # es un diccionario
    myset = set() #crea un set
    
    myset = {[1,2,3], 45, 100} # no puede aplicar como set, ya que es mutable la lista

    lista = [1,1,2,3,4,5,6]
    myset = set(lista) #borra los repetidos de la lista

    tupla = (1,1,2,3,3)
    myset = set(tupla) #borra los repetidos de la lista

    """ agregar más elementos a un set """
    myset.add(4)
    lista2 = {3,4,5,6,7,8}
    tupla2 = (9, 10)
    myset.update(lista2, tupla2)

    """ remover elementos del set """
    myset.discard(1) #borra el valor 1, asi no existan
    
    myset.remove(1) #borrar elementos solo existentes

    myset.pop() #borra un elemento

    myset.clear() # borra todo


def operaciones():
    set1 = set([1,1,2,3])
    set2 = set([1,4,5,6])
    """ union de set """
    myset = set1 | set2

    """ la interseccion, solo elementos en comun """
    myset = set1 & set2

    """ sacar la diferencia de un set"""
    myset = set1 - set2

    """ diferencia simetrica, todos los que no se repiten """
    myset = set1 ^ set2

if __name__ == "__main__":
    setts()
