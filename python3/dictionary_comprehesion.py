
def run2():
    rango = range(1, 101)
    mydic = {}
    for i in rango:
        if i % 3 != 0: 
            mydic[i] = i**3  
    print(mydic)

""" { key:value for value in iterable if condition} """
""" para cada elemento en un iterable se crea una llave y un valor si se comple la condicion"""
def run3():
    rango = range(1, 101)
    mydic = {'item{}'.format(i): i**2 for i in rango if (i % 2) == 0 }
    print(mydic)


def run4():
    rango = range(1, 1001)
    mydic = {}
    for i in rango:
        for j in rango[0:i]:
            if (j**2) == i:
                mydic[i] = j
                break 

    print(mydic)


def run():
    rango = range(1, 1001)
    mydic = {'i{}'.format(i): round(i**0.5, 2) for i in rango }
    print(mydic)


if __name__ == '__main__':
    run()