from datetime import   datetime

def calcular_tiempo_ejecucion(func):
    def tiempo_ejecucion():
        inicia = datetime.now()
        func()
        final = datetime.now()
        diff = final - inicia
        print("La diferencia de tiempo es "+str(diff.total_seconds())+" en segundos ")
    return tiempo_ejecucion

def calcular_tiempo_ejecucion2(func):
    def tiempo_ejecucion(*argv, **kwargv):
        inicia = datetime.now()
        func(*argv, **kwargv)
        final = datetime.now()
        diff = final - inicia
        print("La diferencia de tiempo es "+str(diff.total_seconds())+" en segundos ")
    return tiempo_ejecucion


@calcular_tiempo_ejecucion
def proceso_principal():
    for _i in range(1, 10000000):
        pass

@calcular_tiempo_ejecucion2
def sumar(a: int, b: int) -> int:
    for _i in range(1, 10000000):
        pass
    print("res suma :" +str(a + b))


if __name__ == "__main__":
    #proceso_principal()
    sumar(4, 5)