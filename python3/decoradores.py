""" Decorador, es una funcion closure que recibe como parametro otra funcion, añade opciones, ejecuta y retorna"""
""" 
def decorador(func):
    def envoltura():
        print("funcion anidada de envoltura")
        func()
    return envoltura

def saludo():
    print("Hola") 

saludar = decorador(saludo)    
"""


def decorador(func):
    def envoltura(texto):
        print("funcion anidada de envoltura")
        func(texto.upper())
    return envoltura

@decorador
def saludo(msj):
    print(f"Hola, {msj}")


if __name__ == "__main__":
    saludar = saludo("saludo edwin")
