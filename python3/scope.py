""" Alcance las variables """

clave = "variable global"

""" Cada scope es independiente, y las varibles globales no se ven afectadas"""
def metodo1():
    clave = "variable local 1"
    print(clave)

def metodo2():
    clave = "variable local 2"
    print(clave)

def metodo3():
    clave = "variable local 3"
    def metodo_anidado():
        clave = "variable local anidada"
        print(clave)    
    metodo_anidado()
    print(clave)


if __name__ == "__main__":
    metodo1()
    metodo2()
    print(clave)
    metodo3()