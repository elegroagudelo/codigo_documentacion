def divicion_por_n(n: int) -> float:
    def divide(x):
        return x / n
    return divide


def run(i: int):
    valor = input("Ingresa el valor divisor: ")
    assert valor.isnumeric(), "El valor no es correcto para continuar"    
    
    opts = {
        "por1": divicion_por_n(3),
        "por2": divicion_por_n(4),
    }
    print(opts["por{}".format(i)](int(valor)))


if __name__ == "__main__":
    run(1)
    run(2) 