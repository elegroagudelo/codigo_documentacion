Instalacion Python
=====================

* $~ add-apt-repository -y ppa:jonathonf/python-3.6
* $~ apt-get update -y
* $~ apt-get install -y python3.6
* $~ apt-get install -y python3.6-dev
* $~ apt-get install -y python3.6-distutils
* $~ ln -s /usr/bin/python3.6 /usr/local/bin/python3
* $~ wget https://bootstrap.pypa.io/get-pip.py -O /home/ubuntu/get-pip.py
* $~ python3.6 /home/ubuntu/get-pip.py
* $~ rm /home/ubuntu/get-pip.py
* $~ ln -s /usr/local/bin/pip /usr/local/bin/pip3
* $~ python3 --version
* $~ pip3 --version
* $~ python3 -m venv ENTORNO      # donde `ENTORNO` sea el nombre deseado
* $~ source ENTORNO/bin/activate  # para activar el entorno
* $~ deactivate                   # para desactivar el entorno


Activar entorno virtual
* $~ pip install django -U

Web 
-------
Iniciamos la configuración del servicios para python
* $~ python3 -m venv .env-python3-10-9
* $~ source .env/bin/activate
* (.env)~ pip freeze | grep Django   #muestra que ya esta instalado django
* (.env)~ django-admin 

Nombramos en proyecto django 
------------------------------
* (.env)~ django-admin startproject djprueba .
* (.env)~ cd djprueba  
* (.env)~ python3 ../manage.py

Arranca un servidor web con django en el puerto 8000 de localhost       
* (.env)~ python3 ../manage.py runserver


USANDO pdb DEBUG
------

* (PDB) request.GET
* (PDB) request
* (PDB) request.method
* (PDB) c     #continuar con la ejecucion 


Usando desde Windows
===

Inicializamos el ambiente de python     
~ python -m venv .env
~ .env\Scripts\activate.bat
~ (.env)~  py app.py    
~ (.env)~  py  //ambiente de shell python

* salida del ambiente
~ (.env)~ deactivate    
