from typing import Dict, List, Tuple

""" desde payton 3 se puede establecer que tipo de dato debe alojar una variable """
""" Donde se define el tipo de argnumentos y el tipo de retorno d ela funcion """

def sumar(a: int, b: int) -> int:
    return a + b


""" Se define una lista de valores enteros """    
def crear_lista():
    numeros_pasitivos: List[int]
    numeros_pasitivos = [1,2,3,4,5,6]
    print(numeros_pasitivos) 


""" Donde se declara una arreglo de tipo diccionario con clave y valor """
def crear_diccionarios():
    users: Dict[str, int]
    users = {
        'colombia': 54000000,
        'mexico': 120000000,
        'argentina': 34000000
    }
    print(users)


def collection_paises():
    paises: List[Dict[str, str]]
    paises = [
        {
            "nombre": "colombia",
            "cantidad": "5400000"
        },
        {
            "nombre": "mexico",
            "cantidad": "12000000"
        },
        {
            "nombre": "argentina",
            "cantidad": "7400000"
        }
    ]
    print(paises)


def cordenadas_tubla():
    """ se crea CordenadasTypes que es el tipo de predefinido """
    CordenadasTypes = List[Dict[str, Tuple[int, int]]]
    cordenadas: CordenadasTypes

    cordenadas = [
        {
            "cordenada1": (0, 23),
            "cordenada2": (20, 45)
        },
        {
            "cordenada3": (20, 53),
            "cordenada4": (10, 35)
        }
    ]
    print(cordenadas);


if __name__ == "__main__":
    a: int = 5
    print(a)

    b: float = 5.5
    print(b)

    c: str = "Cadena"
    print(c)

    d: bool = True
    print(d)
    
    print(sumar(4, 5))

    """ los tipo no los identifica si los datos son de tipo strings, asi que los concatena """
    print(sumar('4', '5'))

    """ Ahora para estructura de datos """
    crear_diccionarios()
    crear_lista()
    collection_paises()
    cordenadas_tubla()

    """ mypy """
    """ el modulo de mypy, permite que el sistema inspeccione errores de tipado al ingresar en tiempo de ejecución """
    """ $~ mypy archivo.py  --check-untyped-defs """