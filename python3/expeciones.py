#manejo de excepciones
class Circulo():
    
    def __init__(self, radio):
        self.radio = radio

    @property
    def area(self):
        return 3.1416 * (self.radio ** 2)

try:
    elemento = Circulo(10)
    elemento.area()
    
except Exception:
    print()



lista = [1,2,3,4,5]
try:
    print(lista[8])
except IndexError:
    print("Error por indice")
else:
    print("No hay error")
finally:
    print("Se ejecuto")


#lanzar una excepcion
#raise lanzamos una excepcion
lista = [1,2,3,4,5]
try:
    raise TypeError
    print(lista[8])
except:
    print("Error por tipo")


#como definir una excepcion
class DebugException(Exception):
    
    def __init__(self, valor):
        print("Se presenta un error: ", valor)

try:
    raise DebugException("Error aquí")
except DebugException:
    print("Error escrito")


