

#variables
edad =1 
nombre = "edwin legro"  
altura = 1.77   
estado = true   
code = 7+8j   #numeros complejos

#Operaciones matematicos
print(11%5) #optener el resto de la divicion
print(11//5)  #la doble diagonal hace un redondeo del  valor hacia bajo
print(2**3) #2 a la potencia de 3


#Condicionales
con = 10
if( con > 100):
    print("SI")
else
    print('NO')

if con == 10: #forma simple de condicionar
    if condicion2: #forma simple de condicionar
        print("Ok 1")
    else:
        print("Ok 2")
else:
    print("Ok 3")



#Condiciones iterativas
valor = 100
while (valor < condicion):
    print("Ok")
    print("Datos", valor)
    valor = valor + 1



#TUPLAS
#Es constante su valor, no se puede cambiar
tuples = (19, 1.77, "Edwin")
print(tupla[0])

tupla = (12, 12,23,34,5,67)
tupla2 = tupla[0:5] #toma desde cero los 5 primeros elementos  
tupla2 = tupla[:5] #toma desde cero los 5 primeros elementos  
tupla2 = tupla[2:] #toma desde 2 todos los demas elementos  


#Manejo de Cadenas

#salida de cadenas
print("Hola prueba inicial");
nombre = "Edwin Andres Legro"
print(nombre[0:4]) #Lee una parte de una cadena, partiendo desde la posicion de cero
print(nombre[:4])
print(nombre[6:])

#Listas en python
lista = ['edwin', 'andres', 'alan', 'felipe']
print(len(lista)) #lee el tamaño de la lista

#recorrer una lista
for nombre in lista:
    print(nombre)

#agregar una lista a otra
lista2 = ['juan', 'carlos', 'luis']
lista3 = lista + lista2
print(lista3)

#busqueda usando lista y condicional
if 'edwin' in lista:
    print('SI')
else:
    print('NO')

#mostra lista
print(lista[3:2]) #desde la posicion 3 hasta dos partes
print(lista[:2])
print(lista[3:])

#busqueda de order desc
print(lista[-1]) #busca desde el ultimo hacia delante

#hacer un diccionario de clave y valor
dic= {"edwin": 32, "felipe": 5, "alan": 5}
print(dic["edwin"])
print(len(dic)) #tamaño del dic

#funciones propias del lenguaje
help(tupla) #muestra ayuda del elemento, clase, metodos y atributos que posee


#funcion type 
print(type(dic)) #muestra el tipo de elemento

#funcion str, convierte un dato en una cadena
numero= 122312
numero2 = 10
print(str(numero) + str(numero2)) #permite concatenar

#comando dir, permite identificar el metodos disponibles para un tipo de variable
print(dir(tupla))


#Funciones
def metodo():
    print("Ok es un metodo")

def funcion():
    return "Ok es una funcion"

metodo()
funcion()


#Iteracion en pyton
lista = [1,2,3,4,5,6,7,8,9]
for elemento in lista:
    print(elemento)

#recorrer un archivo de lectura
for linea in open("data.csv"):
    print(linea)

#Generadores
#permiten optener resultados poco a poco

def numeros():
    n=1
    while True:
        yield n  #encargada de detener la ejecucion
        n=n+1

i = numeros()
print(i)
print(i.__next__())
print(i.__next__())