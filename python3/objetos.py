#clases en python
#el pass es requerido en las clases
#poseen atributos
#poseen metodos de clases
#el self permite manejar atributos del mismo objeto

class Objeto:
    pass

class Persona:
    nBrazos=0
    nPiernas=0
    cabello=true

    def saludar(mensaje=''):
        print('OK saludos', mensaje)


class Hombre:
    
    nombre = ""
    sexo = ""

    def caminar():
        pass
    
    def comer(alimento=''):
        if(alimento == '')
            print("a comer: ", alimento)

    def setNombre(self, nombre):
        self.nombre = nombre


#Definir el constructor de la clase
#perimite inicializar un objeto
class Mujer:
    
    edad=0
    nombre= ""

    def __init__(self, edad, nombre):
        self.edad = edad
        self.nombre = nombre 

#La herencia
class Mujer(Persona):

    def index():
        pass

persona = Mujer()
print(persona.saludar("Hola mundo"))


#variables de instancia
class Mujer():

    def __init__(self, nombre, edad, peso, nacionalidad):
        self.nombre = nombre 
        self.edad = edad 
        self.peso = peso 
        self.nacionalidad = nacionalidad

    def saludar(self):
        print("Saludos ", self.nombre)

persona = Mujer("ana", 12, 40, "CO")
print(persona.saludar("Hola mundo"))


#vaiables de clase
class Mujer():
    edad=18 #variable de clase estatica
    peso=60

    def __init__(self, nombre, nacionalidad):
        self.nombre = nombre 
        self.nacionalidad = nacionalidad

    def saludar(self):
        print("Saludos ", self.nombre)

persona = Mujer("ana","CO")
print(Mujer.edad)


#metodos de instancia
class Mujer():
    nombre =""

    def saludar(self): #metodeo de instancia
        print("Saludos ", self.nombre)

persona = Mujer()
persona.saludar()


#metodos de clase
class Mujer():
    nombre =""

    def saludar(self): #metodo de instancia
        print("Saludos ", self.nombre)
    
    #donde el cls y el classmethod es parametro que indica que esta estatica
    @classmethod
    def despedir(cls):
        pass

Mujer.despedir()

#metodo static
class Mujer():
    nombre =""

    def saludar(self): #metodeo de instancia
        print("Saludos ", self.nombre)
    
    #donde el cls y el classmethod es parametro que indica que esta estatica
    @classmethod
    def despedir(cls):
        pass
    
    #con la anotacion de staticmethod
    @staticmethod
    def nadar():
        pass

Mujer.nadar()


#metodos escenciales
class Mujer():
    nombre =""  
    
    #Primero se lee el new al hacer inistancia de la clase
    #No requiere de un self, es un metodo estatico
    #El super() accede al constructor de la clase
    def __new__(cls):
        print("new")
        return super().__new__(cls)
    
    def __init__(self):
        pass

persona = Mujer()

#propiedades de clase
class Circulo():
    
    def __init__(self, radio):
        self.radio = radio

    @property
    def area(self):
        return 3.1416 * (self.radio ** 2)


circulo = Circulo()
circulo.area #permite acceder al metodo como si fuera una propiedad


#Polimorfismo en python
class Perro:

    def avanzar(self):
        print("avanza 4 pasos")


class Paloma:

    def avanzar(self):
        print("volar") 


#metodo de llamada polimorfismo
def mover(animal):
    animal.avanzar()
    return null


animal = Perro() 
mover(animal)

animal = Paloma()
mover(animal)

#Introspeccion

print( dir(animal)) #muestra en detalle los metodos y propiedades de la instancia
if isinstance(animal, Paloma):
    #valida si el animal es instancia de la clase Paloma
    print("OK")

if hasattr(animal, "nombre") : 
    #Indica si el animal posee un atributo nombre
    print("OK")
else
    print("NO")

