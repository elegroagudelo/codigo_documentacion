""" def run():
    square = []
    for i in range(1, 101):
        square.append(i**2)
    print(square) """


""" def run():
    square = []
    for i in range(1, 101):
        if (i % 3 != 0):
            square.append(i**2)
    print(square) """

""" element for element in iterable if condition """
""" para cada elemento en el iterable se guadar ese elemento solo si se cumple la condicion """
""" def run():
    square = [i**2 for i in range(1, 101) if i % 3 != 0 ]
    print(square) """


def run2():
    rango = range(1, 999)
    square = []
    for i in rango:
        if i % 4 != 0:
            continue
        if i % 6 != 0:
            continue
        if i % 9 != 0:
            continue 
        square.append(i)  
    print(square)


def run():
    rango = range(1, 99999)
    square = [ i for i in rango if (i % 4) == 0 and (i % 6) == 0 and (i % 9) == 0]
    print(square)

if __name__ == '__main__':
    run()