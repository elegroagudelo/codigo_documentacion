""" Un generador, permite controlar en tiempo de ejecucion un salto de ejecucion """
import time

class MyFibonacci:

    def __iter__(self):
        self.n1 = 0
        self.n2 = 1
        self.counter = 0
        return self

    def __next__(self):
        if(self.counter == 0):
            self.counter +=1
            return self.n1
        elif (self.counter == 1):
            self.counter +=1
            return self.n2
        else:
            self.aux = self.n1 + self.n2
            self.n1, self.n2 = self.n2, self.aux
            self.counter +=1
            return self.aux  

"""  retorna un generador usando yield """
def fibonacci_generator():
    n1 = 0
    n2 = 1
    counter = 0
    while True:
        if(counter == 0):
            counter +=1
            yield n1
        elif (counter == 1):
            counter +=1
            yield n2
        else:
            aux = n1 + n2
            n1, n2 = n2, aux
            counter +=1
            yield aux  


if __name__ == "__main__":
    fibonacci = fibonacci_generator()
    for item in fibonacci:
        print(item)
        time.sleep(1)

     