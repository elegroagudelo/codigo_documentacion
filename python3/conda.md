Anaconda 3
=====

Shell comandos para Anaconda de Python  
```
    $~ conda create -n venv
    $~ conda env list
    
    $~ conda info --envs
    $~ conda list --explicit
    $~ conda activate venv
    $~ conda deactivate
    
    $~ conda update -n base -c defaults conda
    $~ conda search curl
    $~ conda install curl
    $~ conda list
```

conda create -n venv python=3.9.13

LINUX
====

~ wget https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh
~ bash Anaconda3-2022.10-Linux-x86_64.sh
~ bash 
