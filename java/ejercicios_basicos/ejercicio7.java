//3. -Determinar si un número es múltiplo de 3 y de 5.
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio7
{
 	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese un Numero ");
		int numero= scanner.nextInt();
		if ((numero % 3) == 0 && (numero % 5) == 0 )
		{
			System.out.println(" El numero "+ numero + " es multiplo de 3 y de 5");
		}else if ((numero % 5) == 0 ) {
			System.out.println(" El numero "+ numero + " es multiplo de 5");
		}else if( (numero % 3) == 0 ){
			System.out.println(" El numero "+ numero + " es multiplo de 3");
		}else{
			System.out.println(" El numero "+ numero + " no es multiplo"); 
		}
	}
}