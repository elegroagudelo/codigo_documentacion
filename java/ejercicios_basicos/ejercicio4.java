/*
4. Sistema para convertir de dias a segundos, a minutos,
	y a horas al mismo tiempo, ej: 2 dias tiene: 48 horas,
	o tiene 2880 minutos o tiene 172800 segundos.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio4 {
	
	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese numero de dias");
		int dias= scanner.nextInt();

		int horas = dias* 24;
		int min= horas*60;
		int sec= min*60;
		System.out.println("Conversion Horas:" + horas );
		System.out.println("Conversion Minutos:" + min );
		System.out.println("Conversion Segundos:" + sec );
	}

}