/**
 6 Un restaurante ofrece un descuento del 10% para
 * compras hasta a $100.000 y 20% para compras mayores .
 * Para ambos casos aplica un impuesto del 16%. Determinar el monto de descuento,
 * impuesto y total a pagar.
 */
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio10 {
	
	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese  valor inicial");
		double valor_inicia= scanner.nextDouble();
		double impu= 0.16;
		double descuento= 0.0;

		if (valor_inicia <= 100000) {
			descuento= valor_inicia * 0.10;
		}else if(valor_inicia > 100000 ){
			descuento= valor_inicia * 0.20;
		}
		double valor = valor_inicia - descuento;
		System.out.println("Valor descuento: "+ valor + " Impuesto "+ (valor * impu)+ " Valor a pagar " + (valor+ (valor*impu)) );
	}
}