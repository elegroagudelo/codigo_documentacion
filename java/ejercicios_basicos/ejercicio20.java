/*
WHILE
 Mostrar los N primeros dígitos de la serie de Fibonnacci. El usuario digita N.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio20 {

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese un  numero: ");
		int num = scanner.nextInt();

		long numero1=1;
		long numero2=1;
		int con=1;
		System.out.println(numero1);
		while ( con <= num)
		{
			con=con +1;
			System.out.println(numero2);
			numero2=numero2+numero1;
			numero1=numero2-numero1;
		}
	}
}