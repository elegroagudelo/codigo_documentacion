/*
7 Debido a los excelentes resultados un restaurante decide ampliar sus ofertas
	de acuerdo a la siguiente escala de consumo, ver tabla.
	Determinar el monto del descuento, impuesto y total a pagar.
	El impuesto en todos los casos es del 16%.
*/

package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio11
{

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		//
		System.out.print("Ingrese valor");
		// leemos valor
		int valor= scanner.nextInt();

		double res1=valor*0.10;
		double res2=valor*0.20;
		double res=valor*0.30;
		double res3=(valor-res1)*0.16;
		double res4=(valor-res2)*0.16;
		double re=(valor-res2)*0.16;
		double res5=(valor-res1)+res3;
		double  res6=(valor-res2)+res4;
		double  res7=(valor-res)+re;

		// mostramos los datos por consola
		if(valor<=100000 )
		{
		System.out.println("El impuesto es "+res3+" El descuento es "+res1+" Total a pagar "+res5);
		}


		else if (valor<=200000)
		{
		System.out.println("El impuesto es "+res4+" El descuento es "+res2+" Total a pagar "+res6);

		}
		else {
			System.out.println("El impuesto es "+res+" El descuento es "+re+" Total a pagar "+res7);

		}
	}

}