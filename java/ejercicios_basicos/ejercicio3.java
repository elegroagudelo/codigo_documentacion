/*
3. Sistema para convertir de metros a kilómetros.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio3 {
	
	public static void main(String[] args)
	{ 
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println(" Ingrese  metros");
		int metros= scanner.nextInt();

		int km = metros/ 1000;
		System.out.println("Medida en kilometros: "+ km + " Kilometros"); 
	}
}