/*WHILE
- Imprimir los 100 primeros dígitos de la serie Fibonnacci  (1,1,2,3,5,8,13------------->). 
*/
package ejercicios_basicos;
import java.util.Scanner;
public class ejercicio19 {

	@SuppressWarnings({ "unused", "resource" })
  	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int num =100;
		long numero1=1;
		long numero2=1;
		int con=1;
		System.out.println(numero1);
		while ( con <= num )
		{
			con=con +1;
			System.out.println(numero2);
			numero2=numero2+numero1;
			numero1=numero2-numero1;
		}
	}
}