/*FOR
Dado un número determinar si es primo o no.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio22
{
	
	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
    	Scanner scanner = new Scanner(System.in);
  		System.out.print("Ingrese un  numero entero: ");
		int numero = scanner.nextInt();
		int con=0;
  		for(int i=1; i <= numero; i++ ){
    		if( (numero % i)==0){
    			con+=1;
    		}
   		}
		if(con== 2){
    		System.out.println("numero es primo "+ numero);
    	}else{
    		System.out.println("numero no  es primo "+ numero);
   		}
    }
}
