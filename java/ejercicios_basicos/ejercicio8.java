//4. - Dados tres números enteros devolver cual es el mayor
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio8 {

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese  Numero 1");
		int num1= scanner.nextInt();
		System.out.println("Ingrese  Numero 2");
		int num2= scanner.nextInt();
		System.out.println("Ingrese  Numero 3");
		int num3= scanner.nextInt();

		if (num1 > num2 && num2 > num3) {
			System.out.println(" El numero "+ num1  + " es mayor "+ num2 +" y este es mayor al "+ num3);
		}else if (num2 > num1 && num1 > num3) {
			System.out.println(" El numero "+ num2  + " es mayor "+ num1 +" y este es mayor al "+ num3);
		}else if (num3 > num1 && num1 > num2) {
			System.out.println(" El numero "+ num3  + " es mayor "+ num1 +" y este es mayor al "+ num2);
		}else if (num3 > num2 && num2 > num1) {
			System.out.println(" El numero "+ num3  + " es mayor "+ num2 +" y este es mayor al "+ num1);
		}else if(num1 > num3 && num3 > num2){
			System.out.println(" El numero "+ num1  + " es mayor "+ num3 +" y este es mayor al "+ num2);
		}else{
			System.out.println(" El numero "+ num2  + " es mayor "+ num3 +" y este es mayor al "+ num1);
		}
	}
}