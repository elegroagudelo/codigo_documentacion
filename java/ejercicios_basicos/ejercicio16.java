/*
While
1 Dado 2 números enteros, obtener la cantidad de numeros enteros
que existen entre ellos. Por ejemplo númeroA= 2, númeroB=10,
Respuesta: existen 7 números (3,4,5,6,7,8,9)
*/
package ejercicios_basicos;
import java.util.Scanner;
public class ejercicio16 
{

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese primer entero: ");
		int entero1 = scanner.nextInt();
		System.out.print("Ingrese segundo entero: ");
		int entero2 = scanner.nextInt();
		int con;

		if(entero1 > entero2)
		{
			con = entero2;
			while(con < entero1)
			{
				con = con + 1;
				System.out.println(con);
			}
		}else{
			con = entero1;
			while(con < entero2)
			{
				con = con+1;
				System.out.println(con);
			}
		}
	}
}