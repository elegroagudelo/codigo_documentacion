/*
1 Leer 10 enteros, almacenarlos en un vector y 
determinar en qué posición del vector está el mayor número leído.
 */
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio26 {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		int datos[] = new int[10];
		int mayor= 0;

		for(int i=0; i < 10; i++){
			System.out.println("Ingresar numero entero: ");
			int numero = scanner.nextInt();
			datos[i]=numero;
		}

		for(int j=0; j < 10; j++)
		{
			mayor =datos[j];
			if(datos[j] > mayor){ 
				mayor= datos[j];
			}
		}
		System.out.println("El número mayor es : " + mayor);
		
	}
	 
}
