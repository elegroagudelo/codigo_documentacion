/*
2. El mismo ejercicio anterior para 2 personas al mismo tiempo.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio2 {
	
	public static void main(String[] args)
	{ 
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
	    System.out.println("Ingrese nombre 1");
	    String nom_uno = scanner.next();
	    System.out.println("año de nacimiento 1");
	    int anno_uno = scanner.nextInt();
	    int edad_uno= 2021 - anno_uno; 
	     
	    System.out.println("Ingrese nombre 2 ");
	    String nom_dos = scanner.next();
	    System.out.println("año de nacimiento  2 ");
	    int anno_dos = scanner.nextInt();
	    int edad_dos= 2021 - anno_dos;
    
	    System.out.println("Nombre 1: "+ nom_uno +" su edad es "+ edad_uno + " años " + " Nombre 2:"+ nom_dos + " edad "+ edad_dos + " años "); 
	}

}