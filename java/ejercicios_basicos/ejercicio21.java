/*FOR
Dado 2 números enteros, obtener la cantidad de numeros enteros que existen entre ellos.
Por ejemplo númeroA= 2, númeroB=10, Respuesta: existen 7 números (3,4,5,6,7,8,9)
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio21 
{

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
 		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese un  numero entero 1: ");
		int num1 = scanner.nextInt();
		System.out.print("Ingrese un  numero entero 2: ");
		int num2 = scanner.nextInt();
		int i= 1;
		int con=0;
		if(num1 > num2){
		num2+=1;
		for(i=num2; i < num1; i++ ){
			System.out.println(i);
			con+=1;
		}
		}else{
			num1+=1;
			for(i=num1; i < num2; i++ ){
				System.out.println(i);
				con+=1;
			}
		}
		System.out.println(" Total numeros:  "+ con);
	}
}