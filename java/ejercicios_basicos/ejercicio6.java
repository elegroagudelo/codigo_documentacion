/*
2. Dado un caracter determinar si es una vocal
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio6
{
 	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese un caracter");
		String car= scanner.next();
		if(car.equals("a")|| car.equals("e")|| car.equals("i")||car.equals("o")||car.equals("u") ){
	 		System.out.println(car +" Es una vocal ");
		}else{
	 		System.out.println( car+" No es una vocal"); 
		}
 	}

}