/*
WHILE
Dado un número determinar si es primo o no.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio17 
{

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese un numero: ");
		int numero=scanner.nextInt();

		int i= 1;
		int con= 0;
		while(i <= numero ) {
			if( (numero % i)==0){
			con+=1;
			}
			i+=1;
		}
		if(con== 2){
			System.out.println("numero es primo "+ numero);
		}else{
			System.out.println("numero no  es primo "+ numero);
		}
	}
}