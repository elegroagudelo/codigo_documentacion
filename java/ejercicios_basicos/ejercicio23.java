/*FOR
  Obtener el factorial de un numero.
 */
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio23 {
 
    @SuppressWarnings("resource")
	public static void main(String[] args)
	{
    	Scanner scanner = new Scanner(System.in);
    	System.out.print("Ingrese un  numero entero: ");
		double numero = scanner.nextInt();
		double factorial = 1;
		int num= 2;

		for(int i=num; i <= numero;  i++) {
			factorial=factorial*i;
		}
		System.out.println(factorial);
    }
}
