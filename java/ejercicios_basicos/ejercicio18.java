/*WHILE
Obtener el factorial de un numero.
*/
package ejercicios_basicos;
import java.util.Scanner;

public class ejercicio18 
{
	@SuppressWarnings("resource")
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ingrese un numero factorial: ");
		double numero = scanner.nextInt();
		double factorial = 1;
		int num= 2;
		while (num <= numero) {
			factorial=factorial*num;
			num++;
		}
		System.out.println(factorial);
	}
}