package poo;

public class Sobrecarga {
    
    public static void main(String[] args) 
    { 
       String s = "Esto es una cadena de texto";
       int pos1 = s.indexOf("e");
       int pos2 = s.indexOf('e');

       System.out.println("Pos 1 "+ pos1);
       System.out.println("Pos 2 "+ pos2);
       //invocamos el indexOf pasando primero un argumento de tipo String
       //luego pasamos un argumento tipo char
       //ambas invocaciones son correctas, y funciona porque el metodo indexOf de la clase String esta sobrecargado
       
       Fecha f = new Fecha("2022/10/31");
       System.out.println("Fecha es: "+ f.toString());
    }
}

class Fecha {
    
    private int dia;
    private int mes;
    private int anio;

    public Fecha (String _sfecha){
        int pos1 = _sfecha.indexOf('/');
        int pos2 = _sfecha.lastIndexOf('/');
        String sDia = _sfecha.substring(0, pos1);
        this.dia = Integer.parseInt(sDia);
        String sMes = _sfecha.substring(pos1+1, pos2);
        this.mes = Integer.parseInt(sMes);
        String sAnio = _sfecha.substring(pos2+1);
        this.anio = Integer.parseInt(sAnio);
    }

    public int getDia(){
        return this.dia;
    }
    public int getMes(){
        return this.mes;
    }
    public int getAnio(){
        return this.anio;
    }
    public void setDia(int dia){
        this.dia = dia;
    }
    public void setMes(int mes){
        this.mes = mes;
    }
    public void setAnio(int anio){
        this.anio = anio;
    }
    @Override
    public String toString() {
        return this.anio+"/"+this.mes+"/"+this.dia;
    }
}
