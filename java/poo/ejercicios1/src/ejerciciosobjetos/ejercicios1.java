package ejerciciosobjetos;

import java.util.Scanner;

public class ejercicios1 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		Objetos objeto = new Objetos();

		objeto.setNombre("Edwin Andres");
		objeto.setIdentidad(1000);
		objeto.setSexo("M");

		System.out.println("Nombres: " + objeto.getNombre());
		System.out.println("Identidad: " + objeto.getIdentidad());
		System.out.println("Sexo: " + objeto.getSexo());
	}
}
