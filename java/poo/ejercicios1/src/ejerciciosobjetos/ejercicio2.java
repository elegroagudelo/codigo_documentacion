package ejerciciosobjetos;

import java.util.Scanner;

public class ejercicio2 {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Objetos objeto = new Objetos();
		// Universidad
		objeto.setNombre("Universidad San Jose");
		objeto.setCiudad("Ibague");
		objeto.setDireccion("Carrera 5 calle 19");

		System.out.println("Universidad: " + objeto.getNombre());
		System.out.println("Ciudad: " + objeto.getCiudad());
		System.out.println("Direccion: " + objeto.getDireccion());
	}
}
