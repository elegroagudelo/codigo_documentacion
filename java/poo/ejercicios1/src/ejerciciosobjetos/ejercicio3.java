package ejerciciosobjetos;

import java.util.Scanner;

public class ejercicio3 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		Triangulo triangulo = new Triangulo();

		triangulo.setBase(10);
		triangulo.setAltura(11);

		System.out.println("Area es: " + triangulo.Area());
		System.out.println("Perimetro es; " + triangulo.Perimetro());
	}
}
