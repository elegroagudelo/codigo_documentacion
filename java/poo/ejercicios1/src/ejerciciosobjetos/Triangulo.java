package ejerciciosobjetos;

class Triangulo {

	private int base;
	private int altura;
	private int Area;
	private int Perimetro;

	public void setBase(int base) {
		this.base = base;
	}

	public int getBase() {
		return Area;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getAltura() {
		return Perimetro;
	}

	public int Area() {
		Area = (this.base * this.altura) / 2;
		return Area;
	}

	public int Perimetro() {
		Perimetro = this.base * this.altura;
		return Perimetro;
	}
}
