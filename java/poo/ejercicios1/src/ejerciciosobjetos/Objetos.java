package ejerciciosobjetos;

public class Objetos {

	private String nombre;
	private int identidad;
	private String sexo;
	private String Direccion;
	private String Ciudad;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdentidad() {
		return identidad;
	}

	public void setIdentidad(int identidad) {
		this.identidad = identidad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String Direccion) {
		this.Direccion = Direccion;
	}

	public String getCiudad() {
		return Ciudad;
	}

	public void setCiudad(String Ciudad) {
		this.Ciudad = Ciudad;
	}

}
