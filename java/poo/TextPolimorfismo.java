//Los objetos hijos nunca se dejan de recionocer como miembros de su clase padre
//Todo objecto de java pertenece a su clase padre Object
//la refrencia super(), debido a que los constructores no se heredan, se requiere de poder acceder al constructor de la clase padre y paar ello usamos super()
//En el super(dias), pasamos los parametros que requiere la clase padre 
package poo;

public class TextPolimorfismo {
    
    public static void main(String[] args) {
        
        Fecha fec = new FechaDetallada("10/12/2022");
        Object obj = new FechaDetallada("15/10/2022");

        System.out.println("Fecha "+fec);
        System.out.println("Object "+obj);
    }
}


class Fecha {
    
    private int dia;
    private int mes;
    private int anio;

    public Fecha (String _sfecha){
        int pos1 = _sfecha.indexOf('/');
        int pos2 = _sfecha.lastIndexOf('/');
        String sDia = _sfecha.substring(0, pos1);
        this.dia = Integer.parseInt(sDia);
        String sMes = _sfecha.substring(pos1+1, pos2);
        this.mes = Integer.parseInt(sMes);
        String sAnio = _sfecha.substring(pos2+1);
        this.anio = Integer.parseInt(sAnio);
    }

    public Fecha(){}

    public void addDias(int d){
        int sum = fechaToDias() + d;
        diasToFecha(sum);
    }

    //retorna la fecha en días
    public int fechaToDias(){
        return (this.anio * 360) + (this.mes * 30) + this.dia;
    }

    private void diasToFecha(int i){
        this.anio = (int) i/360;
        int resto = i % 360;

        //el mes es el resto dividido en 30
        this.mes = (int) resto / 360;

        this.dia = resto % 30;

        if(dia == 0){
            this.dia=30;
            this.mes--;
        }
        if(mes == 0){
            this.mes= 12;
            this.anio--;
        }
    }

    public int getDia(){
        return this.dia;
    }
    public int getMes(){
        return this.mes;
    }
    public int getAnio(){
        return this.anio;
    }
    public void setDia(int dia){
        this.dia = dia;
    }
    public void setMes(int mes){
        this.mes = mes;
    }
    public void setAnio(int anio){
        this.anio = anio;
    }
    @Override
    public String toString() {
        return this.anio+"/"+this.mes+"/"+this.dia;
    }
}


class FechaDetallada extends Fecha
{
    private static String meses[] = {
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    };

    public FechaDetallada(String _fecha){
        super(_fecha);
    }

    public String toString(){
        return  this.getDia() + " de "+meses[this.getMes()-1]+" de "+getAnio();
    }
}