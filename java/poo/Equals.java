/*El metodo equals,
 * de los metodos definidos en la clase Object, se usa para comparar objetos.
 * Tambien permite comparar string, ya que la clase string lo hereda de la clase Object
 */
package poo;
import java.util.Scanner;

public class Equals {

    @SuppressWarnings("resource")
    public static void main(String[] args) 
    {    
        Scanner scanner = new Scanner(System.in);
        System.out.println("Iniciamos la prueba de equals");
        System.out.println("Ingresa la fecha Dia, Mes, Año");
        int dia = scanner.nextInt();
        int mes = scanner.nextInt();
        int anio = scanner.nextInt(); 
        Fecha f;
        f = new Fecha(dia, mes, anio);
        System.out.println(f.toString());

        System.out.println("Ingresa la Fecha 2 Dia, Mes, Año");
        dia = scanner.nextInt();
        mes = scanner.nextInt();
        anio = scanner.nextInt(); 
        Fecha f2;
        f2 = new Fecha(dia, mes, anio);
        System.out.println(f2.toString());
        System.out.println(f.equals(f2));
    }
}

class Fecha
{
    private int dia;
    private int mes;
    private int anio;

    public Fecha(int d, int m, int a){
        this.dia = d;
        this.mes = m;
        this.anio = a;
    }
    
    public boolean equals(Object o)
    {
        Fecha otra = (Fecha) o;
        return (dia == otra.dia) && (mes == otra.mes) && (anio == otra.anio);
    }

    public int getDia(){
        return this.dia;
    }
    public int getMes(){
        return this.mes;
    }
    public int getAnio(){
        return this.anio;
    }
    public void setDia(int dia){
        this.dia = dia;
    }
    public void setMes(int mes){
        this.mes = mes;
    }
    public void setAnio(int anio){
        this.anio = anio;
    }
    @Override
    public String toString() {
        return this.anio+"/"+this.mes+"/"+this.dia;
    }
}
