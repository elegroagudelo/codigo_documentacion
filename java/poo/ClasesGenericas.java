/**
 * las clases genericas permiten parametrizar los tipos de datos de los parametros y valores de retorno de los metodos.
 * La clase ClasesGenericas podemos hacerlas "genericas en T"
 */
package poo;

import java.util.Scanner;

public class ClasesGenericas<T> {
    
    private Object datos[] = null;
    
    private int len = 0;

    public ClasesGenericas(String nombre, int capacidadInicial){
        this.datos = new Object[capacidadInicial];
    }

    public static void main(String[] args) 
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingresa el nombre");
        String nom = scanner.next();

        ClasesGenericas<String> mc = new ClasesGenericas<String>("Collection", 5);
        mc.insert(nom, 0);

        System.out.println("Ingresa la Edad");
        int edad = scanner.nextInt();
        ClasesGenericas<Integer> mi = new ClasesGenericas<Integer>("Collection", 5);
        mi.insert(edad, 0);

        System.out.println("El nombre es "+ mc.obtener(0));
        System.out.println("La edad es "+ mi.obtener(0));
    }

    public void agregar(T elem) {
        this.insert(elem, len);
    }

    public void insert(T elm, int i) {
        if( len == datos.length )
        {
            Object aux[] = datos;
            datos = new Object[datos.length*2];
            for(int j=0; j<len; j++)
            {
                datos[j]=aux[j];
            }
            aux=null;
        }
        for( int j=len-1; j>=i; j-- )
        {
            datos[j+1]=datos[j];
        }
        datos[i]=elm;
        len++;
    }

    @SuppressWarnings ("unchecked")
    public T obtener(int i) {
        return (T) this.datos[i];
    }

    @SuppressWarnings ("unchecked")
    public T eliminar(int i) {
        
        Object aux = datos[i];
        for( int j=i; j<len-1; j++ )
        {
            datos[j] = datos[j+1];
        }
        len--;
        return (T) aux;
    }

    public int buscar(T elm) {
        int i=0;
        // mientras no me pase del tope y mientras no encuentre...
        for( ;i<len && !datos[i].equals(elm); i++ );
        // si no me pase entonces encontre, si no... no encontre
        return i<len ? i : -1;
    }

    public int cantidad() {
        return this.len;
    }
}