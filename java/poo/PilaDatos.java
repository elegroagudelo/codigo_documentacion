/**
 * Es una coleccion de objetos que restringe la manera de agregar o eliminar elementos.
 * En una pila, solo se puede apilar y desapilar
 * El ultimo elemento en apilarse sera el primero en desapilarse
 * Se le llama tambien estructura LIFO (Last In First Out)
 *  
 */
package poo;
import poo.ClasesGenericas;

public class PilaDatos {
    
    public static void main(String[] args) {
        
        MiPila<Integer> c = new MiPila<Integer>();
        c.apilar(1);
        c.apilar(4);
        c.apilar(8);
        c.apilar(12);
        System.out.println(c.desapilar());
        System.out.println(c.desapilar());
    }
}

class MiPila<T>
{

    private static final int CAPACIDAD_INICIAL=5;

    private ClasesGenericas<T> coll = new ClasesGenericas<T>("General", CAPACIDAD_INICIAL);

    public void apilar(T elm){
        coll.insert(elm, 0);
    }

    public T desapilar(){
        return coll.eliminar(0);
    }

}

class ClasesGenericas<T> {
    
    private Object datos[] = null;
    
    private int len = 0;

    public ClasesGenericas(String nombre, int capacidadInicial){
        this.datos = new Object[capacidadInicial];
    }

    public void agregar(T elem) {
        this.insert(elem, len);
    }

    public void insert(T elm, int i) {
        if( len == datos.length )
        {
            Object aux[] = datos;
            datos = new Object[datos.length*2];
            for(int j=0; j<len; j++)
            {
                datos[j]=aux[j];
            }
            aux=null;
        }
        for( int j=len-1; j>=i; j-- )
        {
            datos[j+1]=datos[j];
        }
        datos[i]=elm;
        len++;
    }

    @SuppressWarnings ("unchecked")
    public T obtener(int i) {
        return (T) this.datos[i];
    }

    @SuppressWarnings ("unchecked")
    public T eliminar(int i) {
        
        Object aux = datos[i];
        for( int j=i; j<len-1; j++ )
        {
            datos[j] = datos[j+1];
        }
        len--;
        return (T) aux;
    }

    public int buscar(T elm) {
        int i=0;
        // mientras no me pase del tope y mientras no encuentre...
        for( ;i<len && !datos[i].equals(elm); i++ );
        // si no me pase entonces encontre, si no... no encontre
        return i<len ? i : -1;
    }

    public int cantidad() {
        return this.len;
    }
}