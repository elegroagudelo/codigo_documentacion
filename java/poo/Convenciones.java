/*
 * Convenciones a respetar 
 * Nombre de clases: 
 * Siempre comenzar con mayusculas, si posee más de una palabra, entonces, cada inicial tambien debe estar en mayuscula
 * Ejemplo:
 * public class NombreDelaClasse {
 * }
 * 
 * Nombre de metodos:
 * Siempre deben comenzar en minusculas, en casode tener un nombre compuesto, cada inicial debe comenzar en mayusculas salvo la primera
 * public void nombreDelMetodo(){
 * }
 * 
 * Nombre de atributos:
 * Se usa la misma convención que se usa para los metodos, 
 * private String nombre;
 * private Date fechaDeNacimiento;
 *
 * Nombre de constantes:
 * Estas deben disponer de solo letras mayusculas, si es compuesta debe tener un guion (_)
 * Ejemplo: 
 * public static final int NOMBRE_DE_LA_CONSTANTE = 1;
 * 
 * El metodo finalize es el metodo de final de toda clase, que permite hacer algo antes de que se destruya la clase
 * 
 * Variables de instancia o de objecto
 * 
 * Variables de clase o estaticas
 * 
 * Metodos de instancia o de objecto
 * 
 * Metodos de clase o estaticas
 * 
 * Las constantes
 * 
 * Las clases utilitarias
 * Son aquellas que ya existen dentro del core del sistema, permiten acceder a constantes y metodos declarativos que hacen funciones y tareas especificas
 */

package poo;

public class Convenciones {
    
    private static int cont = 0;

    public Convenciones(){
        cont++;
        System.out.println(cont);
    }

    public void finalize(){
        cont--;
    }

    public static void main(String[] args) {
        while(true){
            new Convenciones();
        }
    }
}

class Mate {

    public static final double PI = 3.1415;
    public static final double E = 2.7182;

    public Mate (){
        //la constante se puede acceder desde cualquier parte del sistema, haciendo llamando por medio de la clase  
        System.out.print(Mate.PI);
        double c = Mate.sumar(PI, E);
        System.out.println(c);
    }

    //Metodo de clase
    public static double sumar(double a, double b){
        return a+b;
    }
}