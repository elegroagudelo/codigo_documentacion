package poo;

public class InterfacesComunicacion {
    public static void main(String[] args) {
        
        IComunicador t1 = new TelefonoCelular();
        IComunicador t2 = new PalomaMensajera();
        IComunicador t3 = new Telegrafo();
        
    }
}

class Ave {
}

class Telefono {
}

class Paloma extends Ave {
}

class Reliquia{
}

class TelefonoCelular extends Telefono implements IComunicador {
    
    public void enviarMensaje(String mensaje){
    }
}

class PalomaMensajera extends Paloma implements IComunicador {

    public void enviarMensaje(String mensaje){
    }
}

class Telegrafo extends Reliquia implements IComunicador {

    public void enviarMensaje(String mensaje){
    }
} 


interface IComunicador {
    public void enviarMensaje(String mensaje);
}