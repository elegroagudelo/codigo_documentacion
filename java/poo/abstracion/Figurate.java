/*
 * Una clase abstracta con un unico metodo abstracto
 * Las clases abstractas no pueden ser instanciadas
 * Math pow potencia al cuadrado
 * Las clases abstractas pueden disponer de un constructor
 * Tambien podemos disponer de metodos estaticos dentro de la clase abstracta
*/
package poo.abstracion;

public class Figurate {
    
    public static void main(String[] args) {
        Rectangulo r = new Rectangulo(12, 45);
        System.out.println(r);

        Circulo c = new Circulo(120);
        System.out.println(c);

        //haciendo uso del polimorfismo y la abstracción
        FiguraGeometrica arr[] = {
            new Circulo(120),
            new Rectangulo(20, 30)
        };

        double promo = FiguraGeometrica.areaPromedio(arr);
        System.out.println("El promedio es: "+promo);
    }
}

abstract class FiguraGeometrica {
    
    private String nombre;

    public FiguraGeometrica(String nombre){
        this.nombre = nombre;
    }

    public abstract double area();

    public static double areaPromedio(FiguraGeometrica arr[]){
        int sum=0;
        for (int i = 0; i < arr.length; i++) {
            sum+= arr[i].area();
        }
        return sum/arr.length;
    }

    public String toString(){
        return  "El area de "+nombre+ " es: " + area();
    }
}

class Rectangulo extends FiguraGeometrica {
    
    private int base;
    private int altura;

    public Rectangulo(int base, int altura){
        super("Rectangulo");
        this.base = base;
        this.altura = altura;
    }

    public double area(){
        return this.base * this.altura;
    }
}

class Circulo extends FiguraGeometrica {
    
    private int radio;

    public Circulo(int radio){
        super("Circulo");
        this.radio = radio;
    }

    public double area(){
        return Math.PI * Math.pow(this.radio, 2);
    }
}