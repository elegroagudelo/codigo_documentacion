/*
* dado que todos es un objecto podemos identifiar que algun tipo es en definitiva un Object
* Pero esta opcion no permite el poder identificar el tipo de objeto en concreto 
*/
package poo.abstracion;

import java.util.Scanner;

public class MiColection  extends Coleccion {
    
    private Object datos[] = null;
    private int len = 0;

    public MiColection(String nombre, int capacidadInicial){
        super(nombre);
        this.datos = new Object[capacidadInicial];

    }

    public static void main(String[] args) 
    {
        Scanner scanner = new Scanner(System.in);
        MiColection mc = new MiColection("Collection", 5);
        System.out.println("Ingrese Nombre: ");
        String nom=scanner.next();
        while( !nom.equals("FIN") )
        {
            // inserto siempre en la posicion 0
            mc.insert(nom, 0);
            // leo el siguiente nombre
            nom=scanner.next();
        }
        String aux;
        for(int i=0; i<mc.cantidad(); i++ )
        {
            // el metodo obtener retorna un Object entonces
            // entonces tengo que castear a String
            aux = (String) mc.obtener(i);
            System.out.println(aux +" - "+aux.length()+" caracteres");
        }
    }

    @Override
    public void agregar(Object elem) {
        this.insert(elem, len);
    }

    @Override
    public void insert(Object elm, int i) {
        if( len == datos.length )
        {
            Object aux[] = datos;
            datos = new Object[datos.length*2];
            for(int j=0; j<len; j++)
            {
                datos[j]=aux[j];
            }
            aux=null;
        }
        for( int j=len-1; j>=i; j-- )
        {
            datos[j+1]=datos[j];
        }
        datos[i]=elm;
        len++;
    }

    @Override
    public Object obtener(int i) {
        return this.datos[i];
    }

    @Override
    public Object eliminar(int i) {
        
        Object aux = datos[i];
        for( int j=i; j<len-1; j++ )
        {
            datos[j] = datos[j+1];
        }
        len--;
        return aux;
    }

    @Override
    public int buscar(Object elm) {
        int i=0;
        // mientras no me pase del tope y mientras no encuentre...
        for( ;i<len && !datos[i].equals(elm); i++ );
        // si no me pase entonces encontre, si no... no encontre
        return i<len ? i : -1;
    }

    @Override
    public int cantidad() {
        return this.len;
    }
}

abstract class Coleccion{

    private String nombre;

    public Coleccion(String nombre){
        this.nombre = nombre;
    }

    public abstract void agregar(Object elem);

    public abstract void insert(Object elm, int i);

    public abstract Object obtener(int i);

    public abstract Object eliminar(int i);

    public abstract int buscar(Object elem);

    public abstract int cantidad();

    public String toString(){
        return "";
    }
}