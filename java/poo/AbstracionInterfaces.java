package poo;

public class AbstracionInterfaces {
    
    @SuppressWarnings ("unchecked")
    public static void ordenar (IComparable arr[]){
        boolean ordenado = false;
        while(!ordenado){
            ordenado = true;
            for (int i = 0; i < arr.length; i++) {
                if(arr[i].comparaTo(arr[i]) <0 ){
                    IComparable aux = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = aux;
                    ordenado = false;
                }
            }
        }
    }

    public static void main(String[] args) {
        Alumno arr[] = {
            new Alumno("Alan", 12, 5),
            new Alumno("Felipe", 10, 4.2),
            new Alumno("Jhon", 13, 3.5)
        };
        AbstracionInterfaces.ordenar(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}

class Alumno implements IComparable<Alumno>{

    private String nombre;
    private int edad;
    private double notaPromedio;

    public Alumno(String nom, int edad, double nota){
        this.nombre = nom;
        this.edad = edad;
        this.notaPromedio = nota;
    }

    public int comparaTo(Alumno otroAlumno){
        return this.edad - otroAlumno.edad;
    }

    public String toString(){
        return this.nombre+ " - "+ this.edad + " - "+ this.notaPromedio;
    }

}


interface IComparable<T> {

    public int comparaTo(T obj);
}