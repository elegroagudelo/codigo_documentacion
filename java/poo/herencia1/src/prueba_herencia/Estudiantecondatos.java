
package prueba_herencia;

import java.util.Scanner;

/**
 *
 * @author elegro
 */
public class Estudiantecondatos {

	protected double[] notas;

	public Estudiantecondatos(double notas) {
	}

	public Estudiantecondatos() {
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String nombre = "edwin andres";
		String apellidos = "Legro Agudelo";
		int codigo = 1234234;

		Jherencias jmetodo = new Jherencias(nombre, apellidos, codigo);

		System.out.println("Ingresar direccion: ");
		String direccion = scanner.next();

		System.out.println("Ingresar telefono: ");
		int telefono = scanner.nextInt();

		System.out.print("Ingresar email: ");
		String email = scanner.next();
		System.out.println("Resultado :" + jmetodo.cargarDatos(direccion, telefono, email));

	}

	public double promedioNotas() {
		return (notas[0] + notas[1] + notas[2] + notas[3] + notas[4]) / 5;
	}

	public void setNotas(double notas[]) {
		this.notas = notas;
	}

	public double mayorNotas() {
		double mayor = notas[0];
		for (int x = 1; x < notas.length; x++) {
			if (notas[x] > mayor) {
				mayor = notas[x];
			}
		}
		return mayor;
	}

	public double menorNotas() {
		int menor = 0;
		for (int x = 1; x < notas.length; x++) {
			if (notas[x] < notas[menor]) {
				menor = x;
			}
		}
		return notas[menor];
	}

	public double promedioXNotas() {
		double suma = 0;
		for (int x = 0; x < notas.length; x++) {
			suma += notas[x];
		}
		double promedio = (suma / notas.length);
		return promedio;
	}

}
