
package prueba_herencia;

import java.util.Scanner;

/**
 *
 * @author elegro
 */
public class EstudiantesNotas extends Estudiantecondatos {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double arreglo[] = new double[5];

		for (int i = 0; i < 5; i++) {
			System.out.println("Ingresar notas: " + i);
			double numero = scanner.nextDouble();
			arreglo[i] = numero;
		}

		Estudiantecondatos jmetodo = new Estudiantecondatos();
		jmetodo.setNotas(arreglo);
		System.out.println("Promedio: " + jmetodo.promedioNotas());
		System.out.println("Nota Mayor: " + jmetodo.mayorNotas());
		System.out.println("Nota Menor: " + jmetodo.menorNotas());
	}

}