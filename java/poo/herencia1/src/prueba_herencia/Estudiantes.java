
package prueba_herencia;

/**
 *
 * @author elegro
 */
public class Estudiantes {

	protected String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	protected String apellidos;

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	protected int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String toDatos() {
		String result = "Nombre: " + this.nombre + " " + this.apellidos + "\n Codigo: " + this.codigo;
		return result;
	}

	protected String direccion;

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	protected String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	protected int telefono;

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String allDatos() {
		String result = "Nombre: " + this.nombre + " " + this.apellidos + "\n Codigo: " + this.codigo +
				"\n Direccion: " + this.direccion + "\n Telefono: " + this.telefono + "\n Email: " + this.email;
		return result;
	}
}
