
package prueba_herencia;

public class Jherencias extends Estudiantes {
	
	public Jherencias(String nom, String ape, int code) {
		nombre = nom;
		apellidos = ape;
		codigo = code;
	}

	public Jherencias() {
	}

	public String toString() {
		setNombre(nombre);
		setApellidos(apellidos);
		setCodigo(codigo);
		return toDatos();
	}

	public String cargarDatos(String direccion, int telefono, String email) {
		setNombre(nombre);
		setApellidos(apellidos);
		setCodigo(codigo);
		setDireccion(direccion);
		setTelefono(telefono);
		setEmail(email);
		return allDatos();
	}
}
