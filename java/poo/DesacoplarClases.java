package poo;

public class DesacoplarClases {
    
    public static void main(String[] args) {
        
        IComunicador c = ComunicadorManager.crearComunicador();
        c.enviarMensaje("Ok el mensaje se está pasndo");
    }
}

class Ave {
}

class Telefono {
}

class Paloma extends Ave {
}

class Reliquia{
}

class TelefonoCelular extends Telefono implements IComunicador {
    
    public void enviarMensaje(String mensaje){
    }
}

class PalomaMensajera extends Paloma implements IComunicador {

    public void enviarMensaje(String mensaje){
    }
}

class Telegrafo extends Reliquia implements IComunicador {

    public void enviarMensaje(String mensaje){
    }
} 

interface IComunicador {
    public void enviarMensaje(String mensaje);
}


class ComunicadorManager {

    public static IComunicador crearComunicador(){
        return new PalomaMensajera();
    }
}