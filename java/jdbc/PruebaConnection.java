package jdbc;

import java.sql.*;

public class PruebaConnection {

    public static void main(String[] args) {
        System.out.println("Hello world!");
        String url = "jdbc:mysql://localhost:3306/masterfinancer?useSSL=false&serverTimezone=UTC";
        try {
            Connection conexion = DriverManager.getConnection(url, "root", "8912");
            Statement instruccion = conexion.createStatement();
            String sql = "SELECT id, nombres, apellidos, username, password, saldo, tipo_identificacion FROM usuarios WHERE 1;";
            ResultSet resultado = instruccion.executeQuery(sql);
            while(resultado.next()){
                System.out.println("Id:"+ resultado.getInt(1));
                System.out.println("Nombre:"+ resultado.getString(2));
                System.out.println("Apellido:"+ resultado.getString(3));
                System.out.println("Username:"+ resultado.getString(4));
            }
            resultado.close();
            instruccion.close();
            conexion.close();
        } catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
}

/**
 * POM Maven
 * <dependencies>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.28</version>
        </dependency>
    </dependencies>
 */