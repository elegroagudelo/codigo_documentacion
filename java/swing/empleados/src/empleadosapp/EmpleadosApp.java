package empleadosapp;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class EmpleadosApp extends JFrame implements ActionListener {
	private float tarifa, horas;
	private JLabel ltitulo, lnombre, lresult, ltarifa, lhoras;
	private JButton bcalcular, blimpiar;
	private JTextField tfnombre, tfhoras, tftarifa;
	private JTextArea taResultado;

	public EmpleadosApp(String title) {
		super(title);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBounds(300, 100, 500, 500);

		ltitulo = new JLabel("Pago de Empleados");
		ltitulo.setBounds(200, 10, 200, 40);

		lnombre = new JLabel("Nombre");
		lnombre.setBounds(40, 60, 110, 40);

		tfnombre = new JTextField(20);
		tfnombre.setBounds(140, 70, 300, 20);
		this.add(tfnombre);

		ltarifa = new JLabel("Tarifa");
		ltarifa.setBounds(40, 100, 200, 40);

		tftarifa = new JTextField(20);
		tftarifa.setBounds(140, 110, 100, 20);
		this.add(tftarifa);

		lhoras = new JLabel("Horas");
		lhoras.setBounds(280, 100, 100, 40);

		tfhoras = new JTextField(20);
		tfhoras.setBounds(340, 110, 100, 20);
		this.add(tfhoras);

		bcalcular = new JButton("Calcular");
		bcalcular.setBounds(100, 150, 130, 20);
		bcalcular.addActionListener(this);

		blimpiar = new JButton("Limpiar");
		blimpiar.setBounds(280, 150, 130, 20);
		blimpiar.addActionListener(this);

		taResultado = new JTextArea("Resultado:");
		taResultado.setBackground(Color.orange);
		taResultado.setBounds(40, 200, 400, 100);

		this.add(taResultado);
		this.add(bcalcular);
		this.add(blimpiar);
		this.add(lhoras);
		this.add(ltarifa);
		this.add(ltitulo);
		this.add(lnombre);
		this.setSize(500, 500);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		float total;
		if (e.getSource() == bcalcular) {
			try {
				tarifa = Float.parseFloat(tftarifa.getText());
				horas = Float.parseFloat(tfhoras.getText());
				total = horas * tarifa;
				taResultado.setText("\n Nombre: $ " + tfnombre.getText());
				taResultado.append("\n Horas: " + String.valueOf(horas));
				taResultado.append("\n Tarifa: $ " + String.valueOf(tarifa));
				taResultado.append("\n Resultado: $ " + String.valueOf(total));
			} catch (NumberFormatException ex) {
				tfnombre.setText("");
				tfhoras.setText("");
				tftarifa.setText("");
				taResultado.setText("Resultado");
				JOptionPane.showMessageDialog(null, "Error Digite un valores correctos");
			}
		}

		if (e.getSource() == blimpiar) {
			int x = JOptionPane.showOptionDialog(this, "Seguro que desea borrar los datos", "Mensage de Confirmar",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (x == 0) {
				tfnombre.setText("");
				tfhoras.setText("");
				tftarifa.setText("");
				taResultado.setText("Resultado");
			}
		}
	}

}
