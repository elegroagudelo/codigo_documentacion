
package agendacontactos;

public class Contactos {
    private String nombres;
    private String apellidos;
    private String email;
    private String direccion;
    private int telefono;

    public Contactos(String nombres, String apellidos, String email, String direccion, int telefono) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.direccion = direccion;
        this.telefono = telefono;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Contactos{" + "nombres=" + nombres + ", apellidos=" + apellidos + ", email=" + email + ", direccion=" + direccion + ", telefono=" + telefono + '}';
    }
    
}
