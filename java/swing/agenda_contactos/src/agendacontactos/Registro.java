
package agendacontactos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;

public class Registro extends JDialog implements ActionListener {

	public Registro(JFrame parent, boolean modal) {
		super(parent, modal);

		ltitulo = new JLabel("Nuevo Contacto");
		ltitulo.setBounds(40, 5, 120, 20);

		contenido = new JPanel();
		contenido.setLayout(null);
		contenido.setBounds(100, 10, 300, 300);
		contenido.setBackground(Color.white);

		sidebar = new JPanel();
		sidebar.setLayout(null);
		sidebar.setBounds(0, 10, 100, 300);
		sidebar.setBackground(Color.gray);

		lnombre = new JLabel("Nombre");
		lapellido = new JLabel("Apellido");
		lemail = new JLabel("Email");
		ldireccion = new JLabel("Direccion");
		ltelefono = new JLabel("Telefono");
		tfnombre = new JTextField();
		tfapellido = new JTextField();
		tfemail = new JTextField();
		tfdireccion = new JTextField();
		tftelefono = new JTextField();
		bGuardar = new JButton("Guardar");

		bGuardar.setBounds(0, 0, 100, 60);
		bGuardar.setPreferredSize(new Dimension(100, 80));
		bGuardar.setBackground(new Color(110, 110, 110));
		bGuardar.setForeground(Color.white);
		bGuardar.setBorder(null);
		bGuardar.addActionListener(this);

		// Label
		lnombre.setBounds(5, 30, 120, 20);
		lapellido.setBounds(5, 60, 120, 20);
		lemail.setBounds(5, 90, 120, 20);
		ldireccion.setBounds(5, 120, 120, 20);
		ltelefono.setBounds(5, 150, 120, 20);

		// TextField
		tfnombre.setBounds(100, 30, 160, 20);
		tfapellido.setBounds(100, 60, 160, 20);
		tfemail.setBounds(100, 90, 160, 20);
		tfdireccion.setBounds(100, 120, 160, 20);
		tftelefono.setBounds(100, 150, 160, 20);

		sidebar.add(bGuardar);

		contenido.add(ltitulo);
		contenido.add(lnombre);
		contenido.add(lapellido);
		contenido.add(lemail);
		contenido.add(ltelefono);
		contenido.add(ldireccion);

		contenido.add(tfnombre);
		contenido.add(tfapellido);
		contenido.add(tfemail);
		contenido.add(tftelefono);
		contenido.add(tfdireccion);

		this.add(contenido);
		this.add(sidebar);
		this.setTitle("Registro Nuevo");
		this.setLayout(null);
		this.setLocation(500, 100);
		this.setSize(400, 300);
		this.setLocation(200, 100);
	}

	private JLabel ltitulo, lnombre, lapellido, lemail, ltelefono, ldireccion;
	private JTextField tfnombre, tfapellido, tfemail, tftelefono, tfdireccion;
	private JPanel contenido, sidebar;
	private JButton bGuardar;
	private Contactos pContact = new Contactos("", "", "", "", 0);
	private Ventana padre = (Ventana) this.getParent();

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bGuardar) {
			pContact = new Contactos(tfnombre.getText(), tfapellido.getText(), tfemail.getText(), tfdireccion.getText(),
					Integer.parseInt(tftelefono.getText()));
			padre.agregarDatos(pContact);
			this.setVisible(false);
			this.padre.setVisible(true);
		}
	}

}
