
package agendacontactos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;
import javax.swing.table.DefaultTableModel;

public class Ventana extends JFrame implements ActionListener {

	public Ventana(String title) {
		super(title);
		this.setLayout(null);

		mainBarra = new JMenuBar();
		m_registros = new JMenu("Registros");
		m_archivo = new JMenu("Archivo");
		itSalir = new JMenuItem("Salir");
		itRegistrar = new JMenuItem("Nuevo Registro");
		contenido = new JPanel();
		sidebar = new JPanel();
		lindex = new JLabel();
		lnombre = new JLabel("Nombre");
		lapellido = new JLabel("Apellido");
		lemail = new JLabel("Email");
		ldireccion = new JLabel("Direccion");
		ltelefono = new JLabel("Telefono");
		tfnombre = new JTextField();
		tfapellido = new JTextField();
		tfemail = new JTextField();
		tfdireccion = new JTextField();
		tftelefono = new JTextField();
		bGuardar = new JButton("Guardar");
		bLimpiar = new JButton("Limpiar");
		bBorrar = new JButton("Borrar");
		bEditar = new JButton("Editar");
		miScroll = new JScrollPane();
		miTabla = new JTable();

		mainBarra.setBackground(new Color(150, 50, 50));
		mainBarra.setMaximumSize(new Dimension(136, 429));
		mainBarra.setMinimumSize(new Dimension(0, 3));
		mainBarra.setPreferredSize(new Dimension(140, 50));

		m_registros.setForeground(new Color(255, 255, 255));
		m_registros.setFont(new Font("Segoe UI", 1, 15));
		m_registros.setMargin(new java.awt.Insets(0, 30, 0, 30));

		m_archivo.setMargin(new java.awt.Insets(0, 30, 0, 30));
		m_archivo.setForeground(new Color(255, 255, 255));
		m_archivo.setFont(new Font("Segoe UI", 1, 15));

		itSalir.setPreferredSize(new Dimension(150, 30));
		itSalir.addActionListener(this);
		itRegistrar.setPreferredSize(new Dimension(150, 30));
		itRegistrar.addActionListener(this);
		m_archivo.add(itSalir);
		m_registros.add(itRegistrar);

		// Button
		bGuardar.setBounds(0, 0, 100, 60);
		bGuardar.setPreferredSize(new Dimension(100, 80));
		bGuardar.setBackground(new Color(110, 110, 110));
		bGuardar.setForeground(Color.white);
		bGuardar.setBorder(null);
		bGuardar.addActionListener(this);

		bLimpiar.setBounds(0, 100, 100, 60);
		bLimpiar.setPreferredSize(new Dimension(100, 80));
		bLimpiar.setBackground(new Color(110, 110, 110));
		bLimpiar.setForeground(Color.white);
		bLimpiar.setBorder(null);
		bLimpiar.addActionListener(this);

		bBorrar.setBounds(0, 200, 100, 60);
		bBorrar.setPreferredSize(new Dimension(100, 80));
		bBorrar.setBackground(new Color(110, 110, 110));
		bBorrar.setForeground(Color.white);
		bBorrar.setBorder(null);
		bBorrar.addActionListener(this);

		bEditar.setBounds(0, 300, 100, 60);
		bEditar.setPreferredSize(new Dimension(100, 80));
		bEditar.setBackground(new Color(110, 110, 110));
		bEditar.setForeground(Color.white);
		bEditar.setBorder(null);
		bEditar.addActionListener(this);

		// contenido
		contenido.setLayout(null);
		contenido.setBounds(100, 0, 500, 500);
		contenido.setBackground(Color.white);
		contenido.setPreferredSize(new Dimension(400, 500));

		// opciones
		sidebar.setBounds(0, 0, 100, 500);
		sidebar.setBackground(Color.gray);
		sidebar.setPreferredSize(new Dimension(200, 500));

		miScroll.setBounds(10, 0, 490, 300);
		Object[][] filas = {};
		Object[] columnas = { "Nombre", "Apellido", "Email", "Direccion", "Telefono" };
		modelo = new DefaultTableModel(filas, columnas);

		miTabla.setModel(modelo);

		mainBarra.add(m_archivo);
		mainBarra.add(m_registros);
		this.setJMenuBar(mainBarra);

		miScroll.setViewportView(miTabla);

		// Label
		lnombre.setBounds(15, 330, 120, 30);
		lapellido.setBounds(15, 360, 120, 30);
		lemail.setBounds(15, 390, 120, 30);
		ldireccion.setBounds(15, 420, 120, 30);
		ltelefono.setBounds(15, 450, 120, 30);

		// TextField
		tfnombre.setBounds(100, 330, 200, 30);
		tfapellido.setBounds(100, 360, 200, 30);
		tfemail.setBounds(100, 390, 200, 30);
		tfdireccion.setBounds(100, 420, 200, 30);
		tftelefono.setBounds(100, 450, 200, 30);

		lindex.setBounds(15, 300, 200, 30);

		// Contenidos
		sidebar.add(bLimpiar);
		sidebar.add(bGuardar);
		sidebar.add(bBorrar);
		sidebar.add(bEditar);
		contenido.add(miScroll);

		contenido.add(lnombre);
		contenido.add(lapellido);
		contenido.add(lemail);
		contenido.add(ldireccion);
		contenido.add(ltelefono);
		contenido.add(lindex);

		contenido.add(tfnombre);
		contenido.add(tfapellido);
		contenido.add(tfemail);
		contenido.add(tfdireccion);
		contenido.add(tftelefono);

		this.add(sidebar);
		this.add(contenido);
		// Ventana
		this.setLocation(300, 100);
		this.setSize(620, 620);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	private JTextField tfnombre, tfapellido, tfemail, tfdireccion, tftelefono;
	private JPanel contenido, sidebar;
	private JLabel lnombre, lapellido, lemail, ldireccion, ltelefono, lindex;
	private JButton bGuardar, bLimpiar, bBorrar, bEditar;
	private JMenu m_registros, m_archivo;
	private JMenuBar mainBarra;
	private JMenuItem itSalir, itRegistrar;
	private JScrollPane miScroll;
	private JTable miTabla;
	private DefaultTableModel modelo;

	public void agregarDatos(Contactos con) {
		Object[] fila = { con.getNombres(), con.getApellidos(), con.getEmail(), con.getDireccion(), con.getTelefono() };
		modelo.addRow(fila);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == itRegistrar) {
			Registro window = new Registro(this, true);
			window.setVisible(true);
		}

		if (e.getSource() == bBorrar) {
			try {
				modelo.removeRow(miTabla.getSelectedRow());
			} catch (ArrayIndexOutOfBoundsException aIE) {
				JOptionPane.showMessageDialog(this, "No se ha seleccionado un item de la tabla para borrar");
			}
		}

		if (e.getSource() == bEditar) {
			if (miTabla.getSelectedRows().length > 0) {

				int selection = miTabla.getSelectedRow();
				lindex.setText("" + selection);

				tfnombre.setText((String) miTabla.getModel().getValueAt(miTabla.getSelectedRow(), 0));
				tfapellido.setText((String) miTabla.getModel().getValueAt(miTabla.getSelectedRow(), 1));
				tfemail.setText((String) miTabla.getModel().getValueAt(miTabla.getSelectedRow(), 2));
				tfdireccion.setText((String) miTabla.getModel().getValueAt(miTabla.getSelectedRow(), 3));
				tftelefono.setText("" + (Integer) miTabla.getModel().getValueAt(miTabla.getSelectedRow(), 4));
			} else {
				JOptionPane.showMessageDialog(this, "No se ha seleccionado un item de la tabla para editar");
			}
		}

		if (e.getSource() == bGuardar) {
			if (lindex.getText() != "") {
				int fila = Integer.parseInt(lindex.getText());

				modelo.setValueAt(tfnombre.getText(), fila, 0);
				modelo.setValueAt(tfapellido.getText(), fila, 1);
				modelo.setValueAt(tfemail.getText(), fila, 2);
				modelo.setValueAt(tfdireccion.getText(), fila, 3);
				modelo.setValueAt(tftelefono.getText(), fila, 4);
				JOptionPane.showMessageDialog(this, "OK");
			} else {
				JOptionPane.showMessageDialog(this, "No hay datos para guardar");
			}
		}

		if (e.getSource() == bLimpiar) {
			tfnombre.setText("");
			tfapellido.setText("");
			lindex.setText("");
			tfemail.setText("");
			tfdireccion.setText("");
			tftelefono.setText("");

		}

		if (e.getSource() == itSalir) {
			System.exit(0);
		}

	}
}
