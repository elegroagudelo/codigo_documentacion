package agendacontactos;

public class Usuarios {
  private int identificacion;
  private String nombre;
  private String telefono;
  private String email;

    public Usuarios(int identificacion, String nombre, String telefono, String email) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
    }

  
    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "usuarios{" + "identificacion=" + identificacion + ", nombre=" + nombre + ", telefono=" + telefono + ", email=" + email + '}';
    }
  
  
}
