
package principal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Importes extends JFrame implements ActionListener {

	private JTextArea taResultado;
	private JButton bCalcular;
	private JLabel lbColegio, lbTitulo, lbEstrato, lbResultado;
	private JCheckBox cbPrivado, cbPublico;
	private JComboBox<String> coEstrato;

	public Importes(String title) {
		super(title);
		this.setLayout(null);
		this.setLocation(600, 100);

		lbTitulo = new JLabel("IMPORTES A PAGAR");
		lbTitulo.setBounds(80, 5, 200, 20);
		this.add(lbTitulo);

		lbEstrato = new JLabel("Seleccionar el estrato");
		lbEstrato.setBounds(10, 30, 200, 20);
		this.add(lbEstrato);

		coEstrato = new JComboBox<String>();
		for (int i = 1; i <= 6; i++) {
			coEstrato.addItem("Estrato " + i);
		}
		coEstrato.setBounds(150, 30, 100, 20);
		this.add(coEstrato);

		lbColegio = new JLabel("Seleccione  el tipo Colegio:");
		this.add(lbColegio);
		lbColegio.setBounds(10, 50, 200, 20);

		cbPrivado = new JCheckBox("Privado", false);
		cbPrivado.setBounds(10, 70, 100, 20);
		this.add(cbPrivado);

		cbPublico = new JCheckBox("Publico", false);
		cbPublico.setBounds(150, 70, 100, 20);
		this.add(cbPublico);

		bCalcular = new JButton("CALCULAR");
		bCalcular.setBounds(90, 110, 100, 20);
		this.add(bCalcular);
		bCalcular.addActionListener(this);

		lbResultado = new JLabel("Resultado");
		lbResultado.setBounds(100, 130, 100, 20);
		this.add(lbResultado);

		taResultado = new JTextArea("Resultado: \n");
		taResultado.setBounds(30, 150, 200, 60);
		this.add(taResultado);

		this.setSize(400, 500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bCalcular) {
			if (cbPrivado.isSelected() == true && cbPublico.isSelected() == true) {
				taResultado.setText("");
				JOptionPane.showMessageDialog(null, "ERROR: selecionar un solo tipo de colegio");
			} else {
				try {
					taResultado.setText("");
					int estrato = coEstrato.getSelectedIndex();
					int importe = 0;
					if (cbPrivado.isSelected()) {
						switch (estrato) {
							case 0:
								importe = 50000;
								break;
							case 1:
								importe = 60000;
								break;
							case 2:
								importe = 70000;
								break;
							case 3:
								importe = 80000;
								break;
							case 4:
								importe = 90000;
								break;
							case 5:
								importe = 100000;
								break;
						}
					}

					if (cbPublico.isSelected()) {
						switch (estrato) {
							case 0:
								importe = 20000;
								break;
							case 1:
								importe = 30000;
								break;
							case 2:
								importe = 40000;
								break;
							case 3:
								importe = 50000;
								break;
							case 4:
								importe = 60000;
								break;
							case 5:
								importe = 70000;
								break;
						}
					}

					taResultado.setText("\n Importe a cancelar $" + String.valueOf(importe));
				} catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null, "ERROR: Digite un número válido");
				}
			}
		}

	}

}
