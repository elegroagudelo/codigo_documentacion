package condicionalesp2;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Libretas extends JFrame implements ActionListener{
        private JTextArea taResultado;
	private JButton bCalcular;
	private JLabel lbTitulo, lbEdad, lbEstrato;
	private JTextField tfedad;
	private JRadioButton rbEstrato1, rbEstrato2, rbEstrato3;
	private ButtonGroup bgEstratos;
	private double descuento, base;

    public Libretas(String title){
    super(title);
    this.setLayout(null);
    this.setLocation(600, 100);

    lbTitulo= new JLabel("PAGO DE LIBRETA");
    lbTitulo.setBounds(80, 5, 200, 20);
    this.add(lbTitulo);

    lbEdad = new JLabel("Digite la edad");
    lbEdad.setBounds(10, 30, 100, 20);
    this.add(lbEdad);

    tfedad= new JTextField(20);
	tfedad.setBounds(100, 30, 100, 20);
    this.add(tfedad);


    lbEstrato = new JLabel("Seleccionar el estrato:");
    lbEstrato.setBounds(10, 50, 200, 20);
    this.add(lbEstrato);

    rbEstrato1= new JRadioButton("Estrato 1");
    rbEstrato1.setBounds(10, 70, 100, 20);
    this.add(rbEstrato1);

    rbEstrato2= new JRadioButton("Estrato 2");
    rbEstrato2.setBounds(10, 90, 100, 20);
    this.add(rbEstrato2);

    rbEstrato3= new JRadioButton("Estrato 3");
    rbEstrato3.setBounds(10, 110, 100, 20);
    this.add(rbEstrato3);

	bgEstratos=new ButtonGroup();
	bgEstratos.add(rbEstrato1);
	bgEstratos.add(rbEstrato2);
	bgEstratos.add(rbEstrato3);
	rbEstrato1.setSelected(true);

	bCalcular=new JButton("CALCULAR");
	bCalcular.setBounds(60, 130, 200, 20);
	this.add(bCalcular);
	bCalcular.addActionListener(this);

	taResultado=new JTextArea("Resultado: \n");
	taResultado.setBounds(60, 170, 200, 50);
	this.add(taResultado);


    this.setSize(300, 500);
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);        
    this.setLocation(200, 100);
    this.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    	if (e.getSource()== bCalcular){
          try{
            int edad= Integer.parseInt(tfedad.getText());
            if (edad > 18) {
            	base= 350000;
            	if(rbEstrato1.isSelected()){ descuento= 0.4; }
            	if(rbEstrato2.isSelected()){ descuento= 0.3; }
            	if(rbEstrato3.isSelected()){ descuento= 0.15; }            	
            }else if(edad== 18 ){
            	base= 200000;
            	if(rbEstrato1.isSelected()){ descuento= 0.6; }
            	if(rbEstrato2.isSelected()){ descuento= 0.4; }
            	if(rbEstrato3.isSelected()){ descuento= 0.2; }    
            }else{ 
               JOptionPane.showMessageDialog(null, "Esta persona es menor de edad");
            }
          
            double total=  (base * (descuento + 1))- base;
            taResultado.setText("El Valor a Pagar es: "+total+"\nSu descuento es: "+ (base- total));

           }catch( NumberFormatException nfe){
	        JOptionPane.showMessageDialog(null, "ERROR: Digite una edad válida");
	    }
    	}
    }
    
} 
