
package condicionalesp2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Confenalco extends JFrame implements ActionListener {
	
	private JTextArea taResultado;
	private JButton bCalcular;
	private JLabel lbTitulo, lbTipo, lbClase, lbCantidad, lbVolante;
	private JComboBox cbTipo;
	private JRadioButton rbClase1, rbClase2, rbClase3, rbClase4;
	private ButtonGroup bgClases;
	private JTextField tfCantidad;
	private JCheckBox ckVolantes;

	public Confenalco(String title) {
		super(title);
		this.setLayout(null);
		this.setLocation(800, 100);

		lbTitulo = new JLabel("ENTRADA A CONFELANCO");
		lbTitulo.setBounds(80, 5, 200, 20);
		this.add(lbTitulo);

		lbClase = new JLabel("Selecciona la clase:");
		lbClase.setBounds(10, 30, 200, 20);
		this.add(lbClase);

		rbClase1 = new JRadioButton("Clase A");
		rbClase1.setBounds(10, 50, 200, 20);
		this.add(rbClase1);

		rbClase2 = new JRadioButton("Clase B");
		rbClase2.setBounds(10, 70, 200, 20);
		this.add(rbClase2);

		rbClase3 = new JRadioButton("Clase C");
		rbClase3.setBounds(10, 90, 200, 20);
		this.add(rbClase3);

		rbClase4 = new JRadioButton("NO Afiliado");
		rbClase4.setBounds(10, 110, 200, 20);
		this.add(rbClase4);

		bgClases = new ButtonGroup();
		bgClases.add(rbClase1);
		bgClases.add(rbClase2);
		bgClases.add(rbClase3);
		bgClases.add(rbClase4);
		rbClase1.setSelected(true);

		lbTipo = new JLabel("Selecciona el Tipo:");
		lbTipo.setBounds(10, 130, 200, 20);
		this.add(lbTipo);

		cbTipo = new JComboBox();
		cbTipo.addItem("NIÑO");
		cbTipo.addItem("ADULTOS");
		cbTipo.setBounds(120, 130, 100, 20);
		this.add(cbTipo);

		lbCantidad = new JLabel("Cantidad de personas");
		lbCantidad.setBounds(10, 170, 200, 20);
		this.add(lbCantidad);

		tfCantidad = new JTextField(20);
		tfCantidad.setBounds(150, 170, 100, 20);
		this.add(tfCantidad);

		lbVolante = new JLabel("Selecciona si posee:");
		lbVolante.setBounds(10, 200, 200, 20);
		this.add(lbVolante);

		ckVolantes = new JCheckBox("Volante");
		ckVolantes.setBounds(170, 200, 140, 20);
		this.add(ckVolantes);

		bCalcular = new JButton("CALCULAR");
		bCalcular.setBounds(60, 230, 200, 20);
		this.add(bCalcular);
		bCalcular.addActionListener(this);

		taResultado = new JTextArea("Resultado: \n");
		taResultado.setBounds(60, 250, 200, 50);
		this.add(taResultado);

		this.setSize(300, 400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocation(200, 100);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		taResultado.setText("");
		int valortipo = 0;
		double total = 0;
		if (rbClase1.isSelected()) {
			if (cbTipo.getSelectedIndex() == 0) {
				valortipo = 2000;
			}
			if (cbTipo.getSelectedIndex() == 1) {
				valortipo = 3000;
			}

		}
		if (rbClase2.isSelected()) {
			if (cbTipo.getSelectedIndex() == 0) {
				valortipo = 4000;
			}
			if (cbTipo.getSelectedIndex() == 1) {
				valortipo = 3000;
			}

		}
		if (rbClase3.isSelected()) {
			if (cbTipo.getSelectedIndex() == 0) {
				valortipo = 5000;
			}
			if (cbTipo.getSelectedIndex() == 1) {
				valortipo = 4000;
			}

		}
		if (rbClase4.isSelected()) {
			if (cbTipo.getSelectedIndex() == 0) {
				valortipo = 6000;
			}
			if (cbTipo.getSelectedIndex() == 1) {
				valortipo = 5000;
			}
		}

		// Descuentos
		if (ckVolantes.isSelected()) {
			total = (valortipo - (valortipo * 10) / 100);
		}

		try {
			int cantidad = Integer.parseInt(tfCantidad.getText());
			if (cantidad >= 10) {
				total = (total - (total * 10) / 100);
			}

		} catch (NumberFormatException nfe) {
			JOptionPane.showMessageDialog(null, "ERROR: Digite una cantidad de personas correcta");
		}

		taResultado.setText("Resultado: valor a cancelar: " + total);

	}
}