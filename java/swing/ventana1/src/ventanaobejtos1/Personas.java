
package ventanaobejtos1;

public class Personas {
    private String dNombre;
    private String dApellido;
    private double dEdad;
    private double dH;
    private double dP;

    public Personas(String dNombre, String dApellido, double dEdad, double dH, double dP) {
        this.dNombre = dNombre;
        this.dApellido = dApellido;
        this.dEdad = dEdad;
        this.dH = dH;
        this.dP = dP;
    }

    // Datos+++++++++++++++++++++++++++++++++++++++++++++++
    public double getdP() {
        return dP;
    }

    public void setdP(double dP) {
        this.dP = dP;
    }

    public double getdH() {
        return dH;
    }

    public void setdH(double dH) {
        this.dH = dH;
    }

    public double getdEdad() {
        return dEdad;
    }

    public void setdEdad(double dEdad) {
        this.dEdad = dEdad;
    }

    public String getdApellido() {
        return dApellido;
    }

    public void setdApellido(String dApellido) {
        this.dApellido = dApellido;
    }

    public String getdNombre() {
        return dNombre;
    }

    public void setdNombre(String dNombre) {
        this.dNombre = dNombre;
    }
}
