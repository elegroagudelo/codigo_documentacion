
package ventanaobejtos1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Ventana extends JFrame implements ActionListener {
    private JLabel lTitulo, lResultado, lnombre, lapellidos, ledad, lp, lh;
    private JButton bCalcular;
    private JTextField tfEdad, tfNombre, tfApellido, tfP, tfH;
    private JTextArea taResultado;

    public Ventana(String title) {
        super(title);
        this.setLayout(null);
        this.setLocation(300, 100);

        lTitulo = new JLabel("Indice de masa corporal");
        lTitulo.setBounds(100, 10, 200, 20);
        this.add(lTitulo);

        lnombre = new JLabel("Ingresar nombres");
        lnombre.setBounds(10, 40, 150, 20);
        this.add(lnombre);

        tfNombre = new JTextField(20);
        tfNombre.setBounds(160, 40, 150, 20);
        this.add(tfNombre);

        lapellidos = new JLabel("Ingresar apellidos");
        lapellidos.setBounds(10, 60, 150, 20);
        this.add(lapellidos);

        tfApellido = new JTextField(20);
        tfApellido.setBounds(160, 60, 150, 20);
        this.add(tfApellido);

        ledad = new JLabel("Ingresar la edad");
        ledad.setBounds(10, 80, 150, 20);
        this.add(ledad);

        tfEdad = new JTextField(20);
        tfEdad.setBounds(160, 80, 150, 20);
        this.add(tfEdad);

        lp = new JLabel("Ingresar el peso");
        lp.setBounds(10, 100, 150, 20);
        this.add(lp);

        tfP = new JTextField(20);
        tfP.setBounds(160, 100, 150, 20);
        this.add(tfP);

        lh = new JLabel("Ingresar altura en metros");
        lh.setBounds(10, 120, 220, 20);
        this.add(lh);

        tfH = new JTextField(20);
        tfH.setBounds(230, 120, 120, 20);
        this.add(tfH);

        bCalcular = new JButton("CALCULAR");
        bCalcular.setBounds(100, 150, 200, 30);
        this.add(bCalcular);
        bCalcular.addActionListener(this);

        lResultado = new JLabel("Resultado");
        lResultado.setBounds(100, 190, 200, 20);
        this.add(lResultado);

        taResultado = new JTextArea();
        taResultado.setBounds(100, 220, 200, 200);
        this.add(taResultado);

        this.setSize(400, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocation(200, 100);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String nombre = tfNombre.getText();
            String apellido = tfApellido.getText();
            double edad = Double.parseDouble(tfEdad.getText());
            double dH = Double.parseDouble(tfH.getText());
            double dP = Double.parseDouble(tfP.getText());

            Personas p = new Personas(nombre, apellido, edad, dH, dP);
            double total = p.getdP() / (p.getdH() * p.getdH());
            taResultado.setText("Nombre: " + p.getdNombre() + "\nApellido: " + p.getdApellido() + "\nEdad: "
                    + p.getdEdad() + "\nMasa Corporal:  " + String.valueOf(total));
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "ERROR: ingresar datos correctos");
        }

    }

}
