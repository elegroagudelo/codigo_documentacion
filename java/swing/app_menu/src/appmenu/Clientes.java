
package appmenu;

public class Clientes {
    public Clientes(String nombres, String apellidos, String genero, int identidad) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.genero = genero;
        this.identidad = identidad;
    }

    public Clientes() {
    }
    
    private  String nombres, apellidos, genero;
    private int identidad;
    
    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getGenero() {
        return genero;
    }

    public int getIdentidad() {
        return identidad;
    }

    @Override
    public String toString() {
        return "Clientes{ " + "nombres=" + nombres + ", apellidos=" + apellidos + ", genero=" + genero + ", identidad=" + identidad + '}';
    }
     
}
