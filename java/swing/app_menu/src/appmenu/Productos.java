
package appmenu;

public class Productos {
   private String producto;
   private int codigo;

    public Productos(String producto, int codigo) {
        this.producto = producto;
        this.codigo = codigo;
    }

   
    public String getProducto() {
        return producto;
    }

    public int getCodigo() {
        return codigo;
    }

    @Override
    public String toString() {
        return "Productos{" + "producto=" + producto + ", codigo=" + codigo + '}';
    }
    
}
