package applist;

import java.sql.*;
import javax.swing.JOptionPane;

public class Conexion {

    public Conexion() {
    }

    private Connection dCon;

    public Connection getDCon() {
        dCon = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            dCon = DriverManager.getConnection("jdbc:mysql://localhost/dbagenda", "root", "12345");
            System.out.println("conexion establceida");
            JOptionPane.showMessageDialog(null, "conexion establecida");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("error no es posible la conexion");
            JOptionPane.showMessageDialog(null, "error no es posible la conexion" + e);
        }
        return dCon;
    }

}
