
package applist;

public class Clientes {

    public Clientes(String nombres, int Identidad, double monto) {
        this.nombres = nombres;
        this.Identidad = Identidad;
        this.monto = monto;
    }

    private String nombres;
    private int Identidad;
    private double monto;

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getIdentidad() {
        return Identidad;
    }

    public void setIdentidad(int Identidad) {
        this.Identidad = Identidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @Override
    public String toString() {
        return "Clientes{" + "nombres=" + nombres + ", Identidad=" + Identidad + ", monto=" + monto + '}';
    }

}
