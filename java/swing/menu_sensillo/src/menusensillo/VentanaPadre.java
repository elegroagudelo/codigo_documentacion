
package menusensillo;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;

public class VentanaPadre extends JFrame implements ActionListener{
    
    public VentanaPadre(String title) {
    super(title);
    this.setLayout(null);
    this.setLocation(300, 100);
    
    lTitulo= new JLabel("Prestamo de Equipos");
    lTitulo.setBounds(120,20,400,50);
    lTitulo.setForeground(Color.orange);
    lTitulo.setFont(new Font("Segoe UI", 2, 30));
    this.add(lTitulo);
    
    MenuBarra= new JMenuBar();
    MenuBarra.setBackground(new Color(16, 164, 208));
    MenuBarra.setMaximumSize(new Dimension(136, 42769));
    MenuBarra.setMinimumSize(new Dimension(0, 3));
    MenuBarra.setPreferredSize(new Dimension(136, 45));

    Logo = new JMenu();
    Logo.setForeground(new Color(0, 0, 0));
    Logo.setFont(new Font("Segoe UI", 2, 14));
    Logo.setIcon(new ImageIcon(getClass().getResource("/menusensillo/Logo.png")));
    
    Inicio= new JMenu("Inicio");
    Inicio.setMargin(new java.awt.Insets(0, 20, 0, 20)); 
    Inicio.setForeground(new Color(255, 255, 255));
    Inicio.setFont(new Font("Segoe UI", 2, 14));
    Inicio.setIcon(new ImageIcon(getClass().getResource("/menusensillo/inicio.png")));    
    
    Inicio.add("Cerrar");
    Inicio.add("Imprimir");
    Inicio.add("Guardar");
    
    Registros= new JMenu("Registros");
    Registros.setMargin(new java.awt.Insets(0, 20, 0, 20)); 
    Registros.setForeground(new Color(255, 255, 255));
    Registros.setFont(new Font("Segoe UI", 2, 14));
    Registros.setIcon(new ImageIcon(getClass().getResource("/menusensillo/registros.png")));   
    
    Equipos= new JMenu();
    Equipos.setText("Equipos");
    Equipos.setMargin(new java.awt.Insets(0, 20, 0, 20)); 
    Equipos.setForeground(new Color(255, 255, 255));
    Equipos.setFont(new Font("Segoe UI", 2, 14));
    Equipos.setIcon(new ImageIcon(getClass().getResource("/menusensillo/equipos.png")));
    
    Reservas = new JMenu(); 
    Reservas.setText("Reservas");
    Reservas.setMargin(new java.awt.Insets(0, 20, 0, 20));
    Reservas.setForeground(new Color(255, 255, 255));
    Reservas.setFont(new Font("Segoe UI", 2, 14));
    Reservas.setIcon(new ImageIcon(getClass().getResource("/menusensillo/reservar.png")));
    
    Config = new JMenu(); 
    Config.setText("Configuraciones");
    Config.setMargin(new java.awt.Insets(0, 20, 0, 20));
    Config.setForeground(new Color(255, 255, 255));
    Config.setFont(new Font("Segoe UI", 2, 14));
    Config.setIcon(new ImageIcon(getClass().getResource("/menusensillo/btnmenu.png")));
    
    Perfil = new JMenu(); 
    Perfil.setText("Perfil");
    Perfil.setMargin(new java.awt.Insets(0, 20, 0, 20));
    Perfil.setForeground(new Color(255, 255, 255));
    Perfil.setFont(new Font("Segoe UI", 2, 14));
    Perfil.setIcon(new ImageIcon(getClass().getResource("/menusensillo/Usuario.png")));
        
    Salir = new JMenuItem(); 
    Salir.setText("Salir");
    Salir.setMargin(new java.awt.Insets(2, 20, 2, 20));
    Salir.setForeground(new Color(0, 0, 0));
    Salir.setFont(new Font("Segoe UI", 2, 14));
    Salir.setIcon(new ImageIcon(getClass().getResource("/menusensillo/salir.png")));
    Salir.addActionListener(this);
    
    Re_Usuarios = new JMenuItem();  
    Re_Usuarios.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_MASK | InputEvent.CTRL_MASK));
    Re_Usuarios.setText("Usuarios"); 
    Re_Usuarios.addActionListener(this);
    
    Re_Equipos = new JMenuItem();      
    Re_Equipos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.SHIFT_MASK | InputEvent.CTRL_MASK));
    Re_Equipos.setText("Equipos");
    Re_Equipos.addActionListener(this);
    
   
    Equipos.add(Re_Equipos);    
    Registros.add(Re_Usuarios);
    Inicio.add(Salir);    
    
    this.add(MenuBarra);
    MenuBarra.add(Logo);
    MenuBarra.add(Inicio);
    MenuBarra.add(Registros);
    MenuBarra.add(Equipos);
    MenuBarra.add(Reservas);
    MenuBarra.add(Config);
    MenuBarra.add(Perfil);
    
    
    setJMenuBar(MenuBarra);
    
    this.setSize(900, 300);
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);        
    this.setLocation(200, 100);
    this.setVisible(true);  
    }
 
    private JLabel lTitulo;
    private JMenu Inicio;
    private JMenu Config;
    private JMenu Registros;
    private JMenu Reservas;
    private JMenu Equipos;
    private JMenu Perfil;
    private JMenu Logo;
    private JMenuItem Salir;
    private JMenuBar MenuBarra;
    private JMenuItem Re_Usuarios;
    private JMenuItem Re_Equipos;
    
    
    @Override
    public void actionPerformed(ActionEvent e) { 
    if (e.getSource()== Re_Usuarios){
         VentanaH1 window= new VentanaH1();
    }
    if (e.getSource()== Re_Equipos){
         VentanaH2 window= new VentanaH2();
    }
    if (e.getSource()== Salir){
       System.exit(0);
    }
    
    }

}
