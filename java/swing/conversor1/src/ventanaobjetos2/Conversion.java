
package ventanaobjetos2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Conversion extends JFrame implements ActionListener {
    private double base;
    private JComboBox tipo1, tipo2;
    private JLabel lTitulo, ltipo1, ltipo2, lbase, lResultado;
    private JButton bCalcular;
    private JTextArea taResultado;
    private JTextField tfbase;

    public Conversion(String title) {
        super(title);
        this.setLayout(null);
        this.setLocation(300, 100);

        lTitulo = new JLabel("CONVERSION");
        lTitulo.setBounds(150, 10, 200, 20);
        this.add(lTitulo);

        ltipo1 = new JLabel("Unidad de medida base");
        ltipo1.setBounds(10, 40, 200, 20);
        this.add(ltipo1);

        this.add(getTipo1());
        this.add(getTipo2());

        ltipo2 = new JLabel("Unidad a Convertir");
        ltipo2.setBounds(10, 70, 150, 20);
        this.add(ltipo2);

        lbase = new JLabel("Cantidad a convertir");
        lbase.setBounds(10, 100, 150, 20);
        this.add(lbase);

        tfbase = new JTextField(20);
        tfbase.setBounds(160, 100, 150, 20);
        this.add(tfbase);

        bCalcular = new JButton("CALCULAR");
        bCalcular.setBounds(100, 150, 200, 30);
        this.add(bCalcular);
        bCalcular.addActionListener(this);

        lResultado = new JLabel("Resultado");
        lResultado.setBounds(100, 190, 200, 20);
        this.add(lResultado);

        taResultado = new JTextArea();
        taResultado.setBounds(100, 220, 200, 100);
        this.add(taResultado);

        this.setSize(400, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocation(200, 100);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (tipo2.getSelectedIndex() >= 0 && tipo1.getSelectedIndex() >= 0) {
                double total = 0;
                base = Double.parseDouble(tfbase.getText());
                total = getConversion(base);

                taResultado.setText("Conversion: " + String.valueOf(total));

            }

        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "ERROR: Digite datos correctos");
        }
    }

    public JComboBox getTipo1() {
        tipo1 = new JComboBox();
        tipo1.setBounds(200, 40, 180, 20);
        tipo1.addItem("Kilometros");
        tipo1.addItem("Metros");
        tipo1.addItem("Centimetros");
        tipo1.addItem("Pies");
        return tipo1;
    }

    public void setTipo1(JComboBox tipo1) {
        this.tipo1 = tipo1;
    }

    public JComboBox getTipo2() {
        tipo2 = new JComboBox();
        tipo2.setBounds(200, 70, 180, 20);
        tipo2.addItem("Kilometros");
        tipo2.addItem("Metros");
        tipo2.addItem("Centimetros");
        tipo2.addItem("Pies");
        return tipo2;
    }

    public void setTipo2(JComboBox tipo2) {
        this.tipo2 = tipo2;
    }

    public double getConversion(double base) {
        double x, y, valor;
        valor = 0;

        x = tipo1.getSelectedIndex();
        y = tipo2.getSelectedIndex();

        if (x == 0 && y == 1) {
            valor = (base * 1000); // km-m
        }

        if (x == 0 && y == 2) {
            valor = (base * 1000) * 100; // km-cm
        }

        if (x == 0 && y == 3) {
            valor = (base * 3280); // km-pies
        }

        if (x == 1 && y == 0) {
            valor = (base / 1000); // m-Km
        }

        if (x == 1 && y == 2) {
            valor = (base * 100); // m-cm
        }

        if (x == 1 && y == 3) {
            valor = (base / 32.08) * 100; // m-pies
        }

        if (x == 2 && y == 0) {
            valor = (base / 1000) / 100; // cm-Km
        }

        if (x == 2 && y == 1) {
            valor = (base / 1000); // cm-m
        }

        if (x == 2 && y == 3) {
            valor = (base / 32.08); // cm-pies
        }

        if (x == 3 && y == 0) {
            valor = (base / 0.3208) * 1000; // pies-Km
        }

        if (x == 3 && y == 1) {
            valor = (base / 0.3208); // pies-m
        }

        if (x == 3 && y == 0) {
            valor = base / (0.3208 / 100); // pies-cm
        }

        return valor;
    }

}
