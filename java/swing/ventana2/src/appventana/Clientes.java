
package appventana;
public class Clientes {

    public Clientes(String nombre, int numeroCuenta, double valorMovimiento, String tipoMovimiento) {
        this.nombre = nombre;
        this.numeroCuenta = numeroCuenta;
        this.valorMovimiento = valorMovimiento;
        this.tipoMovimiento = tipoMovimiento;
    }
    
    private String nombre;
    private int numeroCuenta;
    private double valorMovimiento;
    private String tipoMovimiento;

    
    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public double getValorMovimiento() {
        return valorMovimiento;
    }

    public void setValorMovimiento(double valorMovimiento) {
        this.valorMovimiento = valorMovimiento;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Clientes{" + "\nnombre=" + nombre + ",\nnumeroCuenta=" + numeroCuenta + ",\nvalorMovimiento=" + valorMovimiento + ",\ntipoMovimiento=" + tipoMovimiento + '}';
    }

}
