
package appventana;
import java.util.ArrayList;

public class Caja {

    public Caja(String nombre) {
        this.nombreCajero = nombre;
    }
    
    Clientes nuevo= new Clientes("",0,0,"");  
    public ArrayList<Clientes> registros  = new ArrayList<Clientes>();
    private String nombreCajero;

    
    public void ingresarRegistro(Clientes c){
     registros.add(c);
    }
    
    public String getNombreCajero() {
        return nombreCajero;
    }
    public void setNombreCajero(String nombreCajero) {
        this.nombreCajero = nombreCajero;
    }

    
    @Override
    public String toString() {
        String resultado = "Registros del dia";
        for(int i=0; i < registros.size(); i++ )
        {
          Clientes c= registros.get(i);
          resultado+= "\nNombre Cajero: "+ getNombreCajero()+ "\n" + c.toString()+"\n";
        }
        return resultado;
    }
}
