
package condicional1;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Pensiones extends JFrame implements ActionListener {
	
	private JComboBox<String> cmdGenero;
	private JTextArea taResultado;
	private JTextField tfAnno;
	private JButton btnCalcular;
	public String genero, pension;
	public JLabel lbfecha, lbgenero;

	public Pensiones(String title) {
		super(title);
		this.setLayout(new FlowLayout());

		lbfecha = new JLabel();
		lbfecha.setText("Seleciona el año");
		this.add(lbfecha);

		tfAnno = new JTextField(10);
		this.add(tfAnno);

		lbgenero = new JLabel();
		lbgenero.setText("Genero: ");
		this.add(lbgenero);

		cmdGenero = new JComboBox<String>();
		cmdGenero.addItem("Femenino");
		cmdGenero.addItem("Masculino");
		this.add(cmdGenero);
		cmdGenero.setBounds(50, 80, 90, 30);

		btnCalcular = new JButton("..........Calcular..............");
		btnCalcular.addActionListener(this);
		this.add(btnCalcular);

		taResultado = new JTextArea("Resultado:.......... \n");
		this.add(taResultado);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (cmdGenero.getSelectedIndex() == 0) {
			this.genero = cmdGenero.getSelectedItem().toString();
		} else if (cmdGenero.getSelectedIndex() == 1) {
			this.genero = cmdGenero.getSelectedItem().toString();
		}

		try {
			double anno = Double.parseDouble(tfAnno.getText());
			double edad = (2015 - anno);

			if (cmdGenero.getSelectedIndex() == 1) {
				if (edad < 57) {
					double valor = 57 - edad;
					this.pension = "Su edad es de: " + edad + "\n Tiempo a pensionar: " + valor;
				} else {
					this.pension = "Esta pensionado";
				}
			}
			if (cmdGenero.getSelectedIndex() == 2) {
				if (edad < 62) {
					double valor = 62 - edad;
					this.pension = "Su edad es de: " + edad + "\n Tiempo a pensionar: " + valor;
				} else {
					this.pension = "Esta pensionado";
				}
			}

		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(null, "ERROR: Digite una fecha correcta");
		}

		taResultado.setText("Pension: \n" + " Genero: " + genero + "\n" + pension);
	}
}
