
package appgasolina;

public class Surtidores {

    private String nameSurtidor;
    private double precioCorriente;
    private double precioExtra;
    private double precioDisel;

    public Surtidores(String name) {
        this.nameSurtidor = name;
        setPrecioCorriente(8100);
        setPrecioExtra(10000);
        setPrecioDisel(7500);
    }

    public double getPrecioDisel() {
        return precioDisel;
    }

    public void setPrecioDisel(double precioDisel) {
        this.precioDisel = precioDisel;
    }

    public double getPrecioExtra() {
        return precioExtra;
    }

    public void setPrecioExtra(double precioExtra) {
        this.precioExtra = precioExtra;
    }

    public double getPrecioCorriente() {
        return precioCorriente;
    }

    public void setPrecioCorriente(double precioCorriente) {
        this.precioCorriente = precioCorriente;
    }

    public String getNameSurtidor() {
        return nameSurtidor;
    }

    public void setNameSurtidor(String nameSurtidor) {
        this.nameSurtidor = nameSurtidor;
    }

    @Override
    public String toString() {
        return "Surtidores{" + "nameSurtidor= " + nameSurtidor + ", precioCorriente= " + precioCorriente
                + ", precioExtra= " + precioExtra + ", precioDisel= " + precioDisel + '}';
    }

}
