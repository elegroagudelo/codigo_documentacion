
package appgasolina;

public class VentaGasolina extends Surtidores {

    public VentaGasolina(String name, double cantidad, int tipo) {
        super(name);

        if (tipo == 0) {
            this.ventaCorriente = cantidad;
        } else {
            this.ventaCorriente = 0;
        }

        if (tipo == 1) {
            this.ventaExtra = cantidad;
        } else {
            this.ventaExtra = 0;
        }

        if (tipo == 2) {
            this.ventaDisel = cantidad;
        } else {
            this.ventaDisel = 0;
        }

    }

    private double ventaCorriente;
    private double ventaExtra;
    private double ventaDisel;

    public double getVentaCorriente() {
        return ventaCorriente;
    }

    public void setVentaCorriente(double valor) {
        this.ventaCorriente = valor * getPrecioCorriente();
    }

    public double getVentaExtra() {
        return ventaExtra;
    }

    public void setVentaExtra(double valor) {
        this.ventaExtra = valor * getPrecioExtra();
    }

    public double getVentaDisel() {
        return ventaDisel;
    }

    public void setVentaDisel(double valor) {
        this.ventaDisel = valor * getPrecioDisel();
    }

    @Override
    public String toString() {
        return "VentaGasolina{" + "ventaCorriente=" + ventaCorriente + ", ventaExtra=" + ventaExtra + ", ventaDisel="
                + ventaDisel + '}';
    }

}
