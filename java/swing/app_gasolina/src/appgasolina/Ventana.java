
package appgasolina;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

public class Ventana extends JFrame implements ActionListener {

	private JLabel lTitulo, lsurtidor, lcantidad, ltipo;
	private JTextField tfcantidad;
	private JComboBox combtipo, combsurtidor;
	private JTextArea taResultado;
	private JButton bComprar, bConsultar;
	private JScrollPane scroll;
	private VentaGasolina ventas = new VentaGasolina("", 0, 0);
	private ArrayList<VentaGasolina> listaDatos = new ArrayList<VentaGasolina>();

	public Ventana(String title) {
		super(title);
		this.setLayout(null);
		this.setLocation(300, 100);

		lTitulo = new JLabel("CONTROL DE GASOLINA");
		lTitulo.setBounds(100, 10, 200, 20);

		lsurtidor = new JLabel("Selecciona Surtidor:");
		lsurtidor.setBounds(10, 50, 180, 20);

		combsurtidor = new JComboBox();
		combsurtidor.addItem("Suirtidor 1");
		combsurtidor.addItem("Suirtidor 2");
		combsurtidor.addItem("Suirtidor 3");
		combsurtidor.addItem("Suirtidor 4");
		combsurtidor.setBounds(190, 50, 160, 20);

		ltipo = new JLabel("Selecciona Combustible");
		ltipo.setBounds(10, 80, 180, 20);

		combtipo = new JComboBox();
		combtipo.addItem("Corriente");
		combtipo.addItem("Extra");
		combtipo.addItem("Disel");
		combtipo.setBounds(190, 80, 160, 20);

		lcantidad = new JLabel("Cantidad");
		lcantidad.setBounds(100, 110, 160, 20);

		tfcantidad = new JTextField(25);
		tfcantidad.setBounds(190, 110, 100, 20);

		bComprar = new JButton("Ejecuta la Venta");
		bComprar.setBounds(100, 140, 160, 25);
		bComprar.addActionListener(this);

		bConsultar = new JButton("Consultar Ventas");
		bConsultar.setBounds(100, 180, 160, 25);
		bConsultar.addActionListener(this);

		taResultado = new JTextArea();
		scroll = new JScrollPane(taResultado);
		scroll.setBounds(10, 220, 350, 200);

		// ---Agregar
		this.add(scroll);
		this.add(bConsultar);
		this.add(bComprar);
		this.add(tfcantidad);
		this.add(lcantidad);
		this.add(combtipo);
		this.add(ltipo);
		this.add(combsurtidor);
		this.add(lsurtidor);
		this.add(lTitulo);

		this.setSize(400, 500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocation(400, 100);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bComprar) {
			String nombre = "";
			int index = combsurtidor.getSelectedIndex();
			if (index == 0) {
				nombre = "Surtidor 1";
			} else if (index == 1) {
				nombre = "Surtidor 2";
			} else if (index == 2) {
				nombre = "Surtidor 3";
			} else if (index == 3) {
				nombre = "Surtidor 4";
			}

			int Tipo = combtipo.getSelectedIndex();
			ventas = new VentaGasolina(nombre, Double.parseDouble(tfcantidad.getText()), Tipo);
			listaDatos.add(ventas);
			Limpiar();
		}

		if (e.getSource() == bConsultar) {
			for (int i = 0; i < listaDatos.size(); i++) {
				ventas = listaDatos.get(i);
				taResultado.append(ventas.toString() + "\n");
			}
		}
	}

	public void Limpiar() {
		tfcantidad.setText("");
	}
}
