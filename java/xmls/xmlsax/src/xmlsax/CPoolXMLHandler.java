package xmlsax;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
/**
 *
 * @author elegro
 */
public class CPoolXMLHandler extends DefaultHandler{
   
	@Override
   public void startElement(String uri, String localName, String qName, Attributes attributes)
   {
      System.out.println("Comienza tag: "+qName);
      for(int i=0; i < attributes.getLength(); i++)
      {
         System.out.print(""+attributes.getQName(i)+" = ");
         System.out.println(attributes.getValue(i));
      }
   }
   
	@Override
   public void endElement(String uri, String localName, String qName)
   {
      System.out.println("Cierra: "+qName);
   }
}
