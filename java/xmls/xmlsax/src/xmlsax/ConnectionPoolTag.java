package xmlsax;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author elegro
 */
public class ConnectionPoolTag extends DefaultHandler {
	private JDBCTag jdbc;
	private PoolsTag pools;

	private static ConnectionPoolTag instancia = null;

	public JDBCTag getJdbc() {
		return jdbc;
	}

	public void setJdbc(JDBCTag jdbc) {
		this.jdbc = jdbc;
	}

	public PoolsTag getPools() {
		return pools;
	}

	public void setPools(PoolsTag pools) {
		this.pools = pools;
	}

	public static ConnectionPoolTag getInstancia() throws SAXException {
		try {
			if (instancia == null) {
				SAXParserFactory spf = SAXParserFactory.newInstance();
				SAXParser sp = spf.newSAXParser();
				sp.parse("./src/xmls/env.xml", new ConnectionPoolTag());
			}
			return instancia;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException();
		}
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equals("connection-pool")) {
			instancia = new ConnectionPoolTag();
		}
		if (qName.equals("jdbc")) {
			jdbc = new JDBCTag();
			instancia.setJdbc(jdbc);
		}
		if (qName.equals("pools")) {
			pools = new PoolsTag();
			instancia.setPools(pools);
		}
		if (qName.equals("connection")) {
			ConnectionTag c = new ConnectionTag();
			c.setName(attributes.getValue("name"));
			c.setDriver(attributes.getValue("driver"));
			c.setUrl(attributes.getValue("url"));
			c.setUsr(attributes.getValue("usr"));
			c.setPwd(attributes.getValue("pwd"));
			jdbc.addConnectionTag(c);
		}
		if (qName.equals("pool")) {
			int min = Integer.parseInt(attributes.getValue("minsize"));
			int max = Integer.parseInt(attributes.getValue("maxsize"));
			int steep = Integer.parseInt(attributes.getValue("steep"));

			PoolTag c = new PoolTag();
			c.setName(attributes.getValue("name"));
			c.setMinsize(min);
			c.setMaxsize(max);
			c.setSteep(steep);
			pools.addPoolTag(c);
		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
	}

	private ConnectionPoolTag() {
		jdbc = new JDBCTag();
		pools = new PoolsTag();
	}

	@Override
	public String toString() {
		String x = "";
		x += "-- JDBC --\n";
		x += jdbc.toString();
		x += "-- POOLes --\n";
		x += pools.toString();
		return x;
	}

}
