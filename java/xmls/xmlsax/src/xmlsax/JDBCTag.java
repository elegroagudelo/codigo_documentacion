package xmlsax;
import java.util.Enumeration;
import java.util.Hashtable;
/**
 *
 * @author elegro
 */
public class JDBCTag
{
   private Hashtable<String, ConnectionTag> connection;

   public JDBCTag()
   {
      this.connection = new Hashtable<String, ConnectionTag>();
   }
   
   public ConnectionTag getConnectionTag(String name)
   {
      return connection.get(name);
   }
   
   public void addConnectionTag(ConnectionTag c)
   {
      connection.put(c.getName(),c);
   }
   
   @Override
   public String toString()
   {
      String x="";
      ConnectionTag aux;
      Enumeration<String> e = connection.keys();
      while( e.hasMoreElements() )
      {
         aux = connection.get(e.nextElement());
         x+=aux.toString()+"\n";
      }
      return x;
   }

}
