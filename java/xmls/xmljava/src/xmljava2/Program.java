/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmljava2;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
/**
 *
 * @author elegro
 */
public class Program {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args)throws FileNotFoundException 
   {
      // TODO code application logic here
      File xmlFile = new File("./src/xmljava2/sample.xml");
      //Create the parser instance
      UsersXmlParser parser = new UsersXmlParser();
      //Parse the file
      ArrayList<User> users = parser.parseXml(new FileInputStream(xmlFile));
      //Verify the result
      System.out.println(users);
   }
}