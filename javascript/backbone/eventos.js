//Evento On
function ejemplo1()
{
    var objeto= {}; //objeto basio
    _.extend(objeto, Backbone.Events); 
    //intacia al backbonts sobre el objeto con el que se trabaje
    objeto.on("alert", function(msg, el)
    { 
        //metodo on que resive eventos emitidos por si mismo
        alert(msg);
        el.append("<div class='alert alert-danger'>OK</div>"); 
        // una vez ejecuta el alert pasa a agregar el mensage 
    }); 
    //evento asi mismo

    $("#accion1").click(function(event) 
    {
        objeto.trigger("alert","parametros de evento", $(this)); 
        // este trigger me ejecuta el evento anterior alert
        //el mensage OK se ejecuta asi mismo con el This;		
    });		
}

//Eventos Off
function ejemplo2()
{
    var objeto= {};
    _.extend(objeto, Backbone.Events);
    objeto.on("alert", function(msg, el){ 
        el.append("<div class='alert alert-danger'>OK</div>");
        //evento off
        objeto.off("alert"); // con ello se omite el evento alert con sus detalles;
    }); //evento asi mismo

    $("#accion2").click(function(event) {
        objeto.trigger("alert","parametros de evento", $(this)); 		
    });		

}

//Metodo ListenTo y stopListening
function ejemplo3()
{
    var objeto1= {};
    var objeto2= {};
    _.extend(objeto2, Backbone.Events);
    _.extend(objeto1, Backbone.Events); 

    objeto1.on("alert", function(msg){
        alert(msg);			
    }); 	

    $("#accion3").click(function() 
    {
        objeto1.trigger("alert","parametros de evento objeto1 "); 
    });		
    
    objeto2.listenTo(objeto1,"alert", function(msg)
    {
        alert("recibido desde objeto2 "+ msg);
        this.stopListening(objeto1);
    });
}

//Eventos Once
function ejemplo4()
{
    var objeto= {};
    _.extend(objeto, Backbone.Events);
    
    objeto.once("alert", function(msg)
    { 
        alert("resivido solo una vez"+ msg);
    }); 

    $("#accion4").click(function(event) {
            objeto.trigger("alert","parametros de evento", $(this)); 		
    });		
}

//Evento de llamadaa objeto

function ejemplo5()
{
    var objeto= {};
    _.extend(objeto, Backbone.Events);
    
    objeto.on("alert:obj1", function(msg)
    { 
        alert("acceso al evento obj1 "+ msg);
    }); 

    objeto.on("alert:obj2", function(msg)
    { 
        alert("acceso al evento obj2 "+ msg);
    }); 

    $("#accion5").click(function() {
        objeto.trigger("alert:obj1","parametros de evento enlace 1", $(this)); 		
    });		

    $("#accion6").click(function() {
        objeto.trigger("alert:obj2","parametros de evento enlace 2", $(this)); 		
    });
}

function ejemplo6()
{
    var objeto={};
    _.extend(objeto, Backbone.Events);

    objeto.on("all", function(eventName, msg)
    {
        alert(eventName+": "+ msg);
    });

    $("#accion7").click(function() 
    {
        objeto.trigger("alert.obj1","Psando datos por all con el eventName");
    });

}

//Consola en Textarea
function Consola()
{
    window.trace= function(msg)
    {
        $("#Consola").append(msg +"\n");
        console.log(msg);
    }
}

$(document).ready(function($)
{
	Consola();
    ejemplo1();
    ejemplo2();
    ejemplo3();
    ejemplo4();
    ejemplo5();
    ejemplo6();
});