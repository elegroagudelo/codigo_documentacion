var Panel= Backbone.Model.extend(
	{
		//propiedad de acceso o instancia
        nombre: "Original",
        defaults:{
            txt: "Texto de acceso del panel",
            rotulo: "No esta definido"
        }
	},
	{
		//propiedad staticas
        estatica: "Original Estatica"
	}
);  


inicializar();
Consola();


//==========================
//inicialzar
//==========================
function inicializar()
{
	$("#JBoton").click(function() 
	{
        var panel= new Panel();
        window.trace(panel.nombre);
	});


	$("#JBoton2").click(function() 
	{
        var panel1= new Panel();
        var panel2= new Panel({txt:"Este es el nuevo contenido", rotulo:"1000000"});
        //console.debug(panel1);
        //console.debug(panel2);
        window.trace(panel1.defaults.txt);

        panel1.nombre = "Nueva Propiedad Panel";
        Panel.estatica = "Nueva estatica";
        
        window.trace(panel1.nombre);
        window.trace(Panel.estatica);
        window.trace(panel2.nombre);
        
        //rehresar un objeto Json
        window.trace(panel1.toJSON());
        //convertir el objeto a texto se hace=
        window.trace(JSON.stringify(panel1.toJSON()));

	});
}


//==========================
//Consola en Textarea
//==========================
function Consola()
{
    window.trace = function(msg)
    {
        $("#Consola").append(msg +"\n");
        console.log(msg);
    }
}