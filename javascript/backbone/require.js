define(['jquery', 'backbone', 'marionette'], 
function($, Backbone, Mn){

	var BookStoreApp = new Mn.Application();
	var BookStoreControlller = {
		desplayBooks: function(){
			console.log("Ver libros");
		}
	};

	var BooksStoreRouter = Mn.AppRouter.extend({
		controller: BookStoreControlller,
		appRouter:{
			"": "desplayBooks"
		}
	});

	var router = new BooksStoreRouter();
	router.appRoute("", "desplayBooks");

	BookStoreApp.on('start', function(){
		if(!Backbone.history.start()){
			Backbone.history.navigate("");
		}
	});
	
});
