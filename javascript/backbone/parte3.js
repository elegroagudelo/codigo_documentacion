var Modelo = Backbone.Model.extend(
	{
		//propiedad de acceso o instancia
			nombre : "Nombre es Edwin propiedad de acceso del modelo",
			
			defaults : {
				id: "1",
				txt : "Texto de acceso del panel",
				rotulo : "No esta definido"
			}
	},
	{
		//propiedad staticas
			estatica: "Original Estatica"
	}
);


var Contenido =  Backbone.View.extend({
	initialize:function()
	{
		this.render();	
	},
	render:function(eventName){
		$(this.$el).append('<div>Hola mundo'+ this.model.nombre.toString() +'</div>');
		return this;
	}
});

//con platillas
var Contenido2 =  Backbone.View.extend({
	
	template: _.template($('#panel_contenido').html()),

	initialize:function()
	{
		this.render();	
	},
	render:function(eventName){
		$(this.$el).append( this.template(this.model.toJSON()));
		return this;
	}
});

var Paquete = Backbone.Collection.extend({
    model: Modelo,
 });
 

//==========================
//inicialzar
//==========================
function limpiar() {
	$('#Consola').html('');
}

function eventollamado()
{
	$("#JBoton").click(function() 
	{
			var mod = new Modelo();
			window.trace(mod.nombre);
	});


	$("#JBoton2").click(function() 
	{
			//Crear dos objetos que accedan al modelo
			var panel1 = new Modelo();
			var panel2 = new Modelo({ txt : "Este es el nuevo contenido", rotulo : "1´000.000"});

			window.trace(panel1.defaults.txt);

			panel1.nombre = "Nueva Propiedad Panel";

			//se accede a las estaticas directamente al modelo
			Modelo.estatica = "Nueva estatica";
			
			window.trace(panel1.nombre);
			window.trace(Modelo.estatica);
			window.trace(panel2.nombre);
			
			//hacer un objeto Json
			window.trace(panel1.toJSON());
			window.trace(JSON.stringify(panel2.toJSON()));

			//convertir el objeto a texto se hace=
			window.trace(JSON.stringify(panel1.toJSON()));

	});


    //Vistas en Backbone
	$("#nuevavista").click(function() 
	{
			var datos = new Modelo({ nombre:'Edwin Andres'});
			var contenido = new Contenido({
				el: $('#container'), 
				model:datos
			});   
	});
}



//Consola en Textarea
function Consola()
{
    window.trace= function(msg)
    {
        $("#Consola").append(msg +"\n");
        console.log(msg);
    }
}



$(document).ready(function($)
{
	eventollamado();
    Consola();
    $('#limpiar').click(limpiar);

    $('#conPlantilla').click(function() 
    {
            var datos = new Modelo({ nombre:'Edwin', txt:'Nuevo texto lanzado', rotulo:'Este es el nuevo rotulo'});
            var contenido = new Contenido2({
                el: $('#container'),
                model:datos
            });
    });

});