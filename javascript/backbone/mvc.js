/* Modelos Backbone */
var Libro = Backbone.Model.extend({
	validate: function(attributos){
		if (! attributos.titulo) {
			alert("debe tener  titulo");
		}
	},
	defaults:{
		autor: '',
		titulo: '',
		categoria: ''
	}
});


/* Collections Backbone*/
var Coleccion = Backbone.Collection.extend({
	model: Libro
});

/* Vistas Backbone */

var Librerias = Backbone.View.extend({
	tagName : 'input',
	className : 'form-control',
	id: 'ingreso',
	el: '.vista',
	initialize: () => {
		this.render();
	}, 
	render: () => {
		this.$el.html('<p>Hola render</p>');
		return this;
	},
	events: {
	 	'click .cambiarcolor': 'cambiarcolor'
	},
	cambiarcolor: function(){
		this.$el.css('color','red');
	}
});