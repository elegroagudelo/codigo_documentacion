var expression = new RegExp('abs');

var expression = /abc/; /* expresion literal */

var parrafo = /text\/ntexto2/; /* para caso salto de linea */

/* Para validar una expresion en javascript se usa el patron.text(string)*/
var cadena = "aqui hay un acadena"; 
var parrafo = /^\D/i.text(cadena);  /* como resultado retorna true o falso */

var express = /[012345]/  /* valida si se encuentra alguno de los numeros */

/* Usar rangos */
var express = /[0-9]/ /* si se encuentrane rango de numeros */
var express = /[a-z]/
var express = /[0-9a-zA-Z]/ /* valida que los parametros sean numeros y letras */

/* Agrupar cadenas */
/*
\d cualquier numero
\w cualquier letra o numero
\s cualquer espacio en blanco 
\D cualquier caracter que no sea un número
\W cualquier caracter que no sea alfanumerico
\S cualquier caracter que no se un espacio en blanco
. cualquer caracter excepto nuevas lineas
*/

/* Validar le estructura de una fecha */
var esfecha = /\d\d-\d\d-\d\d\d\d/;  /* dia-mes-año*/

if (esfecha.test('12-05-2023')) "OK es valido el formato de fecha"


// Si algun numero se encuentra
var haynumero = /[\d]/;

var express = /[01]/ /* Si posee el 0 y el 1 */

// Invertir la validacion
var express = /[^01]/ /* si no posee el 0 y el 1 en la cadena */


//Iteraciones de patrones
// +
var express = /\d+/; /* Si posee al menos una coincidencia */


//Patrones opcionales
var express = /Hola mun?do/i;   /* La (n) es opcional*/
var express = /\W?(png)$/i; /* Que poseea (.) en la parte final de la cadena*/


//Validacion con atributos
var express = /\d{1,2}-\d{1,2}-\d{4}/;   /* valida el formato de la fecha */
var express = /\d{1,2}\W{1}\d{1,2}\W{1}\d{4}\s{1,2}\d{1,2}\W{1}\d{1,2}\W{1}\d{1,2}/  /* validacion de formato de fecha y hora más completo */


// No distingir entre mayusculas y minuscula (i)
var express = /\D/i;

//Compuesta
/* Hace la union de dos patrones */
var express = /woo+(hoot+)/i;


//Inicio y Fin
var express = /^algun/i; /* valida desde el inicio */
var express = /algun$/i; /* valida desde el Fin */

//Condicion OR (|)
var express = /^any|patron$/;

//Limite de validacion
var express = /\bcat\b/  /* solo el texto pedido */


//Otros metodos de javascript
patron.exec("cadena") /* extrae los datos y el indice donde está el texto buscado*/

cadena.match(/\d+/) /* Retorna un arreglo con el resultado que evalua */

