
<!DOCTYPE html>
<html>
<head>
	<title>Ejemplo II con Jquery</title>
	<link rel="stylesheet" type="text/css" href="css/eti.css">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">

	$(document).ready(function(){ /*llamada a jquery  cuando el documento este listo se realice la siguiente funcion */
		$("#miboton").click(function(){
			$(".mitexto").append(" texto nuevo"); /*texto nuevo en frente */
			$(".mitexto").before(" texto nuevo"); /*agrega texto en un parrafo anterior*/
		}); 
		$("#miboton2").click(function(){
			$(".mitexto").prepend(" texto nuevo"); /*texto nuevo detras*/
			$(".mitexto").after(" texto nuevo"); /*texto en el sigiente parrafo*/

		});

		function cambio(){ /*control del select*/
			var mivalor = $("#miselet").val();
			$("p").html(" resultado "+ mivalor);
		}
		$("select").change(cambio);

	});

</script>
</head>
<body>

<div>
	<input type="button" value="Enter-1" id="miboton" />
		<input type="button" value="Enter-2" id="miboton2" />
			<input type="button" value="Enter-3" id="miboton3" />
</div>
<p class="mitexto"> este es mi texto para cambiar por medio de jquery</p>
<select id="miselet">
	<option>cambio 1</option>
	<option>cambio 2</option>
	<option>cambio 3</option>
</select>

<p>
	estes es el seleccionado
</p>
</body>
</html>
