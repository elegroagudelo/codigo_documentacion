<?php

?>
<!DOCTYPE html>
<html>
<head>
	<title>Ejemplo 4 de jquery</title>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(document).mousemove(function(e){  /*cuando el mouse este moviendose sobre el documento realice la funcion (e)*/
			$(".x").text("Cursor en x" +e.pageX);
			$(".y").text("Cursor en y" +e.pageY);
		});	

		$(".mih").mouseup(function(){ /*al momentode dar click se coloca rojo*/
			$(this).css("background-color","red");
		});
		$(".mih").mousedown(function(){
			$(this).css("background-color","blue"); /*click sostenido cambia de colora azul */
		});

		$(".mih").dblclick(function(){
			console.log("le dieron doble click");
				alert("Esta funcionando");
		});

		/*usando focur y blur en input text  --similar al efecto de placeholder de html*/
		$("#miuser").focus(function(){
			$(this).attr("value","");

		});
		$("#miuser").blur(function(){
			$(this).attr("value","usuario");
		});
	});

	</script>
</head>
<body>
<h1 class="x">Hola</h1><br/>
<h1 class="y">Hola</h1>

<h2 class="mih">nuevo con up </h2>
<h2 class="mih">nuevo con up</h2>
<br/>
<input type="text" value="usuario" id="miuser" />
</body>
</html>
