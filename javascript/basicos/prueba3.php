<?php

?>
<!DOCTYPE html>
<html>
<head>
	<title> Ejercio 3 con Jquery</title>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#mip").mouseover(esconder);
			$("#mip").mouseout(mostrar);
			$(".x").hover(esconder,mostrar); /*con hover se optien el mismo resultado que con mouseover y mouseout*/

			function esconder(){ /* esta es la funcion que necesita el mouse esconder*/
				$(this).css("background-color","yellow"); /*this o este- refernte al elemento que ese este utlizando en la llamada a la funcion-*/

			}
			function mostrar(){ /* esta es la funcion que necesita el mouse mostrar*/
				$(this).css("background-color","white");
			}


		});

	</script>
</head>
<body>
	<h1 id="mip">
	nuevo parrafo para el ejercicio
	</h1>
	<h1 class="x">
	nuevo parrafo para el ejercicio
	</h1>
	<h1 class="x">
	nuevo parrafo para el ejercicio
	</h1>


</body>
</html>
