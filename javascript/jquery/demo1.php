<?php
include_once("inc/head.php");
?>
<script type="text/javascript" src="inc/js/jquery.js"></script>
<link rel="stylesheet" href="inc/css/estilos.css" />

<section class="container">
    <div class="row">
       <div class="col-xs-12">
        <h3 class="text-center">Formulario con ajax</h3>
        <form method="post" class="contacto">
        <fieldset>
            <div><label>Nombre:</label><input type="text" class="nombre" name="nombre" /></div>
            <div><label>Email:</label><input type="text" class="email" name="email" /></div>
            <div><label>Telefono:</label><input type="text" class="telefono" name="email" /></div>
            <div><label>Mensaje:</label><textarea cols="30" rows="5" class="mensaje" name="mensaje" ></textarea></div>
            <div class="ultimo">
                <img src="inc/css/img/ajax.gif" class="ajaxgif hide" />
                <div class="msg"></div>
                <button id="boton_envio">Enviar Mensaje</button>
            </div>
        </fieldset>
     </form>
   </div>
  </div>
 </section>
 
 <script type="text/javascript">
 $(function(){
    $("#boton_envio").click(function() {
 
        var nombre = $(".nombre").val();
        var email = $(".email").val();
        var telefono = $(".telefono").val();
        var mensaje = $(".mensaje").val();
            
            validacion_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
 
        if (nombre == "") {
            $(".nombre").focus();
            return false;
        }else if(email == "" || ! validacion_email.test(email)){
            $(".email").focus();    
            return false;
        }else if(telefono == ""){
            $(".telefono").focus();
            return false;
        }else if(mensaje == ""){
            $(".mensaje").focus();
            return false;
        }else{

            $('.ajaxgif').removeClass('hide');

            var datos = 'nombre='+ nombre + '&email=' + email + '&telefono=' + telefono + '&mensaje=' + mensaje;
            $.ajax({
                type: "POST",
                url: "lib/proceso.php",
                data: datos,
                success: function() {
                    $('.ajaxgif').hide();
                    $('.msg').text('Mensaje enviado!').addClass('msg_ok').animate({ 'right' : '130px' }, 300);  
                },
                error: function() {
                    $('.ajaxgif').hide();
                    $('.msg').text('Hubo un error!').addClass('msg_error').animate({ 'right' : '130px' }, 300);                 
                }
            });
          return false;
        }
    });
});
 </script>