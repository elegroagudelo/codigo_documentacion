<?php
include_once("inc/head.php");
?>
<section class="container">
	<div class="row" id="workshoup-row">
		<div class="col-xs-12" id="workshoup-contenido">
				<h3>Bienvenidos PHP JSON</h3>
				<br>
				<span id="datos"></span>
				<h4>Pasar datos de Json a un array de php</h4>

<?  $varios = '{
		"nombres": "edwin legro",
		"identidad": 1110491951,
		"Logeado":false,
		"Idcursos":[23,24,25],
		"cursos": { "sistemas":"desarrollo de software","Diseño": "manejo de Corel Draw"},
		"pendiente":null 
	}';
				//el Json lo convertimos en un array
				print_r(json_decode($varios, true));
?>
<h4>Pasar datos de php a un array Json</h4>

<?
//JSON en Php 
 $variosObjetos = array(
 		"nombres"=> "edwin legro",
		"identidad" => 1110491951,
		"Logeado" => false,
		"Idcursos" =>array(23,24,25),
		"cursos" => array(   //anidar objetos
				"sistemas"=>"desarrollo de software",
				"Diseño"=> "manejo de Corel Draw"
			),
		"pendiente"=>null 
  );

	echo json_encode($variosObjetos);
 ?>
 			</div>
		</div>
	</section>