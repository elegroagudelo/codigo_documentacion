<?php
include_once("inc/head.php");
?>
<script type="text/javascript">
	function enviardatos(){
		var conexion; 
 		var url= "lib/nuevousuario.php";

		if (window.XMLHttpRequest){ //comprobamos  si  hay una conexion con XMLHttpRequest 
			conexion = new XMLHttpRequest(); // version moderna de navegador
		}else{
			conexion= new ActiveXObjet("Microsoft.XMLHTTP"); // version anteriores del navegador
		}

		var id = document.getElementById("id").value;			
		var user = document.getElementById("user").value;			
		var pass = document.getElementById("pass").value;			
		var rol = document.getElementById("rol").value;			
		var datos ="id="+id+"&user="+user+"&pass="+pass+"&rol="+rol;

		conexion.open("POST",url, true); 
		conexion.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 

		conexion.onreadystatechange= function(){ 
			if (conexion.readyState==4 && conexion.status == 200) { 
			document.getElementById("result").innerHTML=conexion.responseText;	
			}	
		}
		conexion.send(datos);
		document.getElementById("result").innerHTML="Procesando...";
		document.getElementById("id").value="";			
		document.getElementById("user").value="";			
		document.getElementById("pass").value="";			
		document.getElementById("rol").value="";	
	}
</script>

<div id="workshoup-list">
	<section class="well container">
		<div class="row">
			<div class="col-xs-12">
		
		<h3>Bienvenidos</h3>
		<p>En codeigniter: Se requiere un formulario de inserción de datos para 
		la tabla usuarios. El formulario debe ser enviado a través del metodo AJAX</p>
<br>
<div class=" col-xs-6">
	<form method="Post" >
    <div class="form-group"  >
    <p>Identificacion</p>
    <p><input class="form-control" type="text" id="id" name="id"/></p>
    <p>Usuario</p>
    <p><input class="form-control" type="text" id="user" name="user"/></p>
    <p>Rol</p>
    <p><input  class="form-control" type="text" id="rol" name="rol"/></p>
    <p>Password</p>
    <p><input  class="form-control" type="text" id="pass" name="pass"/></p>
    <input  class="btn btn-primary" type="button" value="Enviar" id="cargardatos" onclick="enviardatos()" />
    </div>
	</form>
		<p><span id="result"></span></p>
</div>
<div class="col-xs-6">
	<div class="form-group">
	<h3>Consulta</h3>
 	</div>
 	<div id="consulta"></div>
</div>
			
			</div>
		</div>
	</section>
</div>

<script>
	$(document).ready(function(){
				var urlget= 'lib/nuevousuario.php';
				
				$.get(urlget,"", function(data){
				var json= JSON.parse(data);
				//console.log(json);
				//Recorrer los objetos Json
				var listado= "<table class='table table-bordered'><th>Identidad</th><th>Usuario</th><th>Rol</th><th>Password</th>"; // listado para mostrar 
			for (post in json) { 
				//console.log(json[post]); //separar cada post en consola
			listado +="<tr><td>"+ json[post].idusuario+ "</td>";
			listado +="<td>"+ json[post].nombre+ "</td>";
			listado +="<td>"+ json[post].rol+ "</td>";
			listado +="<td>"+ json[post].clave+ "</td></tr>";
			};
			listado +="</table>";
			$("#consulta").html(listado);
		});
	});
</script>
