<?php
include_once("inc/head.php");
?>
<section class="container">
	<div class="row">
		<div class="col-xs-12">
			<form id="form">
				<h3>Plugin Autocomplete do jQuery UI trabalhando em Conjunto com a Interface Ajax do jQuery</h3>
				<fieldset>
				<input type="text" id="campo-busca" placeholder="Buscar" name="pais" />
				<input type="submit" id="submit-busca" value="Buscar" />
				</fieldset>
			</form>
		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#campo-busca').autocomplete({
		minLength: 1,
		autoFocus: true,
		delay: 300,
 		position: { collision: "flip"},
		appendTo: '#form',
		source: function(request, response){
			$.ajax({
			url: 'lib/busca.php',
			type: 'get',
			dataType: 'html',
			data: { 'termo': request.term }
			}).done(function(data){
				
				if(data.length > 0){
					data = data.split(',');
					response($.each(data, function(key, item){
					return({ label: item });
		
					}));
				}
			});
		}
	});
});
</script>
