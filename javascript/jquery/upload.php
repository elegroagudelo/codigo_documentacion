<?php
include_once("inc/head.php");
?>
<link rel="stylesheet" type="text/css" href="inc/css/green.css">

	<section class="container">
		<div class="row">
			<div class="col-xs-12" id="workshoup-contenido">

			<div class="main">
  			<h1>Ajax Image Upload</h1><br/><hr>
				<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
	  				<div id="image_preview"><img id="previewing" src="noimage.png" /></div><hr id="line">
	  			<div id="selectImage">
	     			<label>Select Your Image</label><br/>
	     			<input type="file" name="file" id="file" required />
	     			<input type="submit" value="Upload" class="submit" />
	   			</div>
				</form>
			</div>
				<h4 id='loading' >loading..</h4>
				<div id="message"></div>
    	</div>
   	</div>
   	<div class="row">
   	</div>
  </section>


<script type="text/javascript">
$(document).ready(function(event) {

	$("#uploadimage").on('submit',(function(event) {
	event.preventDefault();

	$("#message").empty();
	$('#loading').show();

	$.ajax({
		url: "lib/admin.php", // Url to which the request is send
		type: "POST",             // Type of request to be send, called as method
		data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data){
				$('#loading').hide();
				$("#message").html(data);
			}
	});
}));


// Function to preview image after validation
$(function(){
	$("#file").change(function() {
	$("#message").empty(); // To remove the previous error message
	var file = this.files[0];
	var imagefile = file.type;
	var match= ["image/jpeg","image/png","image/jpg"];

	if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
		$('#previewing').attr('src','noimage.png');
		$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		
		}else{
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		}
	});
});

	
	function imageIsLoaded(event) {
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', event.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};
	
});
</script>
