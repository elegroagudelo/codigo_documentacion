<?php
include_once("inc/head.php");
?>
<section class="container">
	<div class="row">
		<div class="col-xs-12" id="workshoup-contenido">
					<h3>Bienvenidos Metodo 2 manejo GET form</h3>
		<div class="col-xs-4">
			<div class="form-group">
				<form  method="post" class='form-signin' role='form'>
					<label>Identificacion</label><br>
					<input type="text"  id="ide" name="ide" class="form-control">
					<label>Nombre</label><br>	
					<input type="text"  id="nom" name="nom" class="form-control">
					<label>Apellido</label><br>	
					<input type="text"  id="ape" name="ape" class="form-control"><br>
					<label>Email</label><br>	
					<input type="text"  id="ema" name="ema" class="form-control"><br>	
					<label>Rol</label><br>	
					<input type="text"  id="rol" name="rol" class="form-control"><br>	
					<label>Password</label><br>	
					<input type="text"  id="pass" name="pass" class="form-control"><br>					
					<button type='button' id='enviar' class='btn btn-primary btn-small'>Enviar Datos </button>
				</form>				
				<div class="form-group"><p id="resultado"></p></div>
			</div>
		 </div>
		</div>
	</div>
</section>

<script>
	var x;
	x=$(document);
	x.ready(inicia);

	function inicia(){
	var x;
	x= $("#enviar");
	x.click(enviardatos);
	}

	function enviardatos(){
	var ide= document.getElementById("ide").value;			
	var nom= document.getElementById("nom").value;			
	var pass= document.getElementById("pass").value;			
	var rol= document.getElementById("rol").value;	
	var ape= document.getElementById("ape").value;
	var ema= document.getElementById("ema").value;
	$.ajax({
		async:true,
		type:"POST",
		dataType:"html",
		contentType: "application/x-www-form-urlencoded",
		url:'<?=base_url()?>index.php/control/getData',
		data: "nom="+nom+"&ape="+ape+"&ide="+ide+"&ema="+ema+"&rol="+rol+"&pass="+pass,
		beforeSend:iniciaenvio,
		success: procesa,
		timeout: 6000,
		error: problemas
	}); 
	return false;
	}

	function iniciaenvio(){
		var x= $("#resultado");
		x.html("cargando...");
	}

	function procesa(datos){
		$('#resultado').text(datos);
	}

	function problemas(){
		$('#resultado').text('Problemas al enviar datos al servidor');
	}

</script>
