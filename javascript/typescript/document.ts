/* Definimos una funcion y pasamos datos dentro del html*/
function holaMundo(nombre){
	return "Hola mundo " + nombre;
}

var nombre = "edwin andres legro";
document.getElementById("main").innerHTML = holaMundo(nombre);

//en caso de que el js este en el head y no ha cargado toda la pagina
let dom_etiqueta = <HTMLElement>document.getElementById('main');
dom_etiqueta.innerHTML =  holaMundo(nombre);
