
// Union de tipos
type SumaParams = string | number;	 //union de tipos
type ResultParams = string | number;	 //union de tipos

function suma(num1: SumaParams, num2: SumaParams): ResultParams{
	return Number(num1) + Number(num2);
}

// Usando las interfaces, podemos hacer union de estas
interface Interface1 {
    prop1: number;
}

interface Interface2 {
    prop2: number;
}

type InterfaceUnion = Interface1 | Interface2;

let inte: InterfaceUnion = {
    prop1: 1,
    prop2: 2
};
console.log(inte);


//Intersection types y su diferencia de Union types.
interface Interface3 {
    prop1: number;
}
interface Interface4 {
    prop2: number;
    prop3: number;  //Si se incluyera ésta propiedad, nuevamente daría error.
}
type InterfaceMix = Interface3 & Interface4;

const interfaceMix: InterfaceMix = { 
    prop1: 2,
    prop2: 2,
    prop3: 2,
};
//Se requiere denifir todos los valores de la interseccion.


// La principal diferente entre la union y la intersección es que la unión es un valor "o" el otro, en cambio
// la intersección nos indica que un valor no puede estar indistinto del otro.
// Se puede utilizar en cualquier cantidad de interfaces, agregando si se desea una Interface3 para ver que si
// procede el código.
// Lo importante es que TypeScript nos indica cuando se da éste tipo de error, y así poder corregirlo y llevar
// una mejor gestión del código.