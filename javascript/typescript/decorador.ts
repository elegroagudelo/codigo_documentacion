/* Permite agregar una funcionalidad a una clase */

function arranque(lanzar: string){
    return function(target: Function) {
        target.prototype.lanzamiento = function():void {
            alert(lanzar)
        }
    }
}

@arranque('Procesando el decorador para una clase')
class ToCoche {

	public color:string;
	public modelo:string;

	public getColor(): string{
	 	return this.color;
	}

    public getModelo():string{
        return this.modelo;
    }
	
}

var code = new ToCoche();
console.log(code);



