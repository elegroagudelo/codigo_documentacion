/* la interface define las propiedades y metodos que debe poseer una clase */
class  MyCoche {
    
	public color:string;
	public modelo:string;
	
    constructor(){
	}
}

class CocheParticular extends MyCoche
{
	constructor(){
		super();
		this.modelo = "0";
		this.color = "gray";
	}

	public getColor(): string{
	 	return this.color;
	}

    public getModelo():string{
        return this.modelo;
    }
	
}