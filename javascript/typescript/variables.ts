/* Variables de tipo dinamico y estatico*/
var mi_name:string = "nombre usuario";
var mi_phone:number = 315714343;
var isValido:boolean = true;

var langs:Array<string> = ["PHP", "JS", "RUBY", "JAVA"];
var datos:Array<any> = [1.2, 45324, "Texto", true];

/* una variable con cualquier valor */
var cualquiera:any = 10000.44;

//el scope, alcances de las variables
var mi_addres = 1243;

/* alcance a nivel de bloque usando let */
let l1 = "variable de bloque fuera 4";
var l2 = "variable global fuera 8";

if (mi_addres === 1243)
{
	let l1 = "variable de bloque dentro 3";
	var l2 = "variable global dentro 2";
	console.log(l1);
	console.log(l2);
}
//El ambito de let es dentro del bloque de codigo
console.log(l1);
console.log(l2);