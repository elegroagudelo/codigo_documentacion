/* funciones de Tipo flecha
Forma más rapida y limpia para llamar las funciones */
setInterval((nombre_variable) => {
	console.log(nombre_variable);
},1000);

var frutas = [
	"Manzanas",
	"Uvas",
	"Fresas",
	"Bananas"
];

var recorrer = frutas.map(fruta => {
	console.log(fruta);
	console.log(fruta.length);
});
