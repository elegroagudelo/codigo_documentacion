class Transporte    {
    private velocidad: number;
    private formaDeMovilidad: string;
    
    constructor(velocidad:number, forma: string) {
        this.velocidad = velocidad;
        this.formaDeMovilidad = forma;
    }

    public getVelocidad(): number{
        return this.velocidad;
    }

    public setVelocidad(velocidad: number){
        this.velocidad = velocidad;
    }

    public getFormaDeMovilidad(): string {
        return this.formaDeMovilidad;
    }

    public setFormaDeMovilidad(forma:string){
        this.formaDeMovilidad = forma;
    }
}

class Auto extends Transporte{
    
    private puertas: number;
    private marca: string;

    constructor(velocidad:number, forma: string, marca: string, puertas: number){
        super(velocidad, forma);
        this.puertas = puertas;
        this.marca = marca;
    }

    public getPuertas(): number{
        return this.puertas;
    }

    public setPuertas(puertas: number){
        this.puertas = puertas;
    }

    public TopeVelocidad():number {
        return super.getVelocidad();
    }

    public getMarca(): string{
        return this.marca;
    }

    public setMarca(marca: string) {
        this.marca = marca;
    }

}

let automovil: Auto = new Auto(150, "Suelo", "BMW", 4);
console.log(automovil); 

let transporte: Transporte = new Transporte(120, 'Suelo');
console.log(transporte); 

// La SHAPE
/* Es la capacidad de poder represnetar una clase hijo para que pueda ser igual a su clase padre, 
más no puede una clase padre ser igual a la clase hija */

//automovil = transporte; //error generado gracias al shape de typescript
transporte = automovil;
console.log(transporte);