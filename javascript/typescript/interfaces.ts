/* la interface define las propiedades y metodos que debe poseer una clase */
interface  CocheBase {
    getColor(): string;
    getModelo(): string;
}

class lCoche implements CocheBase
{
	public color:string;
	public modelo:string;

	constructor(){
		this.modelo = "0";
		this.color = "gray";
	}

	public getColor(): string{
	 	return this.color;
	}

    public getModelo():string{
        return this.modelo;
    }
	
}


type Dni = string; 

// Interfaces para objetos json
interface Persona {
	estatura ?: number; 	//(?:) indica que es un atributo opcional
	edad: number;
	nombres: string;
	apellidos: string;
	dni: Dni
}

//Objeto creado por medio de la interface
let personita: Persona = {
	estatura: 1.75,
	edad: 31,
	nombres: "Edwin Andres",
	apellidos: "Legro Agudelo",
	dni: "Hola"
};

console.log(personita);