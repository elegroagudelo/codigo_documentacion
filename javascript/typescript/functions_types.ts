
//Definicion de funciones

type CallBackError = Error | null;
type CallBack = (error: CallBackError, response:Object) => void;
type SendRequest = (cb: CallBack) => void;

const send_request: SendRequest = (cb: CallBack): void => {
    if(cb){
        cb(null, {
            message: "Ok la funcion"
        });
    }
};