/*
instalar typescript con
$~ sudo npm install -g typescript
$~ tsc -v 
*/

/* Compilar archivo typescript */
/* para ello se requiere de tsc compiler 
$~ tsc -w *.ts  
*/


//metodos con parametro definido
function mi_numero(a:number, b:number){
	return a + b;
}
console.log(mi_numero(10, 15));



//para definir lo que va a retornar
function mi_numero2(a:number, b:number):string{
	return " "+ a + b;
}
console.log(mi_numero2(10, 15));

//podemos ver errores de tipado
let a = 'Hola';
a = "Hola Aquí";
//a = 2;  //genera error

let b: number = 2;
b = 4;
//b = a;	//genera error

//TYPE KEYWORD
//Definir un tipo de datos
//permite hacer cambios globales en el sistema, con la connnotacion dni
type dni = string;	//el tipo dni es un string

let valor: dni = "HOlA nuevo tipo de datos"; 
