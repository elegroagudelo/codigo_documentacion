
//Decorador implementado a metodos
function Log(target, key){
    console.log(key+ ' se ha llamdo');
}

class Personas {
    private nombre: string;

    constructor(nombre: string){
        this.nombre = nombre;
    }
    
    @Log
    sayMyName(){
        console.log(this.nombre);
    }
}

const _person = new Personas("Edwin");
_person.sayMyName();    //salida es, 'Edwin' y luego 'sayMyName se ha llamdo'