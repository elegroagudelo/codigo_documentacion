
function logParametro(target, propertyName, index){
    let metadataKey = `log_${propertyName}_parametros`;
    if (Array.isArray(target[metadataKey])){
        target[metadataKey].push(index);
    } else{
        target[metadataKey] = [index];
    }
    console.log(target);
}

class Personate {
    
    public sepName(@logParametro nombre: string): string {
        return nombre;
    }
}

const person = new Personate();
let nombrete: any = person.sepName("Alan");
console.log(nombrete);