/* Programacion orientada a objetos POO,
un paradigma de la programacion, que permite consevir todos las coasas como objetos,
*/

class Coche 
{
	public color:string;
	public modelo:string;
	public velocidad:number = 0;
	protected placa:string;
	private propietario:string;
	private valor:string;

	constructor(){
		this.modelo = "0";
		this.color = "gray";
	}

	public getColor(){
	 	return this.color;
	}
	public setColor(color:string){
	 	this.color = color;
	}

	//funcion normal
	public acelerar(){
		this.velocidad++;
		return this.velocidad;
	}

	//funcion que retorna un valor numerico
	public frenar():number{
		this.velocidad--;
		return this.velocidad;
	}

	protected parqueo(): boolean {
		return false;
	}

	private cambiar_placa(): string{
		return "nueva_placa";
	}  
}

//crear el objeto
var coche = new Coche();
coche.setColor("rojo");
console.log(coche.getColor());