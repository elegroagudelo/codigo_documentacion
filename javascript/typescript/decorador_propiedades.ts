function logProperty(target, key){
    let valor = this['key'];
    const getter = () => {
        console.log('Se optiene Get '+ key+ ' valor: '+ valor);
        return valor;
    };
    const setter = (nuevoValor)=> {
        valor = nuevoValor;
        console.log('Se cambia Set '+ key+ ' valor: '+ valor);
        return valor;
    }
    const object_property = {
        get: getter,
        set: setter
    };
    Object.defineProperty(target, key, object_property);
}

class Personaje {
    
    @logProperty
    public name: string;

    constructor(name: string){
        this.name = name;
    }
}

let p = new Personaje('Edwin');
p.name = 'App';
const nameformClass = p.name;
console.log(nameformClass);
