
// Code Runner, extencion de visual code para procesar un script javascript
//Closures y Scope de las variables
// las declaradas con var se pueden volver a declarar con var a diferencia que hacerlo con let, que solo se puede hacer una vez


var saludo = 'variable global ';
let world = 'variable local let';
const hellowold = 'constante aquí'

console.log(hellowold);
console.log(world);
console.log(saludo);

var frutas = () => {
    console.log(hellowold);
    console.log(world);
    console.log(saludo);
};

frutas();
console.log("--------------------------------");
//let world = 'variable redefinida 2'; #genera un error de syntaxError


const holaMundo = () => {
    globarVar =  "mi global";  //mala practica ya que se puede acceder desde afuera
};

holaMundo();
console.log(globarVar)

const otraFunction = ()=>{
    var localVar =  globalVar = 'Globar var';
    var otra = "Hola otra";
};

otraFunction();
//console.log(otra); //tampoco se puede acceder
//console.log(localVar);  //donde la variable no se logra definir 
console.log(globalVar); //es una mala practica en js
console.log("--------------------------------");


// Scope local
const functionScopeLocal = ()=>{
    const localVar =  'Globar var';
};

functionScopeLocal();
// console.log(localVar);   //genera un error porque pertenece al scope local de la funcion
console.log("--------------------------------");

//Ambito lexico del scope
var scope = "scope global aqui";

const functionScopeLocal2 = ()=> {
    var scope = "scope local aqui";     //la variable scope no es reasignada por la funcion
    const func = () => {
        return scope;
    }
    return func();
};

salida = functionScopeLocal2();

console.log(salida); 
console.log(scope); 
console.log("--------------------------------");

//Scope local en funciones
const functionScopeLocal3 = ()=>{
    var localVar =  'local var 1';
    var localVar =  'local var 2';
    let valorn = 'local let 1';
    //let valorn = 'local let 2';   //se genera un error porque let no permite redefinir una variable dentro de su mismo ambito
};

functionScopeLocal3();
console.log("--------------------------------");

//Scope local en bloques
const fruits2 = () => {
    if(true){
        var fruta1 = "limon";
        const fruta2 = "banana"; 
        let fruta3 = "mango";
    }
    console.log(fruta1);
    //console.log(fruta2); //no es posible de acceder por ambito de bloque
    //console.log(fruta3); //no es posible de acceder por ambito de bloque
};

fruits2();
console.log("--------------------------------");


//SCOPE BLOQUE

let x = 10;
{
    //console.log(x); //genera un error, ya que la variable en el bloque no se definido
    let x = 20;
    console.log(x);  //el scope no se puede redefinir asi que crea una nueva variable
}
console.log(x);  //aqui se usa el scope global que se declaro inicialmente
console.log("--------------------------------");


const algunaFunctionTime = () => 
{
    //se genera un error de logica al usar var para definir la variable a mostrar por consola
    for (var i = 0; i <= 10; i++) {
        const element = i;
        setTimeout(() => {
            console.log(element);
        }, 1000);        
    }

    for (let i = 0; i <= 10; i++) {
        const element = i;
        setTimeout(() => {
            console.log(element);
        }, 1000);        
    }
};

algunaFunctionTime();