/* Archivo principal de electronjs */
'use strict'

const { app, BrowserWindow } = require('electron')

//mostrar todos los eventos del objeto app electron
console.dir(app);

app.on('before-quit', () => {
    console.log('Saliendo');
})

//al iniciar el aplicativo
app.on('ready', () => {
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        title:  "Ventana de prueba",
        center: true,
        maximizable: false,
        show:false
    });

    //mostrar una vez cargue la información
    win.once('ready-to-show', () => {
        win.show();
    });

    //cargar una url externa
    //win.loadURL("https://127.0.0.1:4001/Sincron/api/login");

    //Ruta LOCAL
    win.loadURL(`file://${__dirname}/index.html`);

    //mover una ventana
    win.on('move', () => {
        const position = win.getPosition();
        console.log(`Posición de la ventana ${position}`); 
    });


    //detecta el cierre de la ventana
    win.on('closed', () => {
        win = null;
        app.quit();
    });
});

//cerrar el aplicativo
//app.quit();