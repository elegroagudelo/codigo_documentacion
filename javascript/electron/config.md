
ElectronJS
============

Inicializar
-----------
Se requiere de inicializar un proyecto npm  
No se recomienda el instalar la version global de electron  

* ~ npm init
* ~ npm install electron --save

Configurar el package.js    
-------
En los scripts, se requiere de agrega el ejecutador de electron 
```
scripts: {
        "dev": "electron app.js",
        "test": "echo \"Hola electron test\""
    }
```

electron-compile
-----------------
Instalamos el electron compile como una dependencia de electron 
* ~ npm install electron-prebuilt-compile --save-dev 

Para usar electorn-compile, se requiere de remover electron normal  
* ~ npm remove electron --save && npm install electron-compile --save  
Permite por medio de babel el compilar ES6      

```
    import {app, BrowserWindow} from 'electron'
```

Variables de entorno con cross-env
Para cualquier plataforma puede funcionar
-------
* ~ npm install cross-env --save

```
    "dev":"cross-env NODE_ENV=development electorn app.js"
```
Ahora   
```
    if(process.env.NODE_ENV == "development")
    {
        console.log("Ambiente para desarrollo")
    }
```


RUN aplication
-----------
Se requiere de correr nuestra aplicación    
* ~ npm run dev


Modo de pruebas del aplicativo
---------------------
Las herramientas del navegador de la ventana chrome
```
    if(process.env.NODE_ENV == "development")
    {
        win.toggleDevTools()
    }
```

electron-debug
--------------
* ~ npm install electron-debug --save    
* ~ npm install devtron --save-dev  
