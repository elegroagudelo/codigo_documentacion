Angular CLI
============
Command Line Interface For Angular      
Angular por medo de la terminal de comando, permite crear, modificar y testear la aplicación.       

Instalación
--------------
* ~ npm install -g @angular/cli     --o tambien: --
* ~ npm install @angular/cli@12.2.15
* ~ ng new myapp
* ~ cd myapp
* ~ ng serve


Ayudas:
* ~ ng --help     
* ~ ng new --help
* ~ ng generate --help
* ~ ng add --help
* Test, Lint, puebas unitarias por CLI


Producción (build)      
* ~ ng build myapp -c production


Estructura De Directorios
--------------------------

|Dir    |Detalle    |
|-------|-----------|
|dist/|Directorio de compilación del proyecto| 
|src/| Todos los archivos necesarios para el desarrollo.|
|src/assets/| Todas las librerias necesarias como backgrounds|
|src/enviroments/| Los archivos para la configuracion del deploy, (desarrollo) y (produccion), e establecen las diferentes rutas durante el desarrollo|
|app/|Se alojan los componentes de Angular, Y todos los principales, para el desarrollo|



