<? include "inc/head.php"; ?>
<script type="text/javascript"></script>
<div class="container">
	<h1>Consulta ajax</h1>
	<div id="datos"></div>
</div>

<script type="text/javascript">
	var request = new XMLHttpRequest(); //llamadas asincronas

	request.open('GET', 'objetosjson.php');
	request.onreadystatechange = function() {
		if ((request.status == 200) && (request.readyState == 4)) {
			document.getElementById("datos").innerHTML = request.responseText;
		}
	}
	request.send();
	/*
	readyState		Descripci�n
		0			La solicitud no se ha inicializado
		1			Conexi�n con el servidor establecida
		2			El servidor ha recibido la solicitud
		3			El servidor est� procesando la solicitud
		4			La solicitud ha sido procesada y la respuesta est� lista
	*/
</script>