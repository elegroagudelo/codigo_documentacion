<? include "inc/head.php"; ?>
<script type="text/javascript" src="inc/js/json.js"></script>
<script type="text/javascript" src="inc/js/aeropuertos.json"></script>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<a class="btn" href="https://mega.nz/#!68wmjbgZ!SVdNEfP-KSgPtmwRX8jsmgG8QJnxfHmsmYS_mtAHz8g">mega1</a>
			<a class="btn" href="https://mega.nz/#!WsI2DSBB!YY4CPEFFPSEGltyZz2btyV_FIILjOsbAu9UHr8sokFM">mega2</a>
			<a class="btn" href="https://mega.nz/#!n8JiRIpY!TT-3Qq0glEFR5h-wn5-n5o1r0ffyzXWmNiUR1ZhTR-s">mega3</a>
			<a class="btn" href="https://mega.nz/#!OhAiDRLb!ir7Vi_VZcRCCcwIQLQ5eNFdrnMI7rp32tdsdBAyegVU">mega4</a>
			<h4>Consultar un js externo</h4>
			<p>Consulta asyncronic para cargar un json</p>
			<p id="datos"></p>
		</div>
		<hr>
		<div class="col-xs-12">
			<div id="aeropuertos"></div>
		</div>
	</div>
</div>

<script>
	var tabla = '<table class="table table-bordered"><thead><tr><th>Nombre</th><th>Apellidos</th><th>Edad</th></tr></thead><tbody>';
	for (var i = 0; i < json.length; i++) {
		tabla += "<tr><td>" + json[i].nombre + "</td><td>" + json[i].apellidos + "</td><td>" + json[i].edad + "</td></tr>";
	}
	tabla += '</tbody></table>';
	document.getElementById('datos').innerHTML = tabla;


	var cont = 0;
	var listado = aeropuertos.length;
	var lista = "<div class='list-group'>";

	while (cont < listado) {
		lista += "<div class='list-group-item'>Nombre: " + aeropuertos[cont].name + " Typo:" + aeropuertos[cont].type + "</div>";
		cont++;
	}
	lista += "</div>";
	document.getElementById('aeropuertos').innerHTML = lista;
</script>