<? include "inc/head.php"; ?>

<div class="container">
	<h4>Anuncios</h4>
	<span id="datos"></span>
	<p>Pasar datos de Json a un array de php</p>

	<?
	$miltiarreglo = '{
		"Idcursos":[ 23, 24, 25],
		"cursos": {
				"sistemas":"desarrollo de software",
				"Diseño":"manejo de Corel Draw"
			},
		"maestro":["Ricardo Perez"]
	}';

	$json = json_decode($miltiarreglo, true);
	print_r($json);
	echo "<hr/>";
	var_dump($json);

	echo "<hr/>";

	$varios = '{
	   	"nombres": "edwin legro",
				"identidad": 1110491951,
				"Logeado":false,
				"pendiente":null
				}';

	//el Json lo convertimos en un array
	$array = json_decode($varios, true);

	foreach ($array as $clave => $valor) {
		echo "Clave: $clave; Valor: $valor<br />\n";
	}
	?>
	<hr>
	<p>Pasar datos de php a un array Json</p>
	<?
	//JSON en Php
	$variosObjetos = array(
		"nombres" => "edwin legro",
		"identidad" => 1110491951,
		"Logeado" => false,
		"Idcursos" => array(23, 24, 25),
		"cursos" => array(   //anidar objetos
			"sistemas" => "desarrollo de software",
			"Diseño" => "manejo de Corel Draw"
		),
		"pendiente" => null
	);

	echo json_encode($variosObjetos);
	?>
</div>