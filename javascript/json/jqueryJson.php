<? include "inc/head.php"; ?>

<div class="container">
	<h1>Jquery y Json</h1>

	<input type="button" id="lista" value="Mostrar Lista" />
	<div id="contenedor">
		<ul class="list-group" id="contenedorLista">
		</ul>
	</div>
</div>

<!-- Cuando hacemos clic en el botón, vamos a utilizar el método getJSON para hacer una llamada AJAX al servidor web para recuperar los datos JSON. Dado que estamos recibiendo una variedad indefinida de elementos, en este caso profesores, pasaremos los datos recuperados en cada iterador jQuery para recuperar un elemento a la vez. Dentro del iterador, estamos construyendo una cadena, que se adjunta como un elemento de la lista a la que llamamos “contenedorLista”, y la pasamos como lista desordenada. -->

<script type="text/javascript">
	$(document).ready(function() {
		$("#lista").click(function() {
			$.getJSON("objetosjson.php", function(data) {
				if (data) {
					$.each(data, function(key, value) {
						$("#contenedorLista").append(
							"<li class='list-group-item'>" + key + "El ID de Profesor es " + value.numeroID + " y su nombre es " + value.nombre + " " + value.apellido + "</li>");
					});
				}
			});
		});
	});
</script>