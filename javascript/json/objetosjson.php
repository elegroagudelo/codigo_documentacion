<?php
header('Content-Type: application/json');

$profesores = array(
	array(
		"numeroID" => "3",
		"nombre"   => "Jorge",
		"apellido" => "Mochon",
		"especialidad" => array("Imagen", "Video")
	),
	array(
		"numeroID" => "120",
		"nombre"   => "Luis",
		"apellido" => "Berganza",
		"especialidad" => array("Desarrollo", "Aplicaciones Moviles")
	)
);

echo json_encode($profesores);
