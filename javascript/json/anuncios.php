<? include "inc/head.php"; ?>

<div class="container">
	<h1>Bienvenidos</h1>
	<p>Los objetos de Json requiren de clave: valor<br />
		Los numeros no van entre comillas<br />
		Las claves y valores simpre van entre comillas<br />
		Los booleanos tambien van sin comillas como tambien los null <br />
		Los objetos permiten anidar mas objetos con {}</p>
	<div class="panel">
		<h4>Objeto 1</h4>
		<code>usuarios = {"nombres":"Edwin Andres", "apellidos":"Legro Agudelo"};<br />
			document.getElementById("datos").innerHTML = usuarios.nombres +' '+ usuarios.apellidos;</code>
		<p id="datos"></p>
	</div>

	<div class="panel">
		<h4>Objeto 2</h4>
		<code> var codigos= ["TA20142001"];<br />
			document.getElementById("code").innerHTML= codigos;</code>
		<p id="code"></p>
	</div>

	<div class="panel">
		<h4>Objeto 3</h4>
		<code>
			var codigos= ["TA20142001", "TB2014202", "TC20145678"];<br />
			document.getElementById("code2").innerHTML= codigos[2];
		</code>
		<p id="code2"></p>
	</div>
	<div class="panel">
		<h4>Objeto 4</h4>
		<code>
			var dias;<br />
			dias= ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];<br />
			document.getElementById("dias").innerHTML= dias;
		</code>
		<p id="dias"></p>
	</div>

	<div class="panel">
		<h4>Objeto 5</h4>
		<p>Array de Objetos</p>
		<code>
			var clientes=[<br />
			{ "nombre":"edwin",<br />
			"code":"12345" <br />
			},<br />
			{ "nombre":"andres",<br />
			"code":"2345" <br />
			},<br />
			{ "nombre":"carlos",<br />
			"code":"3104" <br />
			}<br />
			];<br />
			document.getElementById("registros").innerHTML= clientes[2].nombre;
		</code>
		<p id="registros"></p>
	</div>

	<div class="panel">
		<h4>Objeto 6</h4>
		<code>
			var numero ={"valor":1000};<br />
			document.getElementById("numero").innerHTML= numero.valor;
		</code>
		<p id="numero"></p>
	</div>

	<div class="panel">
		<h4>Object 7</h4>
		<p>Los objetos pueden tener arrays</p>
		<code>
			var varios = {<br />
			"nombres": "edwin legro",<br />
			"identidad": 1110491951,<br />
			"Logeado":false,<br />
			"Idcursos":[23,24,25],<br />
			"cursos": { <br />
			"sistemas":"desarrollo de software",<br />
			"Diseño": "manejo de Corel Draw"<br />
			},<br />
			"pendiente":null <br />
			};<br />

			document.getElementById("variosObjetos").innerHTML= varios.cursos.sistemas;
		</code>
		<span id="variosObjetos"></span>
		<hr>
	</div>

	<div class="panel">
		<P>PHP AND JSON</P>
		<P>por medio de un echo a un json_encode que contenga el arreglo de php optenemos lo siguiente:</P>
		<?php
		$variosObjetos = array(
			"nombres" => "edwin legro",
			"identidad" => 1110491951,
			"Logeado" => false,
			"Idcursos" => array(23, 24, 25),/*agergar un arreglo */
			"cursos" => array("sistemas" => "desarrollo de software", "Diseño" => "manejo de Corel Draw"),/*anidar objetos*/
			"pendiente" => null
		);

		//JSON DESDE PHP
		echo json_encode($variosObjetos);
		?>
	</div>

</div>

<script type="text/javascript">
	var usuarios;

	//Objeto
	usuarios = {
		"nombres": "Edwin Andres",
		"apellidos": "Legro Agudelo"
	};

	document.getElementById("datos").innerHTML = usuarios.nombres + ' ' + usuarios.apellidos; //mostramos desde Javascript

	//arreglos o Coleccion ordenada de valores
	//De un elemento

	var codigos = ["TA20142001"];
	document.getElementById("code").innerHTML = codigos;

	//De multi elementos
	var codigos = ["TA20142001", "TB2014202", "TC20145678"];
	document.getElementById("code2").innerHTML = codigos[2];

	//Dias de la semana
	var dias;
	dias = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
	document.getElementById("dias").innerHTML = dias;

	//varios Objetos

	var clientes = [{
			"nombre": "edwin",
			"code": "12345"
		},
		{
			"nombre": "andres",
			"code": "2345"
		},
		{
			"nombre": "carlos",
			"code": "3104"
		}
	];
	document.getElementById("registros").innerHTML = clientes[2].nombre;


	//en Objetos Json los numeros no van en comillas dobles ni los booleanos.
	var numero = {
		"valor": 1000
	};
	document.getElementById("numero").innerHTML = numero.valor;

	//varios tipos de datos
	var varios = {
		"nombres": "edwin legro",
		"identidad": 1110491951,
		"Logeado": false,
		"Idcursos": [23, 24, 25],
		"cursos": {
			"sistemas": "desarrollo de software",
			"Diseño": "manejo de Corel Draw"
		},
		"pendiente": null
	};

	document.getElementById("variosObjetos").innerHTML = varios.cursos.sistemas;
</script>