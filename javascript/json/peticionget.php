<?php include('inc/head.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h3>Metodo AJAX GET </h3>
			<code>
				$(function(){<br />
				$.ajax({<br />
				url: 'objetosjson.php',<br />
				type: 'GET',<br />
				dataType: 'JSON',<br />
				data: {},<br />
				})<br />
				.done(function(data) {<br />
				console.log(data);<br />
				});<br />
				<br />
				});<br />
			</code>
			<div id="datos"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$.ajax({
				url: 'objetosjson.php',
				type: 'GET',
				dataType: 'JSON',
				data: {},
			})
			.done(function(data) {
				document.getElementById("datos").innerHTML = data[0].nombre;
			});
	});
</script>