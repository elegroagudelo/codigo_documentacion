// Ejemplo de closure

const ProcesarMonedas = (i) =>  {
    var saveCoins = 0;
    let dataCoin = i; //le pasamos el valor del argumento 
    
    const calcularMoneda = (coins) => {
        saveCoins +=coins;
        dataCoin+= 1;
        console.log(saveCoins); 
        console.log(dataCoin); 
    };

    return calcularMoneda;
};

let moneybox = ProcesarMonedas(5);
moneybox(20);
moneybox(15);
moneybox(40);



// Variables privadas de los closures
const Persona = (i) =>  {
    var saveName = i;

    return {
        getname: () => {
            return saveName;
        },
        setname: (nombre) => {
            saveName = nombre;
        }
    };
};

let person = Persona("Edwin Legro");
console.log(person.getname());

person.setname("Alan felipe");
console.log(person.getname());




// Que es el Hoisting, levantamiento de las declaraciones
// Es la forma que se eleva en las declaraciones de las variables, por medio del motor que compila en el navegador
// solo para casos de donde se declara la variable, pero en la inicializacion de las variables o funciones no

a = 2;
var a;
console.log(a); //la variable pasa por elevacion gracias al hoisting


console.log(item + " OK");
var item ="";
//const item = "Variable definida posteriormente";   //genera error para constantes y variables de tipo let

namefunction("Edwin Prueba de Hoisting");

/* var namefunction = (name)=> {
    console.log(name);
}; */

function namefunction(name){
    console.log(name);
}

