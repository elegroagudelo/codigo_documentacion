

void main() {
	String marca = 'BMW';
	String modelo = '2012';
	int ano = 2019;
	double km_max = 80.000;

	print('Hello, World! la marca es $marca');
	print('ano es $ano');
	print('modelo es $modelo');

	//concatenar
	print('todo $marca $ano $km_max');
	print(marca.length);
	print(marca[1]);
	print('tamaño marca '+ marca.length.toString());

	//genericas, predefine el tipo de dato 
	var valor = 12000.00;
	print('valor es $valor');

	//datos boolean
	bool esVerdadPremisa = true;
	print('premisa $esVerdadPremisa');

	if(!(esVerdadPremisa == false)){
		print('es Verdad que es Verdad');
	} else {
		print('no Verdad que es Verdad');
	}
}