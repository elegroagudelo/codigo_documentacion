void main() {
	var vehiculo = new Vehiculo('BMW', '2023', 5500000.00);
	print(vehiculo.toStringer());

	//variables staticas
	final alVehiculos = new Vehiculo('TOYOTA', '2022', 8500000.00);
	print(alVehiculos);
	alVehiculos.modelo = "2019";
	print(alVehiculos);

  alVehiculos.miVehiculo(marca: "MASDA", modelo: "2023", valor: 200000.0, peso: 20000);
  print(alVehiculos.toStringer());
}

class Vehiculo {
	
	String marca='';
	String modelo='';
	double valor=0.0;

	Vehiculo(String marca, String modelo, double valor){
		this.marca = marca;
		this.modelo = modelo;
		this.valor = valor;
	}

	miVehiculo({String marca='', String modelo='', double valor=0, int peso=0}){
		this.marca = marca;
		this.modelo = modelo;
		this.valor = valor;
	}

	String toString(){
		return ' $marca $modelo $valor';
	}

	String toStringer() => 'Datos $marca $modelo $valor';
}