
void main() {

	setName('Edwin Legro');

	//para un valor dinamico segun lo que pueda retornar, no muy recomendado
	dynamic value = getName();
	print(value);

	print(getName());

	print(getCalifica('Edwin Legro', 100));

	print(getCalificacion(nombre:'Edwin Legro', valor:100));
}

//funcion de retorno get de nombre, con retorno generico 
getName(){
	return 'edwin legro';
}

String getFullname(){
	return 'edwin legro';
}

int getEdad(){
	return 33;
}

//metodo no retorna valor
void setName(nombre){
	print(nombre);
}

//referencia de parametros
String getCalifica(String nombre, int valor){
	return 'Resultado $nombre $valor';
}

//transferencia por referencia de parametros
String getCalificacion({String nombre='', int valor=0}){
	return 'Resultado $nombre $valor';
}