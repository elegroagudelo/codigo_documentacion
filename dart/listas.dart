void main() {

	//Listas genericas
	List edadesGenericas = [11,12,13,14,15];
	print(edadesGenericas);

	// adiciona un nuevo valor
	edadesGenericas.add("Pepe");
	print(edadesGenericas);

	//lista definida con valores enteros
	List <int>edades = [11,12,13,14,15];
	edades.add(17);
	print(edades);

	//instancia a la clase de listas cerrada
	List<String> listaInstancia = List<String>.filled(4, '', growable: true);
	listaInstancia[0] = 'edwin';
	listaInstancia[1] = 'andres';
	listaInstancia[2] = 'legro';
	listaInstancia[3] = 'agudelo';

	print(listaInstancia);

	//Object Map dynamic
	Map vehiculo = {
		"id":1,
		"nombre": "carro",
		1: 'Corre'	
	};
	print(vehiculo);
	print(vehiculo['nombre']);

	//Object map string
	Map<String, dynamic> vehiculoString = {
		"id":2,
		"nombre": "caballo"
	};
	print(vehiculoString);

	//agregar datos al Map
	vehiculoString.addAll({"pais":"Colombia"});
	print(vehiculoString);

	Map<int, dynamic> modelos = {
		1:2021,
		2:2023,
		3:"Pendiente"
	};
	print(modelos);
	modelos[1]= "Otro valor";
	print(modelos);

}