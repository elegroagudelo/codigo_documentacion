Extensiones
==============

* Angular Snippets v12
* Backbone.js snippets
* Underscore.js snippets
* Bracket Pair Colorizer 2
* Code Runner
* Django
* Docker
* Emmet Keybindings
* ESlint
* Git Grapth
* Git History
* HTML Boilerplate
* Javascript ES6 code snippets
* jQuery Snippets
* Markdown Preview Github Styling
* Mithril Emmet
* Monokai pro
* PHP DocBlocker
* PHP Intelephense
* Prettier Code formater 
* SFTP doujinya
* TSLint
* VsCode Typescript Compiler