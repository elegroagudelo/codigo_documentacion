CONFIG GIT SERVER
=================

Generando su clave pública SSH
Dicho esto, muchos servidores de Git se autentican usando claves públicas SSH.  
Para proporcionar una clave pública, cada usuario de su sistema debe generar una si aún no la tiene.   
Este proceso es similar en todos los sistemas operativos. En primer lugar, debe verificar para asegurarse    
de que todavía no tiene una clave. De forma predeterminada, las claves SSH de un usuario se almacenan   
en ~ / .ssh de ese usuario directorio. Puede verificar fácilmente si ya tiene una clave yendo a ese   
directorio y enumerando los contenidos: 

* ~ cd ~/.ssh
* ~ ls

authorized_keys2 id_dsa known_hosts
config id_dsa.pub

Está buscando un par de archivos llamados something y something.pub, donde el elemento suele ser id_dsa o id_rsa.   
El archivo .pub es su clave pública, y el otro archivo es su clave privada. Si no tiene estos   
archivos (o si no tiene un directorio .ssh), puede crearlos ejecutando un programa llamado ssh-keygen,  
que se proporciona con el paquete SSH en sistemas Linux / Mac y viene con el paquete MSysGit en Windows:  

* ~ ssh-keygen

Generating public/private rsa key pair.
Enter file in which to save the key (/Users/schacon/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:

Your identification has been saved in /Users/schacon/.ssh/id_rsa.
Your public key has been saved in /Users/schacon/.ssh/id_rsa.pub.
The key fingerprint is:


Primero, confirma dónde desea guardar la clave (.ssh / id_rsa), y luego solicita dos veces una frase de contraseña, que puede dejar en blanco si no desea ingresar una contraseña cuando usa la clave. Ahora, cada usuario que hace esto debe enviar su clave pública a usted o a quien administre el servidor Git (suponiendo que esté utilizando una configuración de servidor SSH que requiera claves públicas). Todo lo que tienen que hacer es copiar el contenido del archivo .pub y enviarlo por correo electrónico. Las claves públicas se ven algo como esto:

* ~ cat ~/.ssh/id_rsa.pub

Para obtener un tutorial más profundo sobre la creación de una clave SSH en múltiples sistemas operativos, consulte la guía de GitHub en las teclas SSH en

http://github.com/guides/providing-your-ssh-key.
Configurando el Servidor
Veamos cómo configurar el acceso SSH en el lado del servidor. En este ejemplo, usará el método authorized_keys para autenticar a sus usuarios. También asumimos que está ejecutando una distribución estándar de Linux como Ubuntu. Primero, creas un usuario 'git' y un directorio .ssh para ese usuario.

* ~ sudo adduser git
* ~ su git
* ~ cd
* ~ mkdir .ssh

A continuación, debe agregar algunas claves públicas de desarrollador SSH al archivo authorized_keys para ese usuario. Supongamos que ha recibido algunas claves por correo electrónico y las ha guardado en archivos temporales. Nuevamente, las claves públicas se ven algo como esto:

* ~ cat /tmp/id_rsa.john.pub

Simplemente añádalos a su archivo authorized_keys:

* ~ cat /tmp/id_rsa.john.pub >> ~/.ssh/authorized_keys
* ~ cat /tmp/id_rsa.josie.pub >> ~/.ssh/authorized_keys
* ~ cat /tmp/id_rsa.jessica.pub >> ~/.ssh/authorized_keys
* ~ cat /tmp/id_rsa.elegro.pub >> ~/.ssh/authorized_keys

Ahora, puede configurar un repositorio vacío para ellos ejecutando git init con la opción --bare, que inicializa el repositorio sin un directorio de trabajo:

* ~ cd /opt/git
* ~ mkdir project.git
* ~ cd project.git
* ~ git --bare init

Luego, John, Josie o Jessica pueden insertar la primera versión de su proyecto en ese repositorio agregándolo como un control remoto y empujando hacia arriba una rama. Tenga en cuenta que alguien debe realizar un shell en la máquina y crear un repositorio desnudo cada vez que quiera agregar un proyecto. Usemos gitserver como el nombre de host del servidor en el que ha configurado su usuario y repositorio 'git'. Si lo está ejecutando internamente, y configura DNS para gitserver para que apunte a ese servidor, entonces puede usar los comandos más o menos como es:

# on Johns computer
* ~ cd myproject
* ~ git init
* ~ git add . 
* ~ git commit -m 'initial commit'
* ~ git remote add origin git@gitserver:/opt/git/project.git
* ~ git push origin master

En este punto, los demás pueden clonarlo e impulsar los cambios de nuevo con la misma facilidad:

* ~ git clone git@gitserver:/opt/git/project.git
* ~ cd project
* ~ vim README
* ~ git commit -am 'fix for the README file'
* ~ git push origin master

Con este método, puede obtener rápidamente un servidor de lectura / escritura Git para un puñado de desarrolladores. Como precaución adicional, puede restringir fácilmente al usuario 'git' a solo hacer actividades de Git con una herramienta de shell limitada llamada git-shell que viene con Git. Si configura esto como el shell de inicio de sesión de su usuario 'git', entonces el usuario 'git' no puede tener acceso normal a su servidor. Para usar esto, especifique git-shell en lugar de bash o csh para el shell de inicio de sesión de su usuario. Para hacerlo, es probable que deba editar su archivo / etc / passwd:

$ sudo vim /etc/passwd
En la parte inferior, deberías encontrar una línea que se parece a esto:

git:x:1000:1000::/home/git:/bin/sh
Cambie / bin / sh a / usr / bin / git-shell (o ejecute git-shell para ver dónde está instalado). La línea debería verse más o menos así:

git:x:1000:1000::/home/git:/usr/bin/git-shell
Ahora, el usuario 'git' solo puede usar la conexión SSH para empujar y arrastrar repositorios Git y no puede shell en la máquina. Si lo intenta, verá un rechazo de inicio de sesión como este:

* ~ ssh git@gitserver

fatal: What do you think I am? A shell?
Connection to gitserver closed.
Acceso público
¿Qué sucede si desea acceso de lectura anónimo a su proyecto? Quizás en lugar de ser el anfitrión de un proyecto privado interno, desee alojar un proyecto de código abierto. O tal vez tenga un montón de servidores de compilación automatizados o servidores de integración continua que cambian mucho, y no desea tener que generar claves SSH todo el tiempo; solo desea agregar acceso de lectura anónimo simple.

Probablemente, la forma más simple para las configuraciones más pequeñas es ejecutar un servidor web estático con su raíz de documento donde están sus repositorios Git, y luego habilitar ese gancho post-actualización que mencionamos en la primera sección de este capítulo. Trabajemos del ejemplo anterior. Digamos que tiene sus repositorios en el directorio / opt / git, y un servidor Apache se está ejecutando en su máquina. Nuevamente, puedes usar cualquier servidor web para esto; pero como ejemplo, demostraremos algunas configuraciones básicas de Apache que deberían darle una idea de lo que podría necesitar.

Primero necesitas habilitar el gancho:

* ~ cd project.git
* ~ mv hooks/post-update.sample hooks/post-update
* ~ chmod a+x hooks/post-update

¿Qué hace este gancho post-update? Se ve básicamente así: 

* ~ cat .git/hooks/post-update  
!/bin/sh

An example hook script to prepare a packed repository for use over
dumb transports.
To enable this hook, rename this file to "post-update".

* ~ exec git-update-server-info
Esto significa que cuando ingresas al servidor a través de SSH, Git ejecutará este comando para actualizar los archivos necesarios para HTTP atractivo.

A continuación, debe agregar una entrada VirtualHost a su configuración de Apache con la raíz del documento como el directorio raíz de sus proyectos de Git. Aquí, asumimos que tiene un DNS comodín configurado para enviar * .gitserver a la caja que está usando para ejecutar todo esto:

<VirtualHost *:80>
  ServerName git.gitserver
  DocumentRoot /opt/git
  <Directory /opt/git/>
      Order allow, deny
      allow from all
  </Directory>
</VirtualHost>


También deberá configurar el grupo de usuarios de Unix de los directorios / opt / git en www-data para que su servidor web pueda acceder a los repositorios, porque la instancia de Apache que ejecuta el script CGI se ejecutará (de forma predeterminada) como tal usuario:

* ~ chgrp -R www-data /opt/git

Cuando reinicie Apache, debería poder clonar sus repositorios bajo ese directorio especificando la URL para su proyecto:

* ~ git clone http://git.gitserver/project.git

De esta manera, puede configurar el acceso de lectura basado en HTTP a cualquiera de sus proyectos para una cantidad razonable de usuarios en pocos minutos. Otra opción simple para el acceso público no autenticado es iniciar un daemon de Git, aunque eso requiere que demoniza el proceso; cubriremos esta opción en la siguiente sección, si prefiere esa ruta.
