export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
export GEM_PATH="/home/elegro/.rbenv/versions/2.5.5/lib/ruby/gems/2.5.0"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH="/opt/blender:$PATH"
export ANDROID_HOME="$HOME/.android/android-studio"
export PATH="$ANDROID_HOME/sdk/tools:$PATH"
export PATH="$ANDROID_HOME/gradle:$PATH"
export PATH="$HOME/.symfony/bin:$PATH"
