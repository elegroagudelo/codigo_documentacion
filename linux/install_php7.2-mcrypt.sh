##
## How to install mcrypt in php7.2 / php7.3
## Linux / MacOS / OSX
##

## https://lukasmestan.com/install-mcrypt-extension-in-php7-2/
sudo apt-get install -y php7.3-cli php7.3-dev php7.3-pgsql php7.3-sqlite3 php7.3-gd php7.3-curl php-memcached php7.3-imap php7.3-mysql php7.3-mbstring php7.3-xml php-imagick php7.3-zip php7.3-bcmath php7.3-soap php7.3-intl php7.3-readline php7.3-common php7.3-pspell php7.3-tidy php7.3-xmlrpc php7.3-xsl php7.3-opcache php-apcu
sudo apt-get install php7.3 libapache2-mod-php7.3 php7.3-cli php7.3-mysql php7.3-gd php7.3-imagick php7.3-recode php7.3-tidy php7.3-xmlrpc php7.3-pgsql php7.3-openssl

sudo nano /etc/php/7.3/apache2/php.ini

sudo systemctl restart apache2.service


sudo apt install php-dev libmcrypt-dev php-pear
sudo pecl channel-update pecl.php.net
sudo pecl install mcrypt-1.0.1

sudo nano /etc/php/7.3/apache2/php.ini
extension=mcrypt.so

# 
# Check version php and pecl
# 
php -v # if default php is not 7.2 then use /usr/bin/php7.2 instead php
pecl version
sudo apt-get install php-pear
sudo apt-get install php7.3-dev

# 
# Install mcrypt extension
# see http://pecl.php.net/package-info.php?package=mcrypt&version=1.0.1
# 
sudo apt-get -y install gcc make autoconf libc-dev pkg-config
sudo apt-get -y install libmcrypt-dev
sudo pecl install mcrypt-1.0.1

# 
# When you are shown the prompt
# 
# libmcrypt prefix? [autodetect] :
# Press Enter to autodetect.

# 
# After success installing mcrypt trought pecl, you should add mcrypt.so extension to php.ini,
# The output will look like this:
# 
# ...
# Build process completed successfully
# Installing '/usr/lib/php/20170718/mcrypt.so'    ---->   this is our path to mcrypt extension lib
# install ok: channel://pecl.php.net/mcrypt-1.0.1
# configuration option "php_ini" is not set to php.ini location
# You should add "extension=mcrypt.so" to php.ini

# 
# Grab installing path and add to cli and apache2 php.ini 
# 
# example:
sudo bash -c "echo extension=/usr/lib/php/20180731/mcrypt.so > /etc/php/7.3/cli/conf.d/mcrypt.ini"
sudo bash -c "echo extension=/usr/lib/php/20180731/mcrypt.so > /etc/php/7.3/apache2/conf.d/mcrypt.ini"

# check that the extension was installed with this command:
php -i | grep mcrypt

# 
# The output will look like this:
# 
# /etc/php/7.2/cli/conf.d/mcrypt.ini
# Registered Stream Filters => zlib.*, string.rot13, string.toupper, string.tolower, string.strip_tags, convert.*, consumed, dechunk, convert.iconv.*, mcrypt.*, mdecrypt.*
# mcrypt
# mcrypt support => enabled
# mcrypt_filter support => enabled
# mcrypt.algorithms_dir => no value => no value
# mcrypt.modes_dir => no value => no value

# final step
sudo service apache2 restart

phpenmod cli 
phpenmod dev 
phpenmod pgsql 
phpenmod sqlite3 
phpenmod gd 
phpenmod curl 
phpenmod php-memcached 
phpenmod imap 
phpenmod mysql 
phpenmod mbstring 
phpenmod xml 
phpenmod php-imagick 
phpenmod zip 
phpenmod bcmath 
phpenmod soap 
phpenmod intl 
phpenmod readline 
phpenmod common 
phpenmod pspell 
phpenmod tidy 
phpenmod xmlrpc 
phpenmod xsl 
phpenmod opcache 
phpenmod php-apcu
