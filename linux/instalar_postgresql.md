Postgresql Debian
=================

Instalar el postgres
Para linuxmint se requiere de cambiar $(lsb_release -cs)-pgdg por bionic-pgdg
Create the file repository configuration:
* ~ sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

Import the repository signing key:
* ~ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

Update the package lists:
* ~ sudo apt-get update

Install the latest version of PostgreSQL.
If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':

* ~ sudo apt-get -y install postgresql-12
* ~ sudo apt-get -y install postgresql-client-12
* ~ sudo apt-get -y install postgresql-contrib-9.x
* ~ sudo apt-get -y install libpq-dev
* ~ sudo apt-get -y install postgresql-server-dev-12

pgAdmin Debian
========
Para debian remplazar $(lsb_release -cs) por focal para linuxmint

* ~ sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
* ~ sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list'
* ~ sudo apt-get update
* ~ sudo apt-get -y install pgadmin4
* ~ sudo apt install pgadmin4-web

Configuracion
=============

Se define la clave del usuario postgres
* ~ sudo -u postgres createuser elegro_admin -s

Se actualiza el posgres local de debian
* $~ sudo passwd postgres
* $~ su - postgres
* postgres@equipo:~$ createdb -h localhost -p 5432 -U elegro_admin testdb



Se ingresa al posgres user psql
----------------------------------
* $~ sudo -u postgres psql
* postgres=# \password postgres


* postgres=# \password elegro_admin
Define el password del usuario a crear

* postgres= #\q

Crear database
---------------
* su - postgres
* postgres@equipo:~$ createdb -h localhost -p 5432 -U elegro_admin testdb
* postgres@equipo:~$ exit

Volvemos a ingresar con usuario postgres para darle todos los privilegios
* postgres=# grant all privileges on database $DATABASE to $USERNAME;
* postgres=# \du


Para ingresar con el nuevo usuario
* psql -h localhost -p 5432 -U dbuser -d testdb
* testdb=# \l
* testdb=# \du          #muestra los usuarios
* testdb=# \dt          #muestra las tablas
* testdb=# \c postgres #cambia de base de datos
* testdb=# \h           #herramienta de ayuda
* testdb=# \q           #salir
* testdb=# SELECT version();  #muestra en detalle la version del gestor instalado
* testdb=# SELECT current_date; #muestra la fecha actual del sistema
* testdb=# SELECT current_user;
* testdb=# SELECT random();

Importar sql
============
* testdb=# \i basics.sql


importar de un archivo txt
==================================
* testdb=# COPY $TABLE FROM '/home/user/$table.txt';


Borrar base de datos
===================
* $~ su - postgres
* postgres@equipo:~$ dropdb testdb



Operators
==========
Un nombre de operador es una secuencia de hasta NAMEDATALEN-1 (63 por defecto) caracteres de la siguiente lista:
# + - * / < > = ~ ! @ # % ^ & | ` ?

Special Characters
====================
* Se utiliza un signo de dólar ($) seguido de dígitos para representar un parámetro posicional en el cuerpo de una definición de función o una instrucción preparada. En otros contextos, el signo de dólar puede ser parte de un identificador o una constante de cadena entre comillas de dólar.

* Los paréntesis (()) tienen su significado habitual para agrupar expresiones y hacer cumplir la precedencia. En algunos casos, se requieren paréntesis como parte de la sintaxis fija de un comando SQL en particular.

* Los corchetes ([]) se utilizan para seleccionar los elementos de una matriz.

* Las comas (,) se utilizan en algunas construcciones sintácticas para separar los elementos de una lista.

* El punto y coma (;) termina un comando SQL.

* Los dos puntos (:) se utilizan para seleccionar "rebanadas" de matrices.

* El asterisco (*) se usa en algunos contextos para indicar todos los campos de una fila de tabla o valor compuesto. También tiene un significado especial cuando se usa como argumento de una función agregada, es decir, que el agregado no requiere ningún parámetro explícito.

* El punto (.) se utiliza en constantes numéricas y para separar nombres de esquemas, tablas y columnas.


Comments
========
* -- Esto es un comentario
* /* multiline comment with nesting: /* nested block comment */


Operator Precedence
===================

Formas incorrectas SELECT 5 ! - 6  /* tambien */  SELECT 5 ! (- 6);
Correcta:
* testdb=# SELECT (5 !) - 6;  /* es = 114 */


Operadores
===========

* .	left	table/column name separator
* ::	left	PostgreSQL-style typecast
* [ ]	left	array element selection
* + -	right	unary plus, unary minus
* ^	left	exponentiation
+ -	left	addition, subtraction
* (any other operator)	left	all other native and user-defined operators
* BETWEEN IN LIKE ILIKE SIMILAR	 	range containment, set membership, string matching
* < > = <= >= <>	 	comparison operators
* IS ISNULL NOTNULL	 	IS TRUE, IS FALSE, IS NULL, IS DISTINCT FROM, etc
* NOT	right	logical negation
* AND	left	logical conjunction
* OR	left	logical disjunction


SCHEMA
===========
* testdb=# CREATE SCHEMA myschema;
* testdb=# DROP SCHEMA myschema CASCADE;
* testdb=# CREATE SCHEMA schema_name AUTHORIZATION user_name;

|Name               |Size       | Description                       |Range                                                                                      |
|-------------------|-----------|-----------------------------------|-------------------------------------------------------------------------------------------|
|smallint           |2 bytes    |small-range integer                |-32768 to +32767                                                                           |
|integer            |4 bytes    |typical choice for integer         |-2147483648 to +2147483647                                                                 |
|bigint             |8 bytes	|large-range integer	            |-9223372036854775808 to +9223372036854775807                                               |
|decimal            |variable	|user-specified precision, exact	|up to 131072 digits before the decimal point; up to 16383 digits after the decimal point   |
|numeric            |variable	|user-specified precision, exact	|up to 131072 digits before the decimal point; up to 16383 digits after the decimal point   |
|real               |4 bytes	|variable-precision, inexact	    |6 decimal digits precision                                                                 |
|double precision   |8          |bytes	variable-precision, inexact	|15 decimal digits precision                                                                |
|smallserial        |2 bytes    |small autoincrementing integer	    |1 to 32767                                                                                 |
|serial             |4 bytes    |autoincrementing integer	        |1 to 2147483647																			|
|bigserial          |8 bytes    |large autoincrementing integer	    |1 to 9223372036854775807															        |
|                                                                                                                                                               |


Generate_series
=================
* testdb=# SELECT x, round(x::numeric) AS num_round, round(x::double precision) AS dbl_round 
FROM generate_series(-3.5, 3.5, 1) as x;

Money
========
* testdb=# SELECT '12.34'::float8::numeric::money;


Extraer horas de tantos minutos
---------------------------------
* testdb=# SELECT EXTRACT(hours from '80 minutes'::interval);

JSON
==========
* testdb=# SELECT '[1, 2, "foo", null]'::json;
* testdb=# SELECT '{"bar": "baz", "balance": 7.77, "active": false}'::json;
* testdb=# SELECT '{"foo": [true, "bar"], "tags": {"a": 1, "b": null}}'::json;

jsonb Containment and Existence
------------------------------------
* testdb=# SELECT '"foo"'::jsonb @> '"foo"'::jsonb;             #true
* testdb=# SELECT '[1, 2, 3]'::jsonb @> '[1, 3, 4]'::jsonb;     #false
* testdb=# SELECT '[1, 2, 3]'::jsonb @> '[1, 2, 2]'::jsonb;     #true
* testdb=# SELECT '{"foo": {"bar": "baz"}}'::jsonb @> '{"bar": "baz"}'::jsonb;  #false

Existe en un arreglo
-----------------------
* testdb=# SELECT '["foo", "bar", "baz"]'::jsonb ? 'bar';  #true
* testdb=# SELECT '{"foo": "bar"}'::jsonb ? 'bar';   #false no aplica para jsons

Datos existentes, buscar en json:
-----------------
{
    "guid": "9c36adc1-7fb5-4d5b-83b4-90356a46061a",
    "name": "Angela Barton",
    "is_active": true,
    "company": "Magnafone",
    "address": "178 Howard Place, Gulf, Washington, 702",
    "registered": "2009-11-07T08:53:22 +08:00",
    "latitude": 19.793713,
    "longitude": 86.513373,
    "tags": [
        "enim",
        "aliquip",
        "qui"
    ]
}

SELECT jdoc->'guid', jdoc->'name' FROM api WHERE jdoc @> '{"company": "Magnafone"}';

Otras
------
* testdb=# SELECT int4range(10, 20) @> 3;
* testdb=# SELECT numrange(11.1, 22.2) && numrange(20.0, 30.0);
* testdb=# SELECT upper(int8range(15, 25));
* testdb=# SELECT int4range(10, 20) * int4range(15, 25);
* testdb=# SELECT isempty(numrange(1, 5));
