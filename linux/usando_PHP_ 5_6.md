Usando PHP 5.6.39 
==================


Install
=======
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php5.6

-----------------------------------------

libapache2-mod-php5.6
php-pear
php-tcpdf
php-gettext

php5.6 php5.6-cli php5.6-common php5.6-curl php5.6-dev php5.6-gd php5.6-intl php5.6-gd php5.6-json php5.6-mbstring php5.6-mcrypt php5.6-mysql php5.6-opcache php5.6-readline php5.6-sqlite3 php5.6-xmlrpc php5.6-xml php5.6-xmlrpc php5.6-zip php5.6-imap php5.6-pspell php5.6-xsl php5.6-tidy php5.6-recode

phpmyadmin 4.5