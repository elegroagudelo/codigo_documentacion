127.0.0.1       localhost
127.0.0.7       appsoap.com     www.appsoap.com
127.0.1.1       elegro-MS-7817
127.0.1.2		educlases

sudo nano /etc/apache2/sites-available/educlases.com.conf
sudo ln -s /etc/apache2/sites-available/educlases.com.conf /etc/apache2/sites-enabled/educlases.com.conf
sudo systemctl restart apache2

<VirtualHost 127.0.0.9:80>
        ServerAdmin educlases.com
        DocumentRoot /var/www/html/educlases

        RedirectMatch 404 /\.git

    <Directory /var/www/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

#D isallow access to xmlrpc
<Files ~ "xmlrpc.php">
        Require all denied
</Files>


<Directory /var/www/html/phpmyadmin>
        Options FollowSymlinks
        AllowOverride all
        Require all granted
</Directory>



<VirtualHost 127.0.0.6:80>
    ServerName pkonva.com
    DocumentRoot /home/elegro/Projects/node/pkonva

    ProxyRequests Off
    ProxyPreserveHost On
    ProxyVia Full
    <Proxy *>
        Require all granted
    </Proxy>
    <Location /home/elegro/Projects/node/pkonva>
        ProxyPass http://127.0.0.6:8080
        ProxyPassReverse http://127.0.0.6:8080
    </Location>
    <Directory "/home/elegro/Projects/node/pkonva">
        Options FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>