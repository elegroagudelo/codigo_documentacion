APACHE 2 Debian
===============
Primero definimos la ruta donde queremos tener alojado el apache como ejemplo:      
* ~ mkdir /usr/local/apache2     

Validar la url del servidor si es valida:       
https://dlcdn.apache.org//httpd/httpd-2.4.51.tar.gz     

---
Dependencias en debian:       

* ~ sudo su
* ~ apt-get install libexpat-dev
* ~ apt-get install -y libpcre3-dev
* ~ apt-get install -y libcurl4-openssl-dev
* ~ apt-get install -y libcurl4-nss-dev
* ~ apt-get install -y libcurl4-gnutls-dev

Descargar los paquetes apache:       
* ~ wget https://dlcdn.apache.org//httpd/httpd-2.4.51.tar.gz --no-check-certificate
* ~ tar -xzvf  httpd-2.4.51.tar.gz
* ~ mv httpd-2.4.51 apache2
* ~ mv apache2 /opt/apache2
* ~ cd apache2/

Requiere de instalar el apr en /usr/local/apr       

Instalación:
-----------------

* ~ ./configure --with-prefix=/usr/local/apache2 --with-apr=/usr/local/apr --with-apr-util=/usr/local/apr/bin/apu-1-config --enable-so --with-mpm=worker

O tambien podemos usar el dir /usr/local/apache2/srclib de apr:      

* ~ ./configure --with-prefix=/usr/local/apache2 --with-apr=/usr/local/apache2/srclib/apr --with-apr-util=/usr/local/apache2/srclib/apr-util/bin/apu-1-config --enable-so --with-mpm=worker

* ~ make
* ~ make install
* ~ vim /usr/local/apache2/conf/httpd.conf
* ~ /usr/local/apache2/bin/apachectl -k start


Opciones:
-----
* --with-prefix=/usr/local/apache2
* --with-mpm=worker
* --with-port=90
* --enable-so


---

APACHE 2 CENTOS 6
==========================

Dependencias en centos:       

* ~ sudo yum -y install git 
* ~ sudo yum -y install apache-maven 
* ~ sudo yum -y install python-devel 
* ~ sudo yum -y install java-devel 
* ~ sudo yum -y install zlib-devel 
* ~ sudo yum -y install libcurl-devel 
* ~ sudo yum -y install openssl-devel 
* ~ sudo yum -y install cyrus-sasl-devel 
* ~ sudo yum -y install cyrus-sasl-md5 
* ~ sudo yum -y install apr-devel 
* ~ sudo yum -y install subversion-devel 
* ~ sudo yum -y install apr-util-devel
* ~ sudo yum -y install yum-utils
* ~ sudo yum -y install gcc
* ~ sudo yum -y install gcc-c++
* ~ sudo yum -y install pcre-devel
* ~ sudo yum -y install expat-devel
  

Instalar las dependencias APR       
--------------------------

Descarga de los paquetes para centos APR:     
* ~ wget https://dlcdn.apache.org/apr/apr-1.7.0.tar.gz --no-check-certificate
* ~ cd apr
* ~ touch libtoolT
* ~ ./configure --prefix=/usr/local/apache2/srclib/apr --bindir=/usr/local/bin 
----

* ~ wget https://dlcdn.apache.org/apr/apr-util-1.6.1.tar.gz --no-check-certificate
* ~ cd apr-util
* ~ ./configure --with-apr=/usr/local/apr 
----

* ~ wget https://dlcdn.apache.org/apr/apr-iconv-1.2.2.tar.gz --no-check-certificate
* ~ ./configure --with-apr=/usr/local/apache2/srclib/apr --prefix=/usr/local/apache2/srclib/apr-util
 

Comandos basicos
=============
* ~ /usr/local/apache2/bin/apachectl -k restart    
* ~ /usr/local/apache2/bin/apachectl stop
* ~ /usr/local/apache2/bin/apachectl start


Agregar el servicio
===========

* ~ ln -s /usr/local/apache2/bin/apachectl /etc/init.d/apache2      
* ~ chkconfig --add apache2      

-----
Crear directorios para apache2:        
Para debian se debe instalar en /etc/apache2        

Configurar el httpd.conf:       
* ~ vim /usr/local/apache2/conf/httpd.conf

Otros directorios  

---
|Directorios
----------------------------------------------|
| $~ mkdir /var/cache/apache2                 |
| $~ mkdir /var/cache/apache2/mod_cache_disk  |
| $~ mkdir /var/lib/apache2                   |                         
| $~ mkdir /var/log/apache2                   |
| $~ mkdir /var/www                           |
| $~ mkdir /var/www/html                      |
|                                             |


Agregar el usuario al grupo     
* ~ usermod -a -G www-data $USER_LOCAL
* ~ usermod -a -G httpd $USER_LOCAL

|Permisos:                              |Detalle:                             |
|---------------------------------------|-------------------------------------|
| $~ chown -R www-data:root /var/www/   | Debian usa www-data, y centos httpd |        
| $~ chmod -R 0755 /var/www/            |                                     |
|                                                                             |

