
CRONTAB CENTOS 6
======


Reemplazar tu crontab existente con tu archivo crontab personalizado        
* crontab /home/username/crontab      

Editar tu servidor crontab  
* crontab -e

Ver tu crontab  
* crontab -l

Eliminar tu crontab  
* crontab -r


Los siguientes ejemplos muestran lo que tú puedes agregar a un nuevo archivo para crear un cron job.   


10 16 * * * perl /home/username/bin/script.pl
Ejemplo 2: Esto corre el comando a las 2:00 AM PST/PDT el sábado, y la única salida son errores.
---

0 2 * * 6 sh /home/username/semanal/semanal.sh > /dev/null
Ejemplo 3: Esto corre a la medianoche del nuevo año (Enero 1) y no hay salida.
---

0 0 1 1 0 python /home/username/script.py >/dev/null 2>&1  
2>&1 es una redirección especial que envía el error estándar ("2>") al mismo lugar que la salida estándar (">" o "1>").
---


Ejemplo 4: Esto corre un script de PHP llamado cron.php empezando cada hora.
0 * * * * php /home/username/cron.php
---

Ejemplo 5: Esto corre el script local (localizado en DreamHost) cada 15 minutos.
*/15 * * * * /usr/local/php74/bin/php /home/username/script.php
---

Ejemplo 6: Esto corre un script especial (alojado en otro lugar) cada 30 minutos usando curl.
*/30 * * * * /usr/bin/curl -s https://example.com/send.php &> /dev/null
&>/dev/null es una abreviación para 1> /dev/null 2> &1. Redirecciona ambos descriptor de archivos 2 (STDERR) y descriptor 1 (STDOUT) a /dev/null.
---

Ejemplo 7: Esto corre el script local (alojado en DreamHost) cada 10 minutos.
*/10 * * * * /usr/local/php74/bin/php /home/username/script.php
---

Ejemplo 8: Esto usa wget para descargar un archivo al directorio llamado /cronfolder.
*/10 * * * */usr/bin/wget -P /home/username/cronfolder/ https://example.com/index.html
---