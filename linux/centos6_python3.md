
Instalar Python 3.10 En Centos 6

```
    $~ sudo bash
    $~ yum update
    $~ cd /usr/local
    $~ yum install openssl-devel bzip2-devel libffi-devel -y
    $~ yum groupinstall "Development Tools" -y
    $~ wget https://www.python.org/ftp/python/3.9.16/Python-3.9.16.tgz
    $~ tar -xzf Python-3.9.16.tgz
    $~ mv Python-3.9.16.tgz python.tgz
    $~ cd python
    $~ ./configure --prefix=/usr/local/python3 --enable-optimizations
    $~ make altinstall
    $~ python3 -V
```

* /usr/local/python3/bin/pip -V
* /usr/local/python3/bin/python -V
* /usr/bin/python3 -V


Opciones lanzadores Python3
----
```
    $~ ln -s /usr/local/python3/bin/python3.10 /usr/local/python3/bin/python3
    $~ ln -s /usr/local/python3/bin/python3.10 /usr/bin/python3
    $~ ln -s /usr/local/python3/bin/python3.10 /usr/local/python3/bin/python
    $~ ln -s /usr/local/python3/bin/python3.10-config /usr/local/python3/bin/python-config
    $~ ln -s /usr/local/python3/bin/pydoc3.10 /usr/local/python3/bin/pydoc
    $~ ln -s /usr/local/python3/bin/idle3.10 /usr/local/python3/bin/idle
```

Opciones lanzadores PIP 
----
```
    $~ ln -s /usr/local/python3/bin/pip3.10 /usr/bin/pip3
    $~ ln -s /usr/local/python3/bin/pip3.10 /usr/local/python3/bin/pip3
    $~ ln -s /usr/local/python3/bin/pip3.10 /usr/local/python3/bin/pip
```

```
    $~ yum -y install wget yum-utils gcc openssl-devel bzip2-devel libffi-devel
    $~ yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel
    $~ ./configure --prefix=/usr/local/python3 --enable-optimizations --with-system-ffi --with-computed-gotos --enable-loadable-sqlite-extensions
    $~ make -j "$(nproc)"
    $~ make altinstall
    $~ ldconfig
```

./configure --prefix=/usr/local/python3 --enable-optimizations


""" https://www.workaround.cz/howto-build-compile-install-latest-python-311-310-39-38-37-centos-7-8-9/ """


INIT
=====

# 👇️ if you have pip already installed
pip install --upgrade pip

# 👇️ if your pip is aliased as pip3 (Python 3)
pip3 install --upgrade pip

# 👇️ if you don't have pip in your PATH environment variable
python -m pip install --upgrade pip

# 👇️ if you don't have pip in your PATH environment variable
python3 -m pip install --upgrade pip

# 👇️ if you have easy_install
easy_install --upgrade pip

# 👇️ if you get a permissions error
sudo easy_install --upgrade pip

# 👇️ if you get a permissions error when upgrading pip
pip install --upgrade pip --user

# 👇️ upgrade pip scoped to the current user (if you get permissions error)
python -m pip install --user --upgrade pip
python3 -m pip install --user --upgrade pip

# 👇️ Installing directly from get-pip.py (MacOS and Linux)
curl https://bootstrap.pypa.io/get-pip.py | python

# 👇️ if you get permissions issues
curl https://bootstrap.pypa.io/get-pip.py | sudo python

# 👇️ alternative for Ubuntu/Debian
sudo apt-get update && apt-get upgrade python-pip

# 👇️ alternative for Red Hat / CentOS / Fedora
sudo yum install epel-release
sudo yum install python-pip
sudo yum update python-pip


INSTALL SQL
=====
# 👇️ in a virtual environment or using Python 2
pip install SQLAlchemy Flask-SQLAlchemy  --root-user-action=ignore 

# 👇️ for python 3 (could also be pip3.10 depending on your version)
pip3 install SQLAlchemy Flask-SQLAlchemy

# 👇️ if you get permissions error
sudo pip3 install SQLAlchemy Flask-SQLAlchemy

# 👇️ if you don't have pip in your PATH environment variable
python -m pip install SQLAlchemy Flask-SQLAlchemy

# 👇️ for python 3 (could also be pip3.10 depending on your version)
python3 -m pip install SQLAlchemy Flask-SQLAlchemy


OTHER
----

pip install --pre SQLAlchemy Flask-SQLAlchemy

pip3 install --pre SQLAlchemy Flask-SQLAlchemy

python -m pip --pre install SQLAlchemy Flask-SQLAlchemy

python3 -m pip --pre install SQLAlchemy Flask-SQLAlchemy


FILE SCIPT
----
import sqlalchemy

print(sqlalchemy.__version__)


TEST
---

python --version

python3 --version


# Try installing the package in a virtual environment

# 👇️ use correct version of Python when creating VENV
python3 -m venv venv

# 👇️ activate on Unix or MacOS
source venv/bin/activate

# 👇️ activate on Windows (cmd.exe)
venv\Scripts\activate.bat

# 👇️ activate on Windows (PowerShell)
venv\Scripts\Activate.ps1

# 👇️ Upgrade pip
pip install --upgrade pip

# 👇️ install SQLAlchemy in virtual environment
pip install SQLAlchemy


pip install SQLAlchemy --upgrade

pip3 install SQLAlchemy --upgrade

python3 -m pip install SQLAlchemy --upgrade



# Install the package with the --user option 

pip install SQLAlchemy --user

pip3 install SQLAlchemy --user

python3 -m pip install SQLAlchemy --user

sudo pip install SQLAlchemy

sudo pip3 install SQLAlchemy

sudo python3 -m pip install SQLAlchemy


# Try running pip install SQLAlchemy in verbose mode

pip install SQLAlchemy -vvv

pip3 install SQLAlchemy -vvv

python -m pip install SQLAlchemy -vvv


python3 -m pip install --user virtualenv

pip install --upgrade setuptools




./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl shared zlib
make -j "$(nproc)"
make test
make install 
