
Para installar android studio
----

Paquetes 32b requeridos
```
apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386    
```

Editar el baschrc del user local
```
export JRE_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
export ANDROID_HOME="$HOME/Android/Sdk"
export PATH="$PATH:$ANDROID_HOME/emulator"
export PATH="$PATH:$ANDROID_HOME/platform-tools"
export PATH="$PATH:$ANDROID_HOME/cmdline-tools/latest/bin"
export PATH="$PATH:$HOME/.android/android-studio/jre/bin"
export PATH="$PATH:$HOME/.android/gradle/bin"
```
