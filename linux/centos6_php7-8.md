Centos 6 y Debian con PHP
=======================
 
PHP7.3 Soportada en centos 6 
----------------
Se debe detener el servicio apache2:         

* ~ /usr/local/apache2/bin/apachectl stop

Dependencias Centos 6:       
------------

* ~ yum -y install libXpm-devel
* ~ yum -y install libpng-devel
* ~ yum -y install db4-devel.x86_64
* ~ yum -y install libjpeg-turbo-devel.x86_64
* ~ yum -y install freerdp-libs.x86_64
* ~ yum -y install libwebp-devel.x86_64
* ~ yum -y install freetype.x86_64
* ~ yum -y install db4-devel
* ~ yum -y install gmp-devel
* ~ yum -y install libc-client-devel
* ~ yum -y install libtidy libtidy-devel
* ~ yum -y install libmcrypt-devel.x86_64
* ~ yum -y install t1lib-devel.x86_64
* ~ yum -y install pcre-devel
* ~ yum -y install libxml2-devel
* ~ yum -y install libxslt-devel
* ~ yum -y install net-snmp-devel
* ~ yum -y install aspell-devel
* ~ yum -y install sqlite-devel
* ~ yum -y install postgresql-devel
* ~ yum -y install unixODBC-devel
* ~ yum -y install openldap-devel
* ~ yum -y install epel-release
* ~ yum -y install bzip2-devel
* ~ yum -y install perl-libintl.x86_64
* ~ yum -y install libicu-devel
* ~ yum -y install curl-devel

Configuramos:       
-----
* ~ vim ~/.baschrc 
* ~ PKG_CONFIG_PATH=/usr/lib64/pkgconfig  >> CENTOS 
* ~ PKG_CONFIG_PATH=/usr/lib/pkgconfig  >> DEBIAN 
* ~ export PKG_CONFIG_PATH

PHP7 Debian
=============

Dependencias Debian:
---------------

* ~ sudo apt-get install libonig-dev


Descargar los paquestes
------------------------

* ~ wget https://www.php.net/distributions/php-7.3.31.tar.gz --no-check-certificate
* ~ tar -xzvf php-7.3.31.tar.gz
* ~ mv php-7.3.31 /usr/local/php7

Instalación alojando en /usr/bin:
-------------
* ~ cd /usr/local/php7/
* ~ ./configure --prefix=/usr/local/php7.3 --bindir=/usr/bin --with-apxs2=/usr/local/apache2/bin/apxs --with-pdo-mysql --with-mysql --with-curl --with-mysqli --enable-phar --enable-pdo
 
* ~ ./configure --bindir=/usr/bin --with-apxs2=/usr/local/apache2/bin/apxs --with-pdo-mysql --with-mysql --with-curl --with-mysqli --enable-phar --enable-pdo
 
* ~ make
* ~ make install

* ~ /usr/local/apache2/bin/apachectl restart


Sin alojar en /usr/bin      
----------------------
./configure --bindir=/usr/local/php5 --with-pdo-mysql --with-mysql --with-mysqli --with-curl --enable-phar --enable-pdo             
 
wget https://prototype.php.net/distributions/php-5.6.40.tar.gz --no-check-certificate       
tar -xzvf php-5.6.40.tar.gz         
mv php-5.6.40 php5          
cd php5         
chmod -R 0755 ./*               
apt-get install libcurl4-gnutls-dev         
apt-get install libcurl4-openssl-dev    
--with-curl=/usr/include/x86_64-linux-gnu/curl/         

Error Openssl
-----
curl -O https://www.openssl.org/source/old/1.0.2/openssl-1.0.2u.tar.gz      
tar xf openssl-1.0.2u.tar.gz        
cd openssl-1.0.2u       
./config --prefix=/opt/openssl-1.0.2u       
make        
make instal        

./configure --prefix=/opt/php-5.6.40 --with-openssl=/opt/openssl-1.0.2u     
make        
make install        
 
======

Todos los paquetes
-------
* ~ ./configure 
----------
|Opciones
|-------- 
|--prefix=/usr/share/php5 
|--datadir=/usr/share/php5 
|--mandir=/usr/share/man 
|--bindir=/usr/bin/php5 
|--includedir=/usr/include/php5 
|--sysconfdir=/etc/php5/apache2 
|--with-config-file-path=/etc/php5/apache2 
|--with-config-file-scan-dir=/etc/php5/conf.d 
|--enable-bcmath 
|--with-curl=shared,/usr 
|--with-mcrypt=shared,/usr 
|--enable-cli 
|--with-Gd 
|--with-mysql 
|--with-mysqli 
|--enable-libxml 
|--enable-session 
|--enable-xml 
|--enable-simplexml 
|--enable-filter 
|--enable-inline-optimization 
|--with-jpeg-dir 
|--with-png-dir 
|--with-zlib 
|--with-bz2 
|--with-curl 
|--enable-exif 
|--enable-soap 
|--with-pic 
|--disable-rpath 
|--disable-static 
|--enable-shared 
|--with-gnu-ld 
|--enable-mbstring

Enlazar con Apache2
------------------- 
Se requiere de usar el --with-apxs2
* ~ ./configure --bindir=/usr/bin --with-apxs2=/usr/local/apache2/bin/apxs --with-pdo-mysql --with-mysql --with-curl --with-mysqli  --enable-phar --enable-pdo


Prueba Exitosa PHP 5.6
--------
* ~ ./configure --prefix=/usr/local/php5 --datadir=/usr/local/php5 --mandir=/usr/share/man --bindir=/usr/bin/php5 --includedir=/usr/local/php5/include --with-config-file-scan-dir=/usr/local/php5/conf.d --enable-bcmath --with-curl=shared,/usr --with-mcrypt=shared,/usr --enable-cli --with-Gd --with-mysql --with-mysqli --enable-libxml --enable-session --enable-xml --enable-simplexml --enable-filter --enable-inline-optimization --with-jpeg-dir --with-png-dir --with-zlib --with-bz2 --with-curl --enable-exif --enable-soap --with-pic --disable-rpath --disable-static --enable-shared --with-gnu-ld --enable-mbstring



Prueba PHP 7.3
----------------------- 
* ~ ./configure --prefix=/usr/local/php7 --datadir=/usr/local/php7 --mandir=/usr/share/man --bindir=/usr/bin/php7 --includedir=/usr/local/php7/include --with-config-file-path=/usr/local/php7  --with-config-file-scan-dir=/usr/local/php7/conf.d --with-gd --with-mysqli --with-jpeg-dir --with-png-dir --with-zlib --with-bz2 --with-curl --with-pic --with-sqlite3 --with-openssl --without-libzip --enable-bcmath --enable-cli --enable-exif --enable-intl --enable-soap --enable-libxml --enable-session --enable-xml --enable-simplexml --enable-filter --enable-inline-optimization --enable-shared --with-gnu-ld --with-pdo-sqlite --enable-mysqlnd --with-mysql-sock=/var/lib/mysql/mysql.sock --enable-mbstring --enable-fpm --enable-xmlwriter --enable-zip --enable-xmlreader  --enable-opcache --disable-phpdbg --disable-phpdbg-webhelper --disable-rpath --disable-static


MCRYPT PHP 7.3
=============

wget https://sourceforge.net/projects/mcrypt/files/Libmcrypt/2.5.8/libmcrypt-2.5.8.tar.gz/download --no-check-certificate
tar -xzvf libmcrypt-2.5.8.tar.gz
mv libmcrypt-2.5.8 libmcrypt
./configure --with-config-file-path=/usr/local/php7/php.ini
make
make install
 

CENTOS PHP 7.3
------
./configure --prefix=/usr/local/php7 --datadir=/usr/local/php7 --mandir=/usr/share/man --bindir=/usr/bin/php7 --includedir=/usr/include --with-config-file-path=/usr/local/php7  --with-config-file-scan-dir=/usr/local/php7/conf.d --with-gd --with-mysqli --with-jpeg-dir --with-png-dir --with-zlib --with-bz2 --with-curl --with-pic --with-sqlite3 --with-openssl --without-libzip --enable-bcmath --enable-cli --enable-exif --enable-intl --enable-soap --enable-libxml --enable-session --enable-xml --enable-simplexml --enable-filter --enable-inline-optimization --enable-shared --with-gnu-ld --with-pdo-sqlite --enable-mysqlnd --with-mysql-sock=/var/lib/mysql/mysql.sock --enable-mbstring --enable-fpm --enable-xmlwriter --enable-zip --enable-xmlreader  --enable-opcache --disable-phpdbg --disable-phpdbg-webhelper --disable-rpath --disable-static
 
 
DEBIAN PHP 8.2

apt-get install -y libwebp-dev
apt-get install -y libxpm-dev
apt-get install -y libjpeg62-dev 
apt-get install -y libpng-dev 
apt-get install -y libfreetype6-dev
apt-get install re2c
apt-get install libsqlite3-dev sqlite3 -y
apt-get install libonig-dev -y
apt-get install bison libcurl4-openssl-dev -y

--with-png-dir=/usr/include/
--with-jpeg-dir=/usr/include/
--with-freetype-dir=/usr/include/

------
./configure --prefix=/usr/local/php8 --datadir=/usr/local/php8 --mandir=/usr/share/man --bindir=/usr/bin/php8 --includedir=/usr/include --with-config-file-path=/usr/local/php8  --with-config-file-scan-dir=/usr/local/php8/conf.d --with-apxs2=/usr/local/apache2/bin/apxs --with-pdo-mysql --with-mysqli --with-zlib --with-bz2 --with-curl --with-pic --with-sqlite3 --with-openssl  --enable-bcmath --enable-cli --enable-exif --enable-intl --enable-soap --enable-session --enable-xml --enable-simplexml --enable-filter --enable-shared --with-gnu-ld --with-pdo-sqlite --enable-mysqlnd --with-mysql-sock=/var/lib/mysql/mysql.sock --enable-mbstring --enable-fpm --enable-xmlwriter --enable-xmlreader  --enable-opcache --disable-phpdbg --disable-phpdbg-webhelper --disable-rpath --disable-static
 

========

DEBAIN 7.4 
--------------
 ./configure --enable-fpm --prefix=/usr/local/php7 --datadir=/usr/local/php7 --bindir=/usr/bin/php74 --includedir=/usr/include/ --enable-cli --enable-opcache --enable-intl --enable-mbstring --enable-zip --enable-bcmath  --enable-pcntl  --enable-ftp  --enable-exif  --enable-calendar  --enable-sysvmsg  --enable-sysvsem --enable-sysvshm  --enable-wddx  --enable-gd-jis-conv --enable-mysqlnd  --with-curl  --with-iconv  --with-openssl  --with-pdo-mysql --with-gettext --with-zlib --with-bz2 --with-gd --with-freetype --with-png --with-jpeg --with-webp --with-xpm --with-mysql-sock=/var/run/mysqld/mysqld.sock 


Resultado de instalación php5.6 con /usr/bin/php
---------
|       |
|-------|
|Installing shared extensions:     /usr/local/lib/php/extensions/no-debug-non-zts-20131226/
|Installing PHP CLI binary:        /usr/local/php5/bin/
|Installing PHP CLI man page:      /usr/local/php/man/man1/
|Installing PHP CGI binary:        /usr/local/php5/bin/
|Installing PHP CGI man page:      /usr/local/php/man/man1/
|Installing build environment:     /usr/local/lib/php/build/
|Installing header files:           /usr/local/include/php/
|Installing helper programs:       /usr/local/php5/bin/
|program: phpize
|program: php-config
|Installing man pages:             /usr/local/php/man/man1/
|page: phpize.1
|page: php-config.1
|Installing PEAR environment:      /usr/local/lib/php/
|[PEAR] Archive_Tar    - already installed: 1.4.14
|[PEAR] Console_Getopt - already installed: 1.4.3
|[PEAR] Structures_Graph- already installed: 1.1.1
|[PEAR] XML_Util       - already installed: 1.4.5
|[PEAR] PEAR           - already installed: 1.10.13
|Wrote PEAR system config file at: /usr/local/etc/pear.conf
|You may want to add: /usr/local/lib/php to your php.ini include_path
|/usr/local/php5/build/shtool install -c ext/phar/phar.phar /usr/local/php5/bin
|ln -s -f phar.phar /usr/local/php5/bin/phar
|Installing PDO headers:           /usr/local/include/php/ext/pdo/



Comando Rsync
==========
Sincroniza dir_origen/  >> Todo lo que se sta dentro del dir_origen/        
Donde el paramtro (-a) comando recursivo de rsync.       

* ~ sudo rsync -a user@server_origen:/dir_origen/  user@server_destino:/dir_destino 




Tener multiples versiones de php en Debian
-----
Se requiere de:     
* apt-get install libapache2-mod-fcgid -y   
* apt-get install software-properties-common -y 
* add-apt-repository ppa:ondrej/php 
* sudo apt install $(apt list --installed | grep php5.6- | cut -d'/' -f1 | sed -e 's/5.6/7.4/g')        
* sudo a2enmod actions alias proxy_fcgi fcgid   
* sudo service apache2 restart




wget https://www.php.net/distributions/php-7.4.8.tar.gz --no-check-certificate

wget https://www.php.net/distributions/php-8.2.4.tar.gz --no-check-certificate

rpm -Uvh libxml2-*
rpm -ivh libxml2-devel-2.9.1-5.el7_0.1.x86_64.rpm 

./configure --prefix=/usr/local/php7 --datadir=/usr/local/php7 --mandir=/usr/share/man --bindir=/usr/bin/php7 --includedir=/usr/local/php7/include --with-config-file-path=/usr/local/php7  --with-config-file-scan-dir=/usr/local/php7/conf.d --with-pdo_pgsql --with-gd2--with-pgsql --with-zlib --with-bz2 --with-curl --with-pic --with-sqlite3 --with-openssl  --with-gnu-ld --with-pdo-sqlite --without-analyzer --without-libzip --with-pgo--enable-pgsql --enable-mbstring --enable-fpm --enable-xmlwriter --enable-xmlreader  --enable-opcache --enable-cli --enable-exif --enable-intl --enable-soap --enable-session --enable-xml --enable-simplexml --enable-filter --enable-shared--enable-openssl --disable-phpdbg --disable-phpdbg-webhelper --disable-rpath --disable-static


./configure 
--prefix=/usr/local/php7 
--datadir=/usr/local/php7 
--mandir=/usr/share/man 
--bindir=/usr/bin/php7 
--includedir=/usr/local/php7/include 
--with-config-file-path=/usr/local/php7  
--with-config-file-scan-dir=/usr/local/php7/conf.d 
--with-pdo_pgsql 
--with-gd2
--with-pgsql 
--with-zlib 
--with-bz2 
--with-curl 
--with-pic 
--with-sqlite3 
--with-openssl  
--with-gnu-ld 
--with-pdo-sqlite 
--without-analyzer 
--without-libzip 
--with-pgo
--enable-pgsql 
--enable-mbstring 
--enable-fpm 
--enable-xmlwriter 
--enable-xmlreader  
--enable-opcache 
--enable-cli 
--enable-exif 
--enable-intl 
--enable-soap 
--enable-session 
--enable-xml 
--enable-simplexml 
--enable-filter 
--enable-shared
--enable-openssl 
--disable-phpdbg 
--disable-phpdbg-webhelper 
--disable-rpath 
--disable-static