Tras Instalar Debian
====================
Se requiere de iniciar la actualizacion del sistema.    

* ~ apt update && apt upgrade
* ~ apt install sudo
* ~ sudo gpasswd -a  $USUARIO
* ~ sudo reboot

Instalar Gnome
=================
Con ello se instala el nucleo de gnome y al reboot se cara la interface  

* ~ sudo apt-get install gnome-core-xorg

Configurar el sources list
==========================
* ~ sudo apt-get install vim
* ~ sudo vim /etc/apt/sources.list

Incluir en el sources.list
-------------------------
Las siguienets lineas   
* deb https://security.debian.org/jessie/updates main contrib non-free
* deb-src https://security.debian.org/jessie/updates main contrib non-free

Mirror
------------
* deb https://ftp.mx.debian.org/debian/jessie main contrib non-free
* deb-src https://ftp.mx.debian.org/debian/jessie main contrib non-free


Install KDE - Desktop
----------------------

Modo basico del kDE 
* ~ apt-get install kde-plasma-desktop 

Modo estandar de kde    
* ~ apt-get install kde-standard

Modo full de kde    
* ~ apt-get install kde-full


Remover el KDE
================
* ~ sudo apt-get remove --purge kubuntu-desktop kde-standard
* ~ sudo apt-get remove --purge language-pack-kde-es
* ~ sudo apt install gdm
* ~ sudo dpkg-reconfigure gdm
* ~ sudo apt-get install usplash-theme-debian
* ~ sudo update-alternatives --config default.plymouth
* ~ sudo update-alternatives --config usplash-artwork.so
* ~ sudo update-initramfs -u
* ~ sudo usplash -c
* ~ sudo apt-get install plymouth-theme
* ~ sudo apt-get install galternatives


Instalación De Composer en Debian
====================================

* ~ sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
* ~ sudo php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Instalacion verificada'; } else { echo 'Instalacion corrupta'; unlink('composer-setup.php'); } echo PHP_EOL;"
* ~ php composer.phar
* ~ mv composer.phar /usr/local/bin/composer
* ~ composer
* ~ php -r "unlink('composer-setup.php');"


Install Cinnamon Desktop 
========================
Environment on Debian 11 Using Tasksel  
* ~ sudo apt install tasksel
* ~ sudo tasksel
* ~ sudo reboot


Install Cinnamon Desktop Environment en Debian 11 Using APT
--------------------------------------------------------------
* ~ sudo apt -y install task-cinnamon-desktop
* ~ sudo reboot


Install Cinnamon Desktop en Ubuntu  
------------------------------------
* ~ sudo apt install cinnamon-desktop-environment


Para casos en que no posee el repositorione Ubuntu   
--------------------------------------------
* ~ sudo add-apt-repository ppa:embrosyn/cinnamon
* ~ sudo apt update && sudo apt install cinnamon