export JAVA_HOME="/usr/lib/jvm/jdk-11.0.12"
export PATH="$PATH:$JAVA_HOME/bin/"
export CATALINA_HOME="/var/tomcat/apache-tomcat-8.5.61"
export CATALINA_BASE="/var/tomcat/apache-tomcat-8.5.61"
export PATH="$PATH:$CATALINA_HOME/bin/"
export JRE_HOME="/usr/java/openjdk/open-jdk-8/"
export ANDROID_HOME="$HOME/.android/android-studio"
export PATH="$PATH:$ANDROID_HOME/bin/"
export PATH="$HOME/.android/platform-tools:$PATH"
export PATH="$HOME/.android/tools:$PATH"
export PATH="$HOME/.android/gradle:$PATH"
export GEM="$HOME/.rbenv/versions/2.7.0/bin/gem"
export PATH="$PATH:$HOME/.rbenv/versions/2.7.0/bin/"
export MAVEN_HOME="/usr/local/apache-maven/3.8.2"
export PATH="$MAVEN_HOME/bin/:$PATH"


#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"

PKG_CONFIG_PATH="/usr/lib/pkgconfig"
export PKG_CONFIG_PATH

export PATH="$PATH:/usr/local/mysql/bin"