Extenciones de PHP v5.6.39
===================

1. php-pear: 
PEAR es un sistema de distribución y estructura para componentes PHP reutilizables.

2. php5-opcache:
OPcache mejora el rendimiento de PHP almacenando el código de bytes de un script precompilado en la memoria compartida, eliminando así la necesidad de que PHP cargue y analice los script en cada petición.
Esta extensión está incluída en PHP 5.5.0 y posteriores, y está » disponible en PECL para las versiones 5.2, 5.3 y 5.4 de PHP.

3. php5-curl:
PHP soporta libcurl, una biblioteca creada por Daniel Stenberg que permite conectarse y comunicarse con diferentes tipos de servidores y diferentes tipos de protocolos. Actualmente, libcurl admite los protocolos http, https, ftp, gopher, telnet, dict, file y ldap. libcurl también admite certificados HTTPS, HTTP, POST, HTTP PUT, subidas mediante FTP (también se puede hacer con la extensión FTP de PHP), subidas basadas en formularios HTTP, proxies, cookies, y autenticación usuario+contraseña.
usar en PHP 5.4.

4. php5-gd:
PHP no está limitado a crear únicamente salidas HTML. También se puede usar para crear y manipular ficheros de imágenes en una variedad de diferentes formatos de imagen, incluyendo GIF, PNG, JPEG, WBMP y XPM. Aún más práctico es que PHP puede transferir flujos de imagen directamente al navegador. Necesitará compilar PHP con la biblioteca de funciones de imágenes GD para que esto funcione. GD y PHP también pueden requierir otras bibliotecas, dependiendo de los formatos de imagen con los que se quiera trabajar.

5. php5-imagick:
Imagick es una extensión nativa de php para crear y modificar imágenes utilizando la API ImageMagick.
ImageMagick es un conjunto de software para crear, editar y componer imágenes de mapa de bits. Puede leer, convertir y escribir imágenes en una variedad de formatos (más de 100) incluyendo DPX, EXR, GIF, JPEG, JPEG-2000, PDF, PhotoCD, PNG, Postscript, SVG y TIFF.

6. php5-json:
Esta extensión implementa el formato de intercambio de datos » JavaScript Object Notation (JSON) La decodificación se realiza mediante el análisis sintáctico basado en la implementación de JSON_checker por Douglas Crockford.

7. php5-mcrypt:
Esta es una interfaz para la biblioteca mcrypt, que admite una gran variedad de algoritmos de bloques tales como DES, TRipleDES, Blowfish (predeterminado), 3-WAY, SAFER-SK64, SAFER-SK128,TWOFISH, TEA, RC2 y GOST en los modos de cifrado CBC, OFB, CFB y ECB. Además, admite RC6 e IDEA, que son considerados "no libres". CFB/OFB son de 8 bits por defecto. Obsoleto desde php7.1

8. php5-memcache:
» memcached es un sistema de alto rendimiento para el almacenamiento de objetos en caché de memoria distribuida, genérico por naturaleza, aunque pensado principalmente para acelerar aplicaciones web dinámicas aliviando la carga de bases de datos.

9. php5-mysql:

10. php5-mysqli:

11. php5-pdo:

12. php5-pgsql:

13. php5-pspell:

14. php5-readline:

15. php5-recode:

16. php5-sqlite3:

17. php5-tidy:

18. php5-xmlrpc:

19. php5-xsl:

20. php5-dev:

21. php5-imap:

22. php5-common:

23. php5-ps:


