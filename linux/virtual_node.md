< VirtualHost *:80>
DocumentRoot "/Library/WebServer/Documents/www.mysite.com"
ServerName local.www.mysite.com
ServerAlias local.www.mysite.com
ProxyPass /src !
ProxyPass / http://local.www.mysite.com:3000/
ProxyPassReverse / http://local.www.mysite.com:3000/
< /VirtualHost>
