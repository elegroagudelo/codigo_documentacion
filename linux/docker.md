DOCKER
===========

Instalación de docker en debian
-----------------------------------
* ~ sudo su     
* ~ apt-get update      
* ~ apt-get -y install apt-transport-https       
* ~ apt-get -y install ca-certificates      
* ~ apt-get -y install curl         
* ~ apt-get -y install software-properties-common
* 
* ~ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
* ~ add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"


Se agregará una línea al archivo de repositorios adicionales.       
deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable     

* ~ apt-get update
* ~ apt-get -y  install docker-ce 
* ~ apt-get -y  install docker-compose
* ~ usermod -aG docker $USER
* ~ docker --version


Documentacion de Docker
-------------------------------

* ~ sudo docker ps       
-- Listar los iniciados y corriendo


* ~ sudo docker ps -a   
-- listar los ps apagados

Muestra los procesos y contenedores de docker que no estan corriendo

* ~ docker container ls        
-- Muestra los contenedores que estan corriendo


Modo interactivo 
---------------------
-i, --interactive       
-t, --tty terminal     
* ~ docker run -i -t centos:6.10 

----

Buscador de lista img docker 
---

* ~ docker pull nombre_imagen
* ~ docker pull nombre_imagen:version          >> ejemplo centos:6.10
* ~ docker run -t -i id_imagen_ o nombre_imagen
* ~ docker run -i -t centos:6.10 
* ~ docker inspect CONTAINER_ID >> muestra todo lo que posee el container
* ~ docker inspect NAMES >>  procesa igual que el CONTAINER_ID

-----

Otros contenedores
---
* ~ docker run --name contenedor_pro contenedor_dev   >> cambia el nombre deun contenedor
* ~ docker rename  contenedor_dev contenedor_pro >> camabia los NAME


----

Borra contenedores
----

* ~ docker rm CONTAINER_ID  <!--  uno a uno -->
* ~ docker container prune  <!-- borrar todos los apagados -->


Preparar proyecto angular 
------------
Creamos el proyecto angular, e ingresamos a la carpeta del mismo.       
Creamos un archivo Dockerfile con:      
```
FROM node

WORKDIR ./
COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]

```

Generar la imagen
--------------------
* ~ sudo docker build -t api-docker-p3-angular  .
