SENTORA CORE

PLEASE NOTE:
Ninguno de estos servicios (o similares) puede estar instalado en su servidor ANTES de instalar Sentora, de lo contrario la instalación fallará. Sentora no usa ni trabaja con Nginx! Usted es libre de probarlo, pero no lo apoyamos!

Sentora - (Current Version)
 MySql -5.5.29
Apache - 2.4.3
PHP -5.3.20
Bind - 9.9.2-P1
 phpMyAdmin - 3.5.8.1*
RoundCube - 0.9.2*
Dovecot- 2.0.9
Postfix- 2.6.6
proFTPd - 1.3.3g

Estos paquetes son fácilmente actualizados a sus versiones actuales. Consulte nuestros foros de soporte para obtener más información.
ATENCIÓN: Sentora no es compatible con Microsoft Windows en este momento. Puede intentar instalar / actualizar manualmente Sentora y sus dependencias, pero algunos cambios en el núcleo de Sentora pueden hacer que Sentora se vuelva inestable o incluso falla en su funcionamiento).

Sistema operativo soportado:
El instalador de Sentora admite oficialmente los siguientes sistemas operativos:
    1. CentOS 6
    2. CentOS 7
    3. Ubuntu 12.04
    4. Ubuntu 14.04.
Sistema operativo no compatible:
Algunas distribuciones derivadas de los sistemas operativos compatibles pueden funcionar. Usted es libre de usar Sentora en sistemas no soportados bajo su propio riesgo. El equipo de desarrollo de Sentora no ofrecerá soporte para distribuciones no soportadas.

Comprobación previa a la instalación:
El instalador está diseñado para ser lo más simple posible. Verificará que todos los requisitos obligatorios se cumplan.
Recomendamos una instalación de distribución mínima.
No puede haber otro panel de administración del servidor instalado antes de instalar Sentora.
No puede haber otros servicios web (Ejemplos: Nginx, MySQL, Apache, PHP, Servidor FTP, Servidor de Correo, etc.) instalados antes de instalar Sentora.

Requerimientos adicionales:
Debe tener un nombre de dominio registrado y un SUB-DOMAIN apuntando a la IP de su servidor,
El dominio principal se utilizará para configurar el servicio de correo electrónico (como webmaster@MAIN-DOMAIN.TLD)
El subdominio se dedicará al acceso al panel Sentora.
Nota:
Puede utilizar un dominio gratuito o, alternativamente, comprar uno de GoDaddy.com.
Puede ejecutar una versión de funcionalidad limitada de Sentora sin DNS. Lee mas

You MUST have opened and forwarded the required ports in your modem or router, and on the server firewall.

Una vez que se cumplan todas las condiciones anteriores, puede comenzar a instalar Sentora en su servidor.
Si alguno de los términos o conceptos anteriores no parecen familiares, le recomendamos que dedique algo de tiempo a leer más sobre servidores web y alojamiento web ANTES de intentar instalar Sentora.

Installation configuration.
El instalador le hará algunas preguntas, algunas son MUY importantes. Esta página tratará de darle alguna ayuda sobre ellos.
El instalador de Sentora se ha mejorado ligeramente desde los primeros usados por Zpanel
Seleccione su TimeZone
Seleccione su país desde la interfaz pseudo-gráfica (Ubuntu) o listas (CentOs).

Subdominio Sentora
Es el nombre de dominio que utilizará para acceder a su panel de control después de la instalación (URL del panel).
Debe ser un subdominio del dominio principal anterior. Introduzca el nombre completo del subdominio.
El instalador comprobará la validez del subdominio: DEBE ser configurado en DNS (y propagado) ANTES de ejecutar el instalador.

SOLO PARA ADMINISTRACIÓN AVANZADA: si desea instalar un servidor local sólo para uso local (servidor de prueba), no podrá accederlo por dominio hasta que configure un dns local (normalmente en las tablas de enrutador).
Puede ingresar cualquier dominio principal y subdominio que desee (pero recuerde que un dominio principal tiene al menos un punto).
Posteriormente, puede cambiar el subdominio Cambiar Sentora del dominio del panel.

IP pública del servidor
El instalador propondrá la IP pública que encontró de un comando enviado al sitio web de Sentora API. Deberia estar bien.
Confirmación:
Si tiene una advertencia antes de la pregunta de confirmación, Sentora NO TRABAJARÁ, en parte completamente, como servidor web público.
Se aconseja asegurarse de que REALMENTE desea instalar Sentora con esta configuración. Confirme sólo si sabe por qué y bajo su propio riesgo, se le aconseja y no seremos capaces de ayudarle efectivamente.

