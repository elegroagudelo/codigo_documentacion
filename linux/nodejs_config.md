NODE JS
==========
Usando el módulo Mod_Proxy de Apache (no SSL)
Habilita los mod_proxy y mod_proxy_html módulos de Apache. 
Deben estar disponibles por defecto, así que simplemente habilítalos con el comando a2enmod.

* ~ sudo a2enmod
* ~ proxy proxy_http
Para obtener más información sobre mod_proxy, consulte la documentación oficial de apache. 
Condición: Debes haber iniciado tu aplicación de nodos y ha superado con éxito http://mydomian.com:3000/

VirtualHost
---------

```
<VirtualHost *: 80>
    NombreServidor midominio.com
    ServerAlias ​​www.midominio.com
    DocumentRoot / var / www / nodeapp /
    Opciones -Indexes
    ErrorDocument 503 /check.html

    ProxyRequests en
    ProxyPass /check.html!
    ProxyPass / http://midominio.com:3000/
    ProxyPassReverse / http://midominio.com:3000/
<VirtualHost>
```

Hemos creado el host virtual para el dominio mydomain.com en el que queremos ejecutar una aplicación de nodo.
Hemos habilitado el módulo de proxy en apache y check.html no será procesado, sino que será servido por el servidor Apache como una página 'normal' (el! Significa que no se enviará, para más información sobre el proxypass directiva ver la documentación oficial de apache.
Al utilizar la configuración de proxy anterior en un archivo vhost, mágicamente obtendrá las respuestas de su servidor de nodo ejecutándose en http: // localhost: 3000 / simplemente ejecutando el nombre de dominio mydomain.com .


Instalar nodejs en Produccion
------------------------
sudo apt-get install -y nodejs

Verion 9
* ~ curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
* ~ sudo apt-get install -y nodejs
* ~ sudo chown -R $USER /usr/lib/node_modules


* ~ sudo apt-get install -y build-essential
Configuración Manula
* ~ Editar archivo httpd.conf de apache2. En las lineas seguidas de:

```
    ServerRoot "C:/wamp/apache"
    ProxyPass /node http://localhost:3000/
```
Tambien debe buscar la linea donde se encuentra el mod_proxy.so y descomentar (#)

```
    LoadModule proxy_module modules/mod_proxy.so
    LoadModule proxy_http_module modules/
```
Luego se requiere de reiniciar el servidor apache

----

Instalar nodejs para Desarrollo
--------------------------------
Descargar los paquetes NVM por medio de curl     
Primero revisa que si este disoponible      
* ~  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh

Y luego si usa el bash      

* ~ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
* ~ source ~/.bashrc
* ~ nvm list-remote

---
* ~ nvm install v10.6.0   --o tambie se puede instalar--  ~ nvm install lts/erbium
---

* ~ nvm list
* ~ nvm use v10.6.0  --o tambien--  ~ nvm use stable
* ~ node -v

