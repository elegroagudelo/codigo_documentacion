MYSQl 5.5 CENTOS 6
====================
Iniciamos con los comandos de reconocimiento del sistema centos:     
* ~ uname -a
* ~ cat /etc/issue
* ~ yum -y install libaio1
* ~ yum install apparmor-profiles
* ~ groupadd mysql
* ~ useradd -r -g mysql -s /bin/false mysql
* ~ ls -la mysql*

Conseguimos la version requerida de centos      

Las Fuentes
---------
* ~ wget https://cdn.mysql.com/archives/mysql-5.5/mysql-5.5.62-linux-glibc2.12-x86_64.tar.gz

Se descomprime y se mueve a su destino      
* ~ tar -xzvf mysql-5.5.62-linux-glibc2.12-x86_64.tar.gz
* ~ mv mysql-5.5.62-linux-glibc2.12-x86_64/ mysql

---         
Se crean los permisos de usuario mysql      
* ~ chown -R mysql:mysql mysql/
* ~ chown mysql:mysql mysql/*

---     
Ingresamos al paquete descargado e instalamos     
* ~ cd mysql/
* ~ ./scripts/mysql_install_db --user=mysql 
* ~ bin/mysqld_safe --user=mysql &

---     
Creamos el servicio de mysql en linux       
* ~ cp support-files/mysql.server /etc/init.d/mysql.server
* ~ cp support-files/my-medium.cnf /etc/my.cnf

---     
Se debe crear el enlace simbolico de acceso al socket mysql     
* ~ ln -s /tmp/mysql.sock /var/lib/mysql/mysql.sock
* ~ ./bin/mysqladmin -u root password '$CLAVE'


Inicializamo el servicio   
----     
* ~ bin/mysqld --initialize --user=mysql --secure-file-priv=''
* ~ systemctl status mysql.service
 
./bin/mysql
 
Simbolicos
---------
ln -s /usr/local/mysql/bin/mysql /usr/bin/mysql     
ln -s /var/lib/mysql/mysql.sock /var/run/mysqld/mysqld.sock
 
----        
Creamos el usuario  manager y admin
---------------------
Con los privilegios requeridos      

CREATE USER 'manager'@'%' IDENTIFIED BY '$CLAVE';
GRANT ALL PRIVILEGES ON * . * TO 'manager'@'%';
FLUSH PRIVILEGES;
 
CREATE USER 'admin'@'localhost' IDENTIFIED BY '$CLAVE';
GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost';
FLUSH PRIVILEGES;
 
CREATE USER root@'%' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON * . * TO root'@'%';
FLUSH PRIVILEGES;
