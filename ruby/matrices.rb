#arreglo = es un contenedor de datos
arreglo = []
#La matriz se diferencia por la catidad de dimenciones
#Los arreglos deben tener la misma dimencion en los arreglos internos
#En una matriz todos los elementos deben ser numeros
_matriz[0] = [[0, 1, 2], [3, 4,5]]

#Declarar una matrix requiere de importar la clase
require 'matrix'
_matrix = Matrix[[0, 1, 2], [3, 4,5]]
print _matrix

_matrix.each do |n|
	puts n
end

#la diagonal de una matrix
#[
#  1 8 3 
#  5 5 2
#  5 5 3
#]
_matrix_new = Matrix[[1,8,3],[5,5,2],[5,5,3]]
_matrix_new.each(:diagonal) do |n|
	puts n
end
#Validar si una matrix es cuadrada
puts _matrix_new.diagonal?