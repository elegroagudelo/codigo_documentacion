##arreglos en ruby
#definir aqui un arreglo
arreglo = []
arreglo = Array.new(7)
arreglo = %w[10 1 23 12 16 4 5]


#multiplicar un arreglo
puts arreglo * 3
puts arreglo * "-" #Se genera un join a una cadena

#join en los arreglos de cadena los convierte en string 
puts arreglo.join(",");
puts arreglo.join(",").class.name #inspecciona el tipo de dato

puts arreglo.sort
puts arreglo.sort.reverse #orden de reverse

puts arreglo.reverse #mostrar en sentido contrario

puts arreglo.include?(23) #buscar un valor en un array #rettorna boolean (true || false)
puts arreglo.fist #primero
puts arreglo.last #ultimo
puts arreglo.uniq #el mismo arreglo sin elementos repetidos
puts arreglo.sample #retorna un valor de forma aleatoria


#rangos e iterador
rango_numeros  = (1..10)  #crear un rango del 1 al 10

(1..10).each do |valor| 
puts valor  # recorre los datos del arreglo
end

#intervalos del iterador de rango
(1..10).step(2).each do |valor| 
puts valor  # recorre los datos del arreglo de dos en dos
end

#intervalos alfanumericos 
rango_alfa = ('a'..'z')
puts rango_alfa

#convert rango a un arreglo
array_rango = rango_alfa.to_a
print array_rango

# Los rangos comparten muchos de los metodos de los array

puts  rango_alfa.max
puts  rango_alfa.min

#rangos inteligentes
print ("ma".."mz").to_a
