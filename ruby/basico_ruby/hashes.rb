#hashes = es una coleccion de datos
#se compone de clave => valor
#claves de cualquier tipo
_hashes = {"nombre"=> "edwin", []=> ["legro", "agudelo"], "edad"=> 28, "estado"=> true}
puts _hashes[[]] # muestra el arreglo
#valor por defecto para claves nulas 
_hashes.default = ":)"

#claves con simbolos
_hashes_new = {:nombre => "Edwin", :edad=> 28, :cursos=> []}
_hashes_json = {nombres:"Edwin Andres", apellidos:"Legro agudelo", edad:28}
#La sintaxis superior es = a la tipo json
puts _hashes_json[:nombres] #para poder conseguir un valor de la sintaxis json o _hash

#Iterar un hash
_hashes_json.each do |clave, valor|
	puts "En el index: #{clave} esta el valor: #{valor}"
end

#Operaciones
#tamaño de un hash
puts _hashes_json.size +  _hashes_json.length 

#buscar una clave de un hash
puts _hashes_json.has_key?(:edad)  #return  a true o false

#conseguir las claves del hash
puts _hashes_json.keys #retorna un arreglo con las claves

#conseguir los valores del hash
puts _hashes_json.values

#limpiar el  hash
puts _hashes_json.clear

#eliminar una clave valor 
_hashes_json.delete(:edad) #busca y elimina la clave del hash
puts _hashes_json

#validar si el hash esta vacio
puts _hashes_json.empty? #retona true o falso

#Buscar una clave o index del hash
puts _hashes_json.index(28) #retorna el index edad

#Validar sin un valor existe en un hash
puts _hashes_json.has_values?(28) #retorna true o false

#invertir las claves por los valores
puts _hashes_json.invert

#combinar los hash
user_info = {direccion:"0", estado:"1"}
puts _hashes_json.merge(user_info)
