#los proc si son objetos
#Los bloques no son objetos, no se pueden alamacenar en variables
#Los proc se pueden almacenar y enviar los bloques como argumentos
#Un metodo solo puede resivir un bloque
#Un metodo puede resivir multiples proc

def saludo &bloque
	puts bloque.class.name #retorna que es un Proc
	bloque.call
end

def despedida proc1 proc2
	proc1.call
	proc2.call	
end

saludo { puts "Hola metodo lo he definido"}

#definir un proc
proc1 = Proc.new { puts "Hola proc 1"}
proc2 = Proc.new { puts "Hola proc 2"}

despedida(proc1, proc2)

#Lambda
#Funcion anonima 
#usar palabra lambda
lambda { puts "Hola lambda"} 
#para llamar el metodo lambda
(lambda { puts "Hola lambda"}).call
#las lambda se puede guadar en una variable
mi_lambda = lambda { puts "Hola lambda"}
mi_lambda.call

#Otra sintaxis para declarar es =
mi_lambda = ->() {puts  "Hola mundo"}

#argumentos en lambda
mi_lambda = ->(nombre) {puts  "Hola mundo #{nombre}"}
mi_lambda.call("Edwin Andres")

#las lambda son de origen de los Proc




