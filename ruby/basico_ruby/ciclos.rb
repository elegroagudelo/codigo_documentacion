#while
#uncle

_cont = 0
while _cont < 10
	#intruciones
	puts _cont
	_cont+=1
end

songs = ["uno", "dos", "tres", "cuatro", "cinco"]
play = true
_index = 0

#mientras que
while (_index < songs.length) && play
	puts "OK.."
	_index+=1
end

#until inverso de while true = false
#hasta que
until (_index > songs.length) && play
	puts "OK.."
	_index+=1	
end

# do while  hacer mientras
numero = 0
begin
numero+=1
puts numero
end while numero < 10


# do while  hacer hasta que
_cont = 0
begin
_cont+=1
end until numero == 10

#otros iteradores
#Times, upto, downto


10.times do |i|  #un numero definido de veces se ejecuta en un ciclo
	puts i
end

#inicia en (x) hasta que llegues a (x)
1.upto(10) do |n|
puts n
end

#inverso de upto con downto
10.downto(1) do |n|
puts n
end

# a menos que se cumpla true = false
unless true
puts "Es correcto"	
end
