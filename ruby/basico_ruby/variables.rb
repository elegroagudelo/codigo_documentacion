#String
nombres = "Edwin Andres Legro"
edad = 28
puts nombres + " #{edad}"

#Numeros
sumatoria = 20 * 3
puts  "Sumatoria #{sumatoria}"

divide = sumatoria  / 3.5 
puts  "Divide #{divide}"

entero = divide.to_i
puts "Conver Int #{entero}"

float = entero.to_f / 1.5
puts "Conver Float #{float}"

#Valor absoluto
valorabs = -30.abs
puts "Valor absoluto {#valorabs}"

#Numero Par
validPar = 10.even?
puts "Boolean numero par #{validPar}"

#Numero siguiente

siguiente = entero.next
puts "Número siguiente #{siguiente}"


#Metodos sobre Cadenas
#Interpolación
#"#{Variable de interpolación}"

nombres_mayus = nombres.upcase;
puts nombres_mayus;

#Ver  todos los metodos de las cadenas 
#puts "".methods

nombre_minus = nombres.downcase
puts nombre_minus


nombre_capitalize = nombres.capitalize
puts nombre_capitalize

#saltos de linea
puts nombres+ "\n" + entero.to_s + "\t espacio tab"

puts "#{ 20 + 20}"+ " Suma en una interpolación"

#comillas simples
puts  '\n no aplica para generar operaciones ni metodos'

#Pasar numero string a entero
puts  "2".to_i;

#Entrada y salida  por consola
#Usando gets || puts || print
#Puts agresa un salto de linea al final de la cadena a diferencia de print

print "Agregar el nombre para el metodo de entrada "
nombre = gets
puts nombre

#metodo chomp quita el ultimo caracter de una cadena

print "Dame tu apellido: "
apellido = gets
apellido = apellido.chomp
puts "Hola #{apellido}"
puts "#{apellido} tiene #{apellido.length} letras"

#Sintaxis
#Comentarios  usar # para cada linea
#usar =begin =end 

=begin
Para muchas lineas de de comentarios		
=end  

# El uso de los (;)
nombre = "edwin"; apellido = "legro"; edad=28
# Los () son opcionales en un metodo o procedimiento usar cuando aplica un (DSL)
# () Se recomienda en usar en las funiones o hace instancia a una funcion que require de un parametro

# usar Snake que son los guines _nombre_edwin para definir las variables
# Unir las variables de clases  NombreEdwin para su definición


#operadores aritmeticos
#precedencia de operadores
=begin
() ** || * / %  || + -
=end

# numeros a la potencia  x2
puts numero_exponecial = 3 ** 2

#Operadores Condicionales
#Operador de comparacion combinado
 10 <=> 20
 # si es = retorna 0
 # si el primero es mayor retorna 1
 # si el segundo es mayor retorn -1

#Comparar valores de tipo distintos
 2.0 == 2  #return true

#Validad la comparacion del mismo tipo se usa eql?

2.eql?(2.0) #return false


