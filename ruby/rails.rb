#Videos de Ruby o Rails
---------------------------
#>>Models
----------
 # requiere que los nombres sean en ingles y en singular, esto genera una tabla en plural
$ rails generate model People document:integer firt_names:string last_names:string email:string address:string phone:string 
#Crear de forma automatica la vista, rutas, modelos, assets
$ rails generate scaffold User document:integer firtnames:string lastnames:string email:string address:string phone:string
genero:string avatar:string

$ bundler install #---instala del archivo Gemfile los dependencias necerias o gemas


#>>Las migraciones
------------------
 #para usar mysql se requiere de la gem mysql2:

$ gem install mysql2 
 # Luego agregar la gem al archivo de Gemfile

$ rake db:migrate    #---ejecuta las migraciones de /proyecto/db/

#Remover una migracion generada
-------------------------------
$ rake db:rollback   #---realiza un drop a las tablas creadas

 # Los models, estos no requieren de un id, este se genera de forma automatica


#>>Consola de rails
-------------------

$ rails console 		#---genera una consola irb$

irb$ Users.all       #---acceso a los registros de la tabla users


#>>Los Layouts
--------------
 # por medio del yield agrega el contenido en el layout application del sistema
 # Usa DRY "Busca no repetir codigo"
 # En el layout application podemos cargar el menu || sidebar || footer y otros
 # crear un formulario en las vistas:

<%= form_for @product do |f| %>
 #crear los campos de texto usar 
 <%= f.text_field :name_product, placeholder:"name product" %>
 <%= f.text_field :code_product, placeholde:"code product"%>
 <%= f.text_area :description %>  
 <%= f.submit :save_model "Guardar", class:'btn btn-primary' %>
<%end%>

 #la vista envia un post  con productos
 


#>>Los routers
--------------
 # Las rutas usamos el protocolo HTTP
 # Los vervos: GET || POST || DELETE || PUT || PATCH
 # Para renombrar una ruta en el router usamos:

>> get "miruta", to: "home#index"
>> root  to: "home#index"

#>>Los resources
-----------------
 # podemos realizar acciones GRUD "Crear, Editar, Borrar, Mostrar"

>> resources :productos
 	get "/productos" 
	post "/productos" 
 	get "/productos/:id" 
	post "/productos/:id" 
 	put "/productos/:id"
 	delete "/productos/:id"
 	patch "/productos/:id"

 # Podemos generar restricciones en los metodos:

$ resources  :productos, only: [:create, :show]  #unicamente o solamente

$ resources  :productos, except: [:delete, :put]


#>>Contollers
-------------
 $ resources :productos   #asume que hay un controller llamado productos

 # crear un controlador
$ rails generate controller nombre_controller metodo_index

 # controller sin javascript y sin hojas de estilo
$ rails generate controller nombre_controller metodo_index --skip-javascripts --skip-stylesheets



 #crear un registro en el modelo desde el controller
$ @products Product.new  #instancia del modelo empty y es enviado a la vista


#>>ActiveRecors
$ @article = Article.find(params[:id])
$ @article = Article.where("id= ?", params[:id])
$ @article = Article.where("id= #{params[:id]}") # no se recomienda por seguridad
$ @article = Article.where.not("id= ?", params[:id]) #diferente a


#metodo destroy 
def destroy 
	@article = Article.find(params[:id])
	@article.destroy
	redirect_to article_patch
end

#metodo update
def update
	@article = Article.find(params[:id])
	@article.update_attributes({title:"", nombre: ""})		
end

#crear y registrar

def create
	#params[:article] variable hash del post enviado
	@article = Article.new(params[:article])	
	if @article.save
		redirect_to @article
	else
	render :new #muestra la vista del metodo controller new
end


<p id="notice"><%= notice %></p>
<p>
  <strong>Document:</strong>
  <%= @user.document %>
</p>
<p>
  <strong>Firtsname:</strong>
  <%= @user.firtsname %>
</p>
<p>
  <strong>Lastsname:</strong>
  <%= @user.lastsname %>
</p>
<p>
  <strong>Email:</strong>
  <%= @user.email %>
</p>
<p>
  <strong>Address:</strong>
  <%= @user.address %>
</p>
<p>
  <strong>Phone:</strong>
  <%= @user.phone %>
</p>
<p>
  <strong>Isadmin:</strong>
  <%= @user.isAdmin %>
</p>
<p>
  <strong>Isempre:</strong>
  <%= @user.isEmpre %>
</p>
<p>
  <strong>Comunidad:</strong>
  <%= @user.comunidad_id %>
</p>
<%= link_to 'Edit', edit_user_path(@user) %> |
<%= link_to 'Back', users_path %>

rails g scaffold Users document:integer firtsname:string lastsname:string email:string address:string phone:string isAdmin:boolean isEmpre:boolean avatar:string genero:string Communities:references

  devise_for :users, path:'', path_names: { 
    sign_in: 'login',
    sign_out: 'logout',
    sign_up: 'register',
    edit: 'settings'
  }
