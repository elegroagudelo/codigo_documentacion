#restaurante POO
#variables de instancia empiezan con @x
#variables de instancia se pueden acceder desde cualquier parte de la misma clase
#metodos accesores  " "

class Platos
  attr_accessor :tipo, :nombre #acceso a lectura y escritura
  attr_reader :precio #lectura
  attr_writer :cantidad #escritura

  def initialize(_tipo, _name)
  	@nombre = _name
  	self.tipo = _tipo
  	#El return no aplica 
  end

  #get
  def nombre
  	@nombre
  end

  #set
  def nombre=(_name)
  	@nombre = _name
  end
end

plato_carta = Platos.new
plato_carta.tipo = "Entrada";
plato_carta.nombre = "Pastas con Pollo";

#Herencia 

class Videos
 attr_accessor :titulo, :duration

 def config(_titulo)
 	@titulo = _titulo
 end
end

class VideosYoutube > Videos #establece la herencia 
 attr_accessor :youtube_id

 def config(titulo) #Override un metodo del padre
 	super
 end

end

yt = VideosYoutube.new
yt.titulo = "Se hereda de Video"


#Ambitos de las variables
#public todos los elementos por default es publico
#private
#protected

class MisPlatos
  attr_accessor :tipo, :nombre #acceso a lectura y escritura
  attr_reader :precio #lectura
  attr_writer :cantidad #escritura

  def initialize()
  end

  private
  def config_privado
  	"#{@precio} Variable private"
  end

  protected
  def config_protected
  	"#{@nombre} Variable protected" 
  end

end

#metodos de clase
#metodos static
class SoyObjeto
 @nombre_clase = "Nombre de clase"

 def self.nombre_clase #metodo de clase
 	@nombre_clase
 end
 def self.nombre_clase=(_name)
 	@nombre_clase= _name
 end
end

puts SoyObjeto.nombre_clase #se acceden directamente por ser de clase
SoyObjeto.nombre_clase = "Nuevo nombre de clase"


#Otra forma de definir los metodos de clase
class SoyObjeto
	class << self
 		def nombre_clase #metodo static de clase
 			@nombre_clase
 		end		
	end
end

#variables de clase
#estas se identifican por los @@x
#se pueden acceder desde fuere por metodos de clase o metodos de objeto
class NewObjeto
	@@tipo = "video/mp4"

	def self.tipo_desde_clase #metodo de clase
		puts @@tipo
	end
	def tipo_desde_clase #metodo de objeto
		puts @@tipo
	end
end

NewObjeto.tipo_desde_clase
NewObjeto.new.tipo_desde_clase

#Polimorfismos #Los metodos son los mismos pero las acciones a ejecutar son distintas
#Ruby no usa las interfaces
class Video
	def play
		#run video
	end
end

class Vimeo < Video
	def play		
		puts "Run usando Vimeo"
	end
end

class YouTube < Video
	def play
		puts "Run usando YouTube"
	end
end
