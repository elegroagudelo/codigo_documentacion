
RUBY
=====

```
    $~ git clone https://github.com/rbenv/rbenv.git ~/.rbenv  
    $~ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc   
    $~ echo 'eval "$(rbenv init -)"' >> ~/.bashrc 
    $~ exec $SHELL    
```

------
```
    $~ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
    $~ echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
    $~ exec $SHELL

    $~ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.profile
    $~ echo 'eval "$(rbenv init -)"' >> ~/.profile
```

-----

* $~ rbenv install 2.5.5
* $~ rbenv global 2.5.5
* $~ gem install bundler
* $~ gem install rails -v 5.2.3
* $~ rbenv rehash

===========

APACHE RUBY
------------------------
Instalar el modulo de apache para ruby
```
    $~ sudo apt-get install libapache2-mod-passenger
    $~ sudo a2enmod passenger
    $~ sudo service apache2 restart
    $~ sudo rm /usr/bin/ruby
    $~ sudo ln -s /home/elegro/.rbenv/shims/ruby /usr/bin/ruby
    
    $~ cd /var/www/ruby/testapp
    $~ bundle install
    $~ sudo a2dissite 000-default
    $~ sudo a2ensite testapp
    $~ sudo service apache2 restart
```


=====

USANDO RVM
----
Requiere de instalar 
* apt-get install gnupg gnupg2 -y   
gnupg permite encriptar llave publicas      

```
    $~ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
    $~ curl -sSL https://get.rvm.io | bash
    $~ source /home/elegro/.rvm/scripts/rvm
    $~ rvm list
    $~ rvm list known
    $~ rvm install ruby-head
    $~ rvm --default use 3.1.1
    $~ ruby --version
```

Manejo de gemsets
----
Permite dar manejo a las versioens de trabajo para ruby. 

----
Instalar Rails
---
```
    npm install --global webpack
    gem search '^rails$' --all
    gem install rails -v 6.1.4.6
```

--- creamos el gemset a usar

```
    rvm gemset create gemset_name_prueba
```

--- especificamos la version de ruby a usar con el gemset

```
    rvm ruby_version@gemset_name --create

    /bin/bash --login
    
    $~ rails new project_prueba1
    $~ rvm gemset list
    $~ echo gemset_name_prueba > .ruby-gemset
    $~ /bin/bash --login
    $~ rvm gemset list

    $~ bundle install
    $~ bundle exec rails webpacker:install
    $~ rails serve 
```


Documentación
----
```
    https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rvm-on-ubuntu-18-04

    https://gist.github.com/dahngeek/c617b8707efbc876d019952a840ce0bd
```