#Bloques= Son un grupo de sentencias agrupadas Closurs
#Podemos usar una iteracion para filtrar

[1,2,3,4,5,6,7,8,9].each {|values| puts values}

#El metodo es igual a
[1,2,3,4,5,6,7,8,9].each do |values|
puts values
end

#la iteracion para una sola linea se usa para una sentencia simple a ejecutar
#Filtro selectivo

impares = [1,2,3,4,5,6,7,8].select do |values|
	values % 2 != 0 #sentencia a ejecutar retona un true
end
puts impares

#metodo con más de un argumento
['a','b','c','d','e'].each_with_index do |values, index|
	puts "El #{values} se encuentra en el #{index}"
end

#REGLAS
#Los bloques retornan un result pero no requieren de la variable return
#Un bloque por lo general se almacenan en variables
#El valor que retorna es la ultima intruccion dentro del bloque


#Metodos en bloques
#llamada de bloques

def metodo
	yield 
end

metodo { puts "Hola mundo"}
#El metodo es igual siguiente=
metodo do
	puts "Hola mundo"
end

def calculo
	yield if block_given? #Condiciona si existe un bloque en el metodo
end

def calculo &argumento #pasando un argumento de bloque
	argumento.call if block_given? #valida el argumento y el bloque
end


#Bloques para recibir argumantos y retornar valores
class Usuario
	attr_accessor :nombre
	
	def saludar #bloque de metodo
		yield(@nombre)
	end
end
new_usuario = Usuario.new
new_usuario.nombre = "Edwin Andres"
#defino el bloque 
new_usuario.saludar { |nombre| puts	"Hola #{nombre}"}
new_usuario.saludar do |nombre|
	saludo = "Hola #{nombre}"
end



def metodo_puro index
	mese = [
		'ninguno',
		'enero',
		'febrero',
		'marzo',
		'abril',
		'mayo',
		'junio',
		'julio',
		'agosto',
		'septiembre',
		'octubre',
		'noviembre',
		'diciembre'
	];
	puts meses[index]
end

metodo_lambda = -> (index) {
	mese = [
		'ninguno',
		'enero',
		'febrero',
		'marzo',
		'abril',
		'mayo',
		'junio',
		'julio',
		'agosto',
		'septiembre',
		'octubre',
		'noviembre',
		'diciembre'
	];
	puts meses[index]
}