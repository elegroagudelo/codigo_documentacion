<p>XML and HTML are the most popular markup languages (textual ways of describingstructured data). HTML is used to describe textual documents, like you see on theWeb. XML is used for just about everything else: data storage, messaging, configura-tion files, you name it. Just about every software buzzword forged over the past fewyears involves XML.<br/><br/>

Java and C++ programmers tend to regard XML as a lightweight, agile technology,and are happy to use it all over the place. XML is a lightweight technology, but onlycompared to Java or C++. Ruby programmers see XML from the other end of thespectrum, and from there it looks pretty heavy. Simpler formats like YAML and JSONusually work just as well (see Recipes 14.1 or 14.2), and are easier to manipulate. Butto shun XML altogether would be to cut Ruby off from the rest of the world, andnobody wants that. This chapter covers the most useful ways of parsing, manipulat-ing, slicing, and dicing XML and HTML documents.<br/><br/>

There are two standard APIs for manipulating XML: DOM and SAX. Both are over-kill for most everyday uses, and neither is a good fit for Ruby’s code block-heavystyle. Ruby’s solution is to offer a pair of APIs that capture the style of DOM and SAXwhile staying true to the Ruby programming philosophy.1 Both APIs are in the stan-dard library’s REXML package, written by Sean Russell.<br><br>
Like DOM, the Document class parses an XML document into a nested tree of objects.You can navigate the tree with Ruby accessors (Recipe 12.2) or with XPath queries(Recipe 12.4). You can modify the tree by creating your own Element and Textobjects (Recipe 12.8). If even Document is too heavyweight for you, you can use theXmlSimple library to transform an XML file into a nested Ruby hash (Recipe 12.5).
<br><br>
With a DOM-style API like Document, you have to parse the entire XML file beforeyou can do anything. The XML document becomes a large number of Ruby objectsnested under a Document object, all sitting around taking up memory. With a SAX-style parser like the StreamParser class, you can process a document as it’s parsed,creating only the objects you want. The StreamParser API is covered in Recipe 12.3.
<br><br>
The main problem with the REXML APIs is that they’re very picky. They’ll only parsea document that’s valid XML, or close enough to have an unambiguous representa-tion. This makes them nearly useless for parsing HTML documents off the WorldWide Web, since the average web page is not valid XML.For more information on the REXML package, see the following:
<br><br>
</p>

<h4>Discussion</h4>
<p>
	Programs that use XML validation are more robust and less complicated than nonva-lidating versions. Before starting work on a document, you can check whether or notit’s in the format you expect. Most services that accept XML as input don’t have for-giving parsers, so you must validate your document before submitting it or it mightfail without you even noticing.<br><br>
	One of the most popular and complete XML libraries around is the GNOME libxml2library. Despite its name, it works fine outside the GNOME platform, and has beenported to many different OSes. The Ruby projects nokogiri and libxml are Rubywrappers around the GNOME libxml2 library. Not only does libxml support valida-tion and a complete range of XML manipolation techniques, it can also improve yourprogram’s speed by an order of magnitude, since it’s written in C instead of REXML’spure Ruby.<br><br>
	Don’t confuse the libxml project with the libxml library. The latter is part of theXML::Tools project. It binds against the GNOME Libxml2 library, but it doesn’texpose that library’s validation features. If you try the preceding example code butcan’t find the XML::Dtd or the XML::Schema classes, then you’ve got the wrong bind-ing. If you installed the libxml-ruby package on Debian GNU/Linux, you’ve got thewrong one. You need the one you get by installing the libxml-ruby gem. Of course,you’ll need to have the actual GNOME libxml library installed as well.
	
</p>

<h4>CHAPTER 7 <br>Files and Directories</h4>
<p>CHAPTER 7Files and DirectoriesAs programming languages increase in power, we programmers get further and fur-ther from the details of the underlying machine language. When it comes to the oper-ating system, though, even the most modern programming languages live on a levelof abstraction that looks a lot like the C and Unix libraries that have been around fordecades.</p>

<p>We covered this kind of situation in Chapter 4 with Ruby’s Time objects, but the issuereally shows up when you start to work with files. Ruby provides an elegant object-oriented interface that lets you do basic file access, but the more advanced file libra-ries tend to look like the C libraries they’re based on. To lock a file, change its Unixpermissions, or read its metadata, you’ll need to remember method names like mtime,and the meaning of obscure constants like File::LOCK_EX and 0644. This chapter willshow you how to use the simple interfaces, and how to make the more obscure inter-faces easier to use.</p>
<br>
<p>Looking at Ruby’s support for file and directory operations, you’ll see four distincttiers of support. The most common operations tend to show up on the lower-numbered tiers:</p>

<ol>
	<li>File objects to read and write the contents of files, and Dir objects to list thecontents of directories—for examples, see Recipes 7.5, 7.7, and 7.17. Also seeRecipe 7.13 for a Ruby-idiomatic approach.</li>
	<li>Class methods of File to manipulate files without opening them. For instance, todelete a file, examine its metadata, or change its permissions. For examples, seeRecipes 7.1, 7.3, and 7.4.</li>
	<li>Standard libraries, such as find to walk directory trees, and fileutils to per-form common filesystem operations like copying files and creating directories.For examples, see Recipes 7.8, 7.12, and 7.20.</li>
	<li>Gems like file-tail, lockfile, and rubyzip, which fill in the gaps left by thestandard library. Most of the file-related gems covered in this book deal with spe-cific file formats, and are covered in Chapter 13.</li>
</ol>

<p>Kernel#open is the simplest way to open a file. It returns a File object that you canread from or write to, depending on the “mode” constant you pass in. We’ll introduceread mode and write mode here; there are several others, but we’ll talk about most ofthose as they come up in recipes.</p>
<p>To write data to a file, pass a mode of w to open. You can then write lines to the filewith File#puts, just like printing to standard output with Kernel#puts. For morepossibilities, see Recipe 7.7:</p>

<code>
	open('beans.txt', "w") do |file|<br/>
		&nbsp;&nbsp;file.puts('lima beans')<br/>
		&nbsp;&nbsp;file.puts('pinto beans')<br/>
		&nbsp;&nbsp;file.puts('human beans')<br/>
	end
</code>

<p>To read data from a file, open it for read access by specifying a mode of r, or justomitting the mode. You can slurp the entire contents into a string with File#read, orprocess the file line-by-line with File#each. For more details, see Recipe 7.6:</p>
<code>
	open('beans.txt') do |file| <br/>
		&nbsp;&nbsp;file.each { |l| puts "A line from the file: #{l}" }<br/>
	end
</code>

<p>As seen in these examples, the best way to use the open method is with a code block.The open method creates a new File object, passes it to your code block, and closesthe file automatically after your code block runs—even if your code throws an excep-tion. This saves you from having to remember to close the file after you’re done withit. You could rely on the Ruby interpreter’s garbage collection to close the file once it’sno longer being used, but Ruby makes it easy to do things the right way.<br>To find a file in the first place, you need to specify its disk path. You may specify anabsolute path, or one relative to the current directory of your Ruby process (seeRecipe 7.21). Relative paths are usually better, because they’re more portable acrossplatforms. Relative paths like beans.txt or subdir/beans.txt will work on any plat-form, but absolute Unix paths look different from absolute Windows paths:</p>

<code>
open('/etc/passwd')
<br>
open('c:/windows/Documents and Settings/User1/My Documents/ruby.doc')
</code>
<p>Windows paths in Ruby use forward slashes to separate the parts of a path, eventhough Windows itself uses backslashes. Ruby will also accept backslashes in a Win-dows path, so long as you escape them:</p>

<code>open('c:\\windows\\Documents and Settings\\User1\\My Documents\\ruby.doc')</code>

<p>Although this chapter focuses mainly on disk files, most of the methods of File areactually methods of its superclass, IO. You’ll encounter many other classes that arealso subclasses of IO, or just respond to the same methods. This means that most ofthe tricks described in this chapter are applicable to classes like the Socket class forInternet sockets and the infinitely useful StringIO (see Recipe 7.15).</p>

<p>Your Ruby program’s standard input, output, and error ($stdin, $stdout, and$stderr) are also IO objects, which means you can treat them like files. This one-lineprogram echoes its input to its output:</p>

<code>$stdin.each { |l| puts l }</code>
<p>The Kernel#puts command just calls $stdout.puts, so that one-liner is equivalent tothis one:</p>
<code>$stdin.each { |l| $stdout.puts l }</code>
<p>Not all file-like objects support all the methods of IO. See Recipe 7.11 for ways to getaround the most common problem with unsupported methods. Also see Recipe 7.16for more on the default IO objects.Several of the recipes in this chapter (such as Recipes 7.12 and 7.20) create specificdirectory structures to demonstrate different concepts. Rather than bore you by fill-ing up recipes with the Ruby code to create a certain directory structure, we’ve writ-ten a method that takes a short description of a directory structure, and creates theappropriate files and subdirectories:</p>

<code>
# create_tree.rb<br>
def create_tree(directories, parent=".")<br>
 &nbsp;directories.each_pair do |dir, files|<br>
 &nbsp;&nbsp;path = File.join(parent, dir)<br>
 &nbsp;&nbsp;Dir.mkdir path unless File.exists? path<br>
 &nbsp;&nbsp;files.each do |filename, contents| <br>
 &nbsp;&nbsp;&nbsp;if filename.respond_to? :each_pair# It's a subdirectory <br>
 &nbsp;&nbsp;&nbsp;&nbsp;create_tree filename, path<br>
 &nbsp;&nbsp;&nbsp;else # It's a file<br>
 &nbsp;&nbsp;&nbsp;&nbsp;open(File.join(path, filename), 'w') { |f| f << contents || "" } <br>
 &nbsp;&nbsp;&nbsp;end<br>
 &nbsp;&nbsp;end<br>
 &nbsp;end<br>
 end
</code>

<p>Now we can present the directory structure as a data structure and you can create itwith a single method call:</p>
<code>
	require 'create_tree'<br>
	create_tree 'test' =>[ 'An empty file',['A file with contents', 'Contents of file'],{ 'Subdirectory' => ['Empty file in subdirectory',['File in subdirectory', 'Contents of file'] ] },{ 'Empty subdirectory' => [] }]<br>

	require 'find'<br>
	Find.find('test') { |f| puts f }<br>
	# test<br>
	# test/Empty subdirectory# test/Subdirectory<br>
	# test/Subdirectory/File in subdirectory<br>
	# test/Subdirectory/Empty file in subdirectory<br>
	# test/A file with contents<br>
	# test/An empty file<br><br>

	File.read('test/Subdirectory/File in subdirectory')<br>
	# => "Contents of file"
</code>


<h2>7.1 Checking to See If a File Exists</h2>

<h4>Problem</h4>
<p>
Given a filename, you want to see whether the corresponding file exists and is theright kind for your purposes.
</p>
<h4>Solution</h4>
<p>
Most of the time you’ll use the File.file? predicate, which returns true only if thefile is an existing regular file (that is, not a directory, a socket, or some other specialfile):</p>

<code>
	filename = 'a_file.txt'<br>
	File.file? filename<br><br>
	
	require 'fileutils'<br>
	FileUtils.touch(filename)<br>
	File.file? filename
</code>
<p>Use the File.exists? predicate instead if the file might legitimately be a directory orother special file, or if you plan to create a file by that name if it doesn’t exist.File.exists? will return true if a file of the given name exists, no matter what kindof file it is:</p>

<code>
	directory_name = 'a_directory'<br>
	FileUtils.mkdir(directory_name)<br>
	File.file? directory_name # => false<br>
	File.exists? directory_name<br>
</code>

<h4>Discussion</h4>
<p>A true response from File.exists? means that the file is present on the filesystem,but says nothing about what type of file it is. If you open up a directory thinking it’s aregular file, you’re in for an unpleasant surprise. This is why File.file? is usuallymore useful than File.exists?.Ruby provides several other predicates for checking the type of a file: the other com-monly useful one is File.directory?:</p>
<code>
	File.directory? directory_name # => true<br>
File.directory? filename</code>

<p>The rest of the predicates are designed to work on Unix systems. File.blockdev?tests for block-device files (such as hard-drive partitions), File.chardev? tests forcharacter-device files(such as TTYs), File.socket? tests for socket files, andFile.pipe? tests for named pipes:</p>

<code>
	File.blockdev? '/dev/hda1'# => true<br>
	File.chardev? '/dev/tty1'# => true<br>
	File.socket? '/var/run/mysqld/mysqld.sock' # => true<br>
	system('mkfifo named_pipe')<br>
	File.pipe? 'named_pipe'<br>
</code>
<p>File.symlink? tests whether a file is a symbolic link to another file, but you onlyneed to use it when you want to treat symlinks differently from other files. A symlinkto a regular file will satisfy File.file?, and can be opened and used just like a regularfile. In most cases, you don’t even have to know it’s a symlink. The same goes for sym-links to directories and to other types of files:</p>

<code>
	new_filename = "#{filename}2"<br>
	File.symlink(filename, new_filename)<br>
	File.symlink? new_filename # => true<br>
	File.file? new_filename #=> false
</code>
<p>All of Ruby’s file predicates return false if the file doesn’t exist at all. This means youcan test “exists and is a directory” by just testing directory?; it’s the same for theother predicates.</p>

<h2>7.2 Checking Your Access to a File</h2>

<h4>Problem</h4>
<p>
You want to see what you can do with a file—that is, whether you have read, write, or(on Unix systems) execute permission on it.

</p>
<h4>Solution</h4>
<p>Use the class methods File.readable?, File.writeable?, and File.executable?:</p>

<code>
	File.readable?('/bin/ls')# => true <br>
	File.readable?('/etc/passwd-') # => false <br>
	filename = 'test_file'File.open(filename, 'w') {} <br>
	File.writable?(filename) # => true <br>
	File.writable?('/bin/ls') # => false <br>
	File.executable?('/bin/ls') # => true <br>
	File.executable?(filename) # => false <br>
</code>

<h4>Discussion</h4>
<p>Ruby’s file permission tests are Unix-centric, but readable? and writable? work onany platform; the rest fail gracefully when the OS doesn’t support them. For instance,Windows doesn’t have the Unix notion of execute permission, so File.executable?always returns true on Windows.</p>
<p>The return value of a Unix permission test depends in part on whether your userowns the file in question, or whether you belong to the Unix group that owns it. Rubyprovides convenience tests File.owned? and File.grpowned? to check this:</p>

<code>
	File.owned? 'test_file'# => true <br>
	File.grpowned? 'test_file'# => true <br>
	File.owned? '/bin/ls'# => false <br>
</code>

<p>On Windows, File.owned? always returns true (even for a file that belongs to anotheruser) and File.grpowned? always returns false.The File methods just described should be enough to answer most permission ques-tions about a file, but you can also see a file’s Unix permissions in their native form bylooking at the file’s mode. The mode is a number, each bit of which has a differentmeaning within the Unix permission system.1 You can view a file’s mode withFile::Lstat#mode.</p>

<p>The result of mode contains some extra bits describing things like the type of a file.You probably want to strip that information out by masking those bits. This exampledemonstrates that the file originally created in the solution has a Unix permissionmask of 0644:</p>

<code>
	File.lstat('test_file').mode & 0777 
	# Keep only the permission bits.
	# => 420
	# That is, 0644 octal.
</code>

<h4>setuid and setgid Scripts</h4>
<p>readable?, writable?, and executable? return answers that depend on the effectiveuser and group ID you are using to run the Ruby interpreter. This may not be youractual user or group ID: the Ruby interpreter might be running setuid or setgid, oryou might have changed their effective ID with Process.euid= or Process.egid=.Each of the permission checks has a corresponding method that returns answersfrom the perspective of the process’s real user and real group IDs: executable_real?,readable_real?, and writable_real?. If you’re running the Ruby interpreter setuid,then readable_real? (for instance) will give different answers from readable?. Youcan use this to disallow users from reading or modifying certain files unless theyactually are the root user, not just taking on the root users’ privileges through setuid.For instance, consider the following code, which prints our real and effective user andgroup IDs, then checks to see what it can do to a system file:</p>

<code>
	def what_can_i_do?<br>
		&nbsp;sys = Process::Sys <br>
		&nbsp;puts "UID=#{sys.getuid}, GID=#{sys.getgid}" <br>
		&nbsp;puts "Effective UID=#{sys.geteuid}, Effective GID=#{sys.getegid}" <br>

		&nbsp;file = '/bin/ls' <br>
		&nbsp;can_do = [:readable?,:writable?,:executable?].inject([]) do |arr,method| <br>
			&nbsp;&nbsp;arr << method, file); arr <br>
		&nbsp;end <br>
		&nbsp;puts "To you, #{file} is: #{can_do.join(', ')}" <br>
	end
</code>
<p>If you run this code as root, you can call this method and get one set of answers, thentake on the guise of a less privileged user and get another set of answers:</p>

<code>
	what_can_i_do?
	<br>
	Process.uid = 1000
	<br>
	what_can_i_do?
</code>


<h2>7.3 Changing the Permissions on a File</h2>
<h4>Problem</h4>
<p>You want to control access to a file by modifying its Unix permissions. For instance,you want to make it so that everyone on your system can read a file, but only you canwrite to it.</p>

<h4>Solution</h4>
<p>Unless you’ve got a lot of Unix experience, it’s hard to remember the numeric codesfor the nine Unix permission bits. Probably the first thing you should do is defineconstants for them. Here’s one constant for every one of the permission bits. If thesenames are too concise for you, you can name them USER_READ, GROUP_WRITE, OTHER_EXECUTE, and so on:</p>

<code>
	class File
		&nbsp;U_R = 0400<br>
		&nbsp;U_W = 0200<br>
		&nbsp;U_X = 0100<br>
		&nbsp;G_R = 0040<br>
		&nbsp;G_W = 0020<br>
		&nbsp;G_X = 0010<br>
		&nbsp;O_R = 0004<br>
		&nbsp;O_W = 0002<br>
		&nbsp;O_X = 0001<br>
	end
</code>
<p>You might also want to define these three special constants, which you can use to setthe user, group, and world permissions all at once:</p>

<code>
	class File
		&nbsp;A_R = 0444<br>
		&nbsp;A_W = 0222<br>
		&nbsp;A_X = 0111<br>
	end
</code>

<p>Now you’re ready to actually change a file’s permissions. Every Unix file has a permis-sion bitmap, or mode, which you can change (assuming you have the permissions!)by calling File.chmod. You can manipulate the constants we just defined to get a newmode, then pass it in along with the filename to File.chmod.The following three chmod calls are equivalent: for the file my_file, they give read-write access to the user who owns the file, and restrict everyone else to read-onlyaccess. This is equivalent to the permission bitmap 11001001, the octal number 0644,or the decimal number 420:
</p>


<code>
open("my_file", "w") {}<br>
File.chmod(File::U_R | File::U_W | File::G_R | File::O_R, "my_file")<br>
File.chmod(File::A_R | File::U_W, "my_file") <br>
File.chmod(0644, "my_file")# Bitmap: 110001001 <br>
File::U_R | File::U_W | File::G_R | File::O_R # => 420 <br>
File::A_R | File::U_W # => 420 <br>
0644 # => 420 <br>
File.lstat("my_file").mode & 0777
</code>

<p>Note how we build a full permission bitmap by combining the permission constantswith the OR operator (|).</p>

<h4>Discussion</h4>
<p>A Unix file has nine associated permission bits that are consulted whenever anyonetries to access the file. They’re divided into three sets of three bits. There’s one set forthe user who owns the file, one set for the user group who owns the file, and one setfor everyone else.Each set contains one bit for each of the three basic things you might do to a file inUnix: read it, write it, or execute it as a program. If the appropriate bit is set for you,you can carry out the operation; if not, you’re denied access.When you put these nine bits side by side into a bitmap, they form a number that youcan pass into File.chmod. These numbers are difficult to construct and read withouta lot of practice, which is why we recommend you use the constants defined earlier.It’ll make your code less buggy and more readable.1File.chmod completely overwrites the file’s current permission bitmap with a newone. Usually you just want to change one or two permissions—to make sure the fileisn’t world-writable, for instance. The simplest way to do this is to use File.lstat#mode to get the file’s current permission bitmap, then modify it with bitoperators to add or remove permissions. You can pass the result into File.chmod.Use the XOR operator (^) to remove permissions from a bitmap, and the OR opera-tor, as seen previously, to add permissions:</p>

<code>
	new_permission = File.lstat("my_file").mode ^ File::O_R<br>
	File.chmod(new_permission, "my_file")<br>
	File.lstat("my_file").mode & 0777 # => 416 # 0640 octal<br>
	# Give everyone access to everything<br>
	new_permission = File.lstat("my_file").mode | File::A_R | File::A_W | File::A_X<br>
	File.chmod(new_permission, "my_file")<br>
	File.lstat("my_file").mode & 0777 # => 511 <br>
	# 0777 octal<br>
	# Take away the world's write and execute access<br>
	new_permission = File.lstat("my_file").mode ^ (File::O_W | File::O_X)<br>
	File.chmod(new_permission, "my_file")<br>
	File.lstat("my_file").mode & 0777 # => 508 <br>
	# 0774 octal<br>
</code>

<p>If doing bitwise math with the permission constants is also too complicated for you,you can use code like this to parse a permission string like the one accepted by theUnix chmod command:</p>
