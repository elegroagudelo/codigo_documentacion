require 'rexml/document'

=begin
require 'rexml/document'
def valid_xml?(xml)
  begin 
    REXML::Document.new(xml)
  rescue REXML::ParseException
    # Return nil if an exception is thrown
  end
end

bad_xml = %{<tasks><pending><entry>Grocery Shopping</entry><done><entry>Dry Cleaning</entry></tasks>}
valid_xml?(bad_xml)

good_xml = %{<groceries><bread>Wheat</bread><bread>Quadrotriticale</bread></groceries>}
doc = valid_xml?(good_xml)
puts doc.root.elements[1]


orders_xml = %{
  <orders>
    <order>
      <number>105</number>
      <date>02/10/2006</date>
      <customer>Corner Store</customer>
      <items>
        <item upc="404100" desc="Red Roses" qty="240" />
        <item upc="412002" desc="Candy Hearts" qty="160"/>
      </items>
    </order>
  </orders>}

orders = REXML::Document.new(orders_xml)

orders.root.each_element do |order|
  order.each_element do |node|
    if node.has_elements?
      node.each_element do |child|
      puts "#{child.name}: #{child.attributes['desc']}"
      end
    else 
      puts "#{node.name}: #{node.text}"
    end
  end
end
#REXML::Document.new(invalid_xml).write
=end
=begin
event_xml = %{
  <eventos>
    <clean system="dev" start="01:35" end="01:55" area="build" error="1"/>
    <backup system="prod" start="02:00" end="02:35" size="2300134" error="0"/>
    <backup system="dev" start="02:00" end="02:01" size="0" error="2" />
    <backup system="test" start="02:00" end="02:47" size="327450" error="0"/>
  </events>}

require 'rexml/document'
require 'rexml/streamlistener'

class ErrorListener
  include  REXML::StreamListener

  def tag_start(name, attrs)
    if attrs["error"] != nil and attrs["error"] != "0"
      puts "Event #{name} failed for system #{attrs["system"]} with code #{attrs["error"]}"
    end
  end
end
REXML::Document.parse_stream(event_xml, ErrorListener.new)
=end

xml = %{
  <aquarium>
    <fish color="blue" size="small" />   
    <fish color="orange" size="large">
        <fish color="green" size="small">
        <fish color="red" size="tiny" />
      </fish>
    </fish>
    <decoration type="castle" style="gaudy">
      <algae color="green" />
    </decoration>
  </aquarium>}

#require 'rexml/document'
#require 'rexml/streamlistener'
#doc = REXML::Document.new(xml)

=begin
puts REXML::XPath.first(doc, '//fish')
puts REXML::XPath.match(doc, '//[@color="green"]')

def describe(fish)
  "#{fish.attribute('size')} #{fish.attribute('color')} fish"
end

REXML::XPath.each(doc, '//fish/fish') do |fish|
  puts "The #{describe(fish.parent)} has eaten the #{describe(fish)}."
end
=end

=begin
red_fish = doc.children[1].children[1]
puts red_fish.xpath

puts REXML::XPath.first(doc, red_fish.xpath)

puts REXML::XPath.match(doc, '//[@color="green"]')[1]

puts REXML::XPath.match(doc, '//fish[@size="small"]/@color')

puts REXML::XPath.first(doc, "count(//fish[@size='large'][1]//*fish)")
puts ">>>>>>>>>>>>"

doc.elements.each('//fish') { |f| puts f.attribute('color') }

puts ">>>>>>>>>>>>"
puts doc.elements[1]
puts ">>>>>>>>>>>>"
puts doc.children[0]
=end

=begin
xml = %{
  <freezer temp="-12" escala="celcius">
    <food> Phyllo masa </food>
    <food> Helado </food>
  <icecubetray>
    <cube1 />
    <cube2 />
  </icecubetray>
  </freezer>}

#gem install 'xml-simple'
require_relative 'xmlsimple.rb'
require 'pp'

parsed1 = XmlSimple.xml_in(xml)
pp parsed1

parsed2 = XmlSimple.xml_in(xml, 'KeyAttr' => 'name')
pp parsed2

#parsed1["item"].detect { |i| i['name'] == 'Phyllo dough' }['type']

#parsed1["item"].delete_if { |i| i["name"] == "Ice cube tray" }

puts XmlSimple.xml_out(parsed1, "RootName"=>"freezer")

parsed3 = XmlSimple.xml_in(xml, 'KeepRoot'=>true)

puts XmlSimple.xml_out(parsed3, 'RootName'=>nil)
=end
=begin
gem 'nokogiri'
require 'nokogiri'
require_relative 'xmlsimple.rb'
require 'pp'
dtd = Nokogiri::XML::Document.parse(%{
  <!ELEMENT rubycookbook (recipe+)>
  <!ELEMENT recipe (title?, problem, solution, discussion, seealso?)+>
  <!ELEMENT title (#PCDATA)>
  <!ELEMENT problem (#PCDATA)><!ELEMENT solution (#PCDATA)>
  <!ELEMENT discussion (#PCDATA)><!ELEMENT seealso (#PCDATA)>})
=end
=begin
open('cookbook.xml', 'w') do |f|f.write %{
  <?xml version="1.0"?>
    <rubycookbook>
      <recipe>
        <title>A recipe</title>
        <problem>A difficult/common problem</problem>
        <solution>A smart solution</solution>
        <discussion>A deep solution</discussion>
        <seealso>Pointers</seealso>
      </recipe>
    </rubycookbook>}
end
=end

#document = Nokogiri::XML(File.open('cookbook.xml'))
#pp document


str = %{
<?xml version="1.0"?>
<!DOCTYPE doc [<!ENTITY product 'Stargaze'>
<!ENTITY version '2.3'>]>
<doc>&product; v&version; is the most advanced astronomy product on the market.</doc>}

#doc = REXML::Document.new str
#puts doc.root.children[0].value
#puts doc.root.text
#puts doc.root.children[0].to_s

=begin
require 'delegate'
require 'rexml/text'

class EntitySubstituter <  DelegateClass(IO)
  def initialize (io, document, filter=nil)
    @document = document
    @filter = filter
    super(io)      
  end

  def <<(s)
    super(REXML::Text::unnormalize(s, @document.doctype, @filter))
  end

end
output = EntitySubstituter.new($stdout, doc)
puts doc.write(output)
=end

#MODIFICANDO EL XML

doc = REXML::Document.new

metting = doc.add_element 'metting'
metting_start = Time.local(2019, 06, 15, 00)
metting.add_element('time', {'from' => metting_start, 'to'=> metting_start + 3600})

#doc.children[0] 
#doc.children[0].children[0];
#doc.write($stdout, 1)
#doc.children[0]
#doc.children[1]

=begin
agenda = metting.add_element 'agenda'
puts doc.children[0].children[1]

agenda.add_text "Texto aqui de agenda"
agenda.add_text " Más texto"
puts doc.children[0].children[1]
doc.write($stdout, 1)


item1 = agenda.add_element 'item'
puts doc.children[0].children[1].children[1]
item1.text = 'Weekly status meetings: improving attendance'
puts doc.children[0].children[1].children[1]
doc.write($stdout, 1)
=end
=begin
doc = REXML::Document.new %{
	<?xml version='1.0'?>
		<girl size="little">
			<foods>
				<sugar />
				<spice />
			</foods>
			<set of="nice things" cardinality="all" />
		</girl>}

root = doc[1]
root.name = 'body'
root.elements['//sugar'].name = 'snails'
root.delete_element('//spice')

set= root.elements['//set']
set.attributes["of"] = 'snips'
set.attributes['cardinality'] = 'some'
root.add_element('set', { 'of' => 'puppy dog tails', 'cardinality'=> 'some 2'})
doc.write
=end

doc = REXML::Document.new File.new("inventory.xml")
doc.elements.each("inventory/section") do |element| 
	puts element.attributes["name"] 
end

#doc.elements.each("*/section/item") { |element| puts element.attributes["upc"] }
doc.elements.each("*/section/item") do |element| 
	puts element.attributes["upc"] 
end

root = doc.root
puts root.attributes["title"]

puts root.elements["section/item[@stock='44']"].attributes["upc"]

puts root.elements["section"].attributes["name"]
puts root.elements[1].attributes["name"] 
=begin
root.detect do |node| 
	node.kind_of? Element and node.attributes["name"] == "food" 
end
=end
=begin
input_encoding = CharGuess::guess(doc)
output_encoding = 'utf-8'
converted_doc = Iconv.new(output_encoding, input_encoding).iconv(doc)
puts CharGuess::guess(converted_doc)
=end